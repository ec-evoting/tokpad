require import NewFMap List Int IntExtra Distr Real AllCore FSet.
require import LeftOrRight.
require (*  *) TokpadDef LazyEager Encryption.
require (*  *) Ex_Plug_and_Pray.

clone include TokpadDef.
(* random oracle *)
clone include LazyEager with
  type from <- ident,
  type to   <- mask, 
  op dsample<- (fun x, dmask). 


(* voting scheme that uses random oracle *)  
module type VotingScheme'(H: ARO) = {  
  proc register(id : ident, pk : pkey) : cred * cipher {H.o}

  proc setup() : pkey * skey {}
  proc vote(id : ident, v : vote, w : cred) : ident * mask {}
  proc devkey (id: ident): idkey {}
  proc token(m: mask, k:idkey): code {}
  proc valid(k:idkey, b:ident*mask*code):bool {}
  proc tally(bb: (ident * mask * code) list, 
             kL: (ident, idkey) fmap, 
             cL: (ident, cipher) fmap, sk: skey) : result {}
}.

module MV_RO(E: EncScheme, H:ARO): VotingScheme ={
  
  proc setup = MV(E).setup
  proc vote  = MV(E).vote
  proc devkey= MV(E).devkey
  proc token = MV(E).token
  proc tally = MV(E).tally
  proc valid = MV(E).valid
  (* register algorithm *)
  proc register(id: ident, pk:pkey): cred * cipher = {
    var w,c;

    w <@ H.o(id);
    c<@ E.enc(pk,w);
    return (w,c);
  }
}.


(* short form for random oracle *)
module EO = EagerRO.RO.
module LO = LazyRO.RO.

module SB ={
  var ikt: (ident, mask * code) fmap
}.

section Dishonest_Browser.
declare module E: EncScheme {BTS, SB, BP, BS, EO, LO, H.Count, H.HybOrcl, WrapAdv}.
declare module A_DB: DB_Adv {BTS, SB, BP, BS, E, EO, LO, H.Count, H.HybOrcl, WrapAdv}.


op keypair: pkey -> skey -> bool.
op decrypt: cipher -> skey -> cred option.

(* lossless *)
axiom Ek_ll:
  islossless E.kgen.
axiom Ee_ll:
  islossless E.enc.
axiom Ed_ll:
  islossless E.dec.

axiom Aa1_ll (O <: DB_Oracles { A_DB }):
  islossless O.reg =>
  islossless O.corrupt =>
  islossless O.vote =>
  islossless O.castH=>
  islossless O.castC=>
  islossless A_DB(O).a1.

axiom Aa2_ll (O <: DB_Oracles { A_DB }):
  islossless A_DB(O).a2.

axiom Voters_uniq:
  uniq Voters.

axiom kgen_keypair:
  equiv [E.kgen ~ E.kgen:  ={glob E} ==> 
          ={glob E,  res} /\ 
          keypair res{2}.`1 res{2}.`2].
  
axiom dec_decrypt (ge: (glob E)) (skx: skey) (cx: cipher):
  phoare [E.dec:  
          (glob E) =ge /\ arg = (skx, cx)
           ==>   
          (glob E) =ge /\
          res = decrypt cx skx] = 1%r.

axiom enc_decrypt_two (skx: skey) (pkx:pkey) (mx: cred):
  keypair pkx skx =>
  equiv [E.enc ~ E.enc: 
          ={glob E, arg} /\ arg{1} = (pkx, mx)
           ==>   
          ={glob E, res}/\
          Some mx = decrypt res{1} skx].



(* dishonest browser with b0 replaced by b0- line 152, for Left*)
module DB_L2(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, bb,vbb, e,imt;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    (* take all valid ballots *)
    vbb <- [];
    i <- 0;
    while (i < size BP.bb_b){
      imt <- nth witness BP.bb_b i;
      e <@ V.valid(oget BP.kL.[imt.`1],imt);
      if (e) { 
        vbb <- vbb ++ [imt];
      }
      i <- i+1;
    }
    (* use v0 (via b0) only for b_beta *) 
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;
    (* compute result *)
    r <@ V.tally(bb, BP.kL, BP.cL, BP.sk);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

(* inline dishonest browser experiment *)
local lemma db_db_2  &m:
  Pr[DB_L(MV(E),A_DB,Left).main() @ &m : res] =
  Pr[DB_L2(MV(E),A_DB,Left).main() @ &m : res].
proof.
  byequiv =>/>.
  proc.  
  sim. 
  wp 18 18.
  conseq (: ={BP.sk, BP.bb_b, BP.b_b, BP.kL, BP.cL, BP.regL, vbb, glob A_DB, glob E}/\
            (forall x, BP.lrb{2}.[x] <> None => BP.lrb{2}.[x] = Some x))=>/>.
  progress.
  have ->: (fun (x : ident * mask * code) =>
     if lrb_R.[x] <> None then oget lrb_R.[x] else x) = idfun.
    rewrite /idfun fun_ext /(==). 
    move => x; case (lrb_R.[x] <> None).
    + by move => Hx; rewrite (H x Hx) oget_some.
    + by done.
    by rewrite map_id.
  while (={ BP.bb_b, BP.kL, glob E, vbb, i}); first by sim.
  wp; call (: ={BP.sk, BP.bb_b, BP.b_b, BP.b_0, BP.votL, BP.wL,
                BP.kL, BP.cL, BP.regL, BP.pk, glob E, BP.corL, BP.regL}/\
            (forall x, BP.lrb{2}.[x] <> None => BP.lrb{2}.[x] = Some x))=>/>.
  + proc. sp; if=>//=. inline*; wp. 
    by call(: true); rnd; auto; progress.
  + by proc; wp. 
  + proc. sp; if=>//=. 
    inline*; auto; progress.
    + smt.
  + by proc; wp.
  + by proc; wp.    
  + by proc; wp. 
  inline*; wp; call(: true).
  while (={i, BP.kL}); first by sim.  
  auto=>/>; progress.
  by move: H1; rewrite map0P. 
qed.

(* inline tally, and replace encryption by credential *)
module BT_L(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, m, t, v, ubb, w,bb, vbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.kk  <- [];
    BP.lrm <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;
    fbb <- filter ((mem BP.regL)\o get_id) (first_id bb);
    i <- 0;
    ubb <- [];

    while (i < size fbb){
     (id,m,t) <- nth witness fbb i;
     if (verify (oget BP.kL.[id]) m t) {
       w <- BP.wL.[id];
       v <- m - oget w;
       ubb <- ubb++[v];
     }
     i <- i +1;
    }

    r <- Rho ubb;

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b,BP.bb_b);
    return b;
  }
}.

(* inline dishonest browser experiment *)
local lemma bpriv_bt_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[DB_L2(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BT_L(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  while (={ubb, BP.regL, fbb} /\ 0<= i0{1}/\
         i0{1} = i{2} /\ kL{1} = BP.kL{2}/\
         (forall x, x \in fbb{1}=>
            BP.wL.[x.`1]{2} = decrypt (oget cL.[x.`1]{1}) sk0{1})).
    seq 1 1: (={ubb, BP.regL, fbb, m, t} /\
              0 <= i0{1} /\ id1{1} = id{2} /\
              (id1,m,t){1} = nth witness fbb{1} i0{1} /\
              i0{1} = i{2} /\
              kL{1} = BP.kL{2} /\
              (forall x, x \in fbb{1} => 
                BP.wL{2}.[x.`1] = decrypt (oget cL{1}.[x.`1]) sk0{1}) /\
              i0{1} < size fbb{1} /\ i{2} < size fbb{2}).
      by auto=>/>; smt(@Core).
    if =>//=. 
    + exists * (glob E){1}, sk0{1}, cL{1}, id1{1};
      elim* => ge skx wx ix.
      wp; call{1} (dec_decrypt ge skx (oget wx.[ix])).
      auto=>/>; progress. 
      + rewrite -(H1 (id{2}, m{2}, t{2}) _).  
          by rewrite H0 (mem_nth witness) H H2. 
        by done.
      + rewrite (addz_ge0 _ _ H lez01). (* smt (@Int). *)
    - auto=>/>; progress; 
        first by rewrite (addz_ge0 _ _ H lez01).
  wp; while{1} (0<=i{1} /\
                vbb{1} = filter (fun (x: ident*mask*code),
                         verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3) (take i{1} BP.bb_b{1}))
               (size BP.bb_b{1} - i{1}); progress.
    auto; progress.
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int). 
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int).
  wp; conseq (: ={glob A_DB, BP.regL, BP.cL, BP.lrb,  BP.kL, BP.b_0,BP.b_b,BP.bb_b, BP.votL}/\
                            (forall x, x \in BP.regL{1} => 
                              BP.wL.[x]{2} = decrypt (oget BP.cL.[x]{1}) BP.sk{1})
                )=>/>.
  progress. 
  + by rewrite take0. 
  + by rewrite lezNgt subz_gt0 in H1. 
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + move: H2; rewrite mem_filter /(\o) /get_id. 
    move => [Hm Hr].
    by rewrite (H _ Hm).
  + move: H2.
    have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b}/\
          keypair BP.pk{2} BP.sk{2}/\
          (forall (x : ident), x \in BP.regL{1} => 
            BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1})).
  + proc. 
    sp; if=>//=.
    inline*; wp.   
    seq 3 3: ( o{1} = None /\
               ={id, o, id0, pk, w0, glob LR, glob E, BP.corL, BP.regL, 
                 BP.cL, BP.pk, BP.wL, BP.sk, BP.votL, BP.kL, BP.lrb,
                BP.bb_b, BP.b_0, BP.b_b} /\
                keypair BP.pk{2} BP.sk{2} /\
               (forall (x : ident), x \in BP.regL{1} => 
                 BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1}) /\
               (id{1} \in Voters) /\ ! (id{1} \in BP.regL{1}) /\
               id0{1} = id{1} /\ pk{1} = BP.pk{1}).  
      by rnd; auto=>/>. 
    exists * BP.pk{1}, BP.sk{2}, w0{1};
       elim* => pkx skx mx.
    pose kp:= keypair pkx skx. 
    cut em_kp: kp \/ !kp by rewrite orbN. 
    elim em_kp. 
    + move => h.
      call{1} (enc_decrypt_two skx pkx mx h).
      auto; progress. 
      move: H4; rewrite ?mem_cat ?mem_seq1; move => Hm. 
      smt (getP). 
    - move => h.
      conseq(_: _ ==> !(keypair BP.pk{2} BS.sk{2}))=>/>.
      call{1} Ee_ll; call{2} Ee_ll. 
      by auto=>/>.
  + by proc; auto=>/>. 
  + by proc; sp; if=>//=; inline*; wp; call(: true); wp.
  + by proc; wp.
  + by proc; wp.          
  + by proc; wp. 
  wp; call{1} (kgen_keypair). 
  while (={i, BP.kL}); first by sim.
  by auto. 
qed.


module BSP_L(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb, bb, vbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.lrm <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

  
    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;

    fbb <- filter ((mem BP.regL)\o get_id) bb;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    cbb <- filter ((mem BP.corL) \o get_id) fbb;

    uhbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g hbb; 

    ucbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g cbb; 

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

local lemma uniq_last (L: (ident* mask*code) list):
  uniq (map get_id L) =>
  last_id L = L.
proof.
  elim: L=>//=. 
  move => x L HuL.
  elim => [HnM Hu]. 
  have ->: !has (pred1 x.`1 \o get_id) L.
    rewrite hasPn /get_id /(\o) /pred1.
    move: HnM; rewrite mapP /get_id negb_exists //=.
    smt (@Logic).
  by rewrite //= (HuL Hu).
qed.

local lemma uniq_first (L: (ident* mask*code) list):
  uniq (map get_id L) =>
  first_id L = L.
proof.
  rewrite /first_id. rewrite -rev_uniq -map_rev.
  move => Hu. 
  by rewrite (uniq_last (rev L) Hu) revK. 
qed.

local lemma bt_bsp_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BT_L(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSP_L(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  wp; while{1} ( 0 <= i{1} /\
                 let f = fun (x: ident*mask*code), 
                              verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 in
                 let g = fun (x: ident*mask*code),
                             (x.`2 - oget BP.wL{1}.[x.`1]) in
                 ubb{1} = map g (filter f (take i{1} fbb{1})))
               (size fbb{1} -i{1}); progress.
    auto=>/>; progress. 
    + rewrite (addz_ge0 _ _ H lez01).  
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1.     
    + smt(@Int). 
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1. 
    + smt(@Int).
  wp; call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
                BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b}/\
             uniq (map get_id BP.bb_b{1})/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => x.`1 = (oget BP.lrb{1}.[x]).`1)/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => 
                   verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)).
  + by proc; sp; if=>//=; inline*; wp; call(: true); auto.
  + by proc; wp.
  + proc; sp; if =>//=; inline*; wp; call(: true); auto; progress. 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H6; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H0 x Hx). 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H6; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H1 x Hx). 
  + proc; auto; progress.
    by rewrite map_cat cat_uniq H //=.
  + proc; auto; progress.
    by rewrite map_cat cat_uniq H //=.
  + by proc.
  wp; call(: true).
  while(={i, BP.kL}); first by sim.
  auto=>/>; progress. search map0.
  + by move: H1; rewrite map0P. 
  + by move: H1; rewrite map0P.
  + by rewrite take0.
  + by rewrite lezNgt subz_gt0 in H7.  
  + rewrite take_oversize; first by rewrite StdOrder.IntOrder.lerNgt.  
    pose c:= fun (x: ident*mask*code),
             if lrb_R.[x] <> None then oget lrb_R.[x] else x.
    pose v:= fun (x : ident * mask * code) =>
                       verify (oget kL_R.[x.`1]) x.`2 x.`3.
    pose L:= filter (mem regL_R \o get_id)
             (map c (filter v bb_b_R)).
    rewrite -map_cat (* -filter_cat*). 
    pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]). 

    have Hp: perm_eq L (filter (predC (mem corL_R \o get_id)) L ++
                        filter (mem corL_R \o get_id) L) 
      by rewrite perm_eq_sym perm_filterC.
    have Hpp: perm_eq (map g (filter v L)) 
                      (map g (filter v (filter (predC (mem corL_R \o get_id)) L ++
                                        filter (mem corL_R \o get_id) L))).
      by rewrite perm_eq_map perm_eq_filter Hp. 
    have Hpx: perm_eq (map g L) 
                      (map g (filter (predC (mem corL_R \o get_id)) L ++
                                        filter (mem corL_R \o get_id) L)).
      by rewrite perm_eq_map Hp. 
    rewrite uniq_first. 
    have Huf: uniq (map get_id (filter v bb_b_R));
      first by rewrite (uniq_map_filter _ _ _ H3). 
    rewrite -map_comp.  
    have ->: (forall (L: (ident*mask*code) list),
              (forall x, x \in L => x.`1 = (c x).`1)=>
              map (get_id \o c) L = map get_id L).
    + move => A HA. 
      rewrite -eq_in_map /(\o) /get_id. 
      by move => a Ha; rewrite (HA a Ha).
    + move => x. rewrite mem_filter. move => [Hvx Hmx].
      rewrite /c. 
      case (lrb_R.[x] <> None).
      + by move => Hv; rewrite -(H4 x Hv).
      + by done.
    + by done.
    have ->: (filter v (filter (mem regL_R \o get_id) (map c (filter v bb_b_R)))) =
              L.
      rewrite /L.
    rewrite ?filter_map. 
    rewrite -?filter_predI. 
    rewrite (eq_in_filter (predI (predI (preim c v) (preim c (mem regL_R \o get_id))) v)
                          (predI (preim c (mem regL_R \o get_id)) v) bb_b_R).
      move => x Hxm. rewrite /predI /preim.
      progress. 
      rewrite /c.
      case (lrb_R.[x] <> None).
      + by move => Hv; rewrite /v -(H4 x Hv) (H5 x Hv).
      + by done.
    by done.
    by rewrite (rho_perm _ _ Hpx).                     
qed.


module BSPP_L(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        if BP.lrb.[x] <> None 
                        then oget BP.votL.[x.`1]
                        else (x.`2 - oget BP.wL.[x.`1])  in
            map h (filter f hbb);

    (* find the position of an element that has valid token,
       but was not generated by Ovote, and is for a different message *)
    BP.ibad<- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
              find (fun (x: ident*mask*code),
                         BP.lrb.[x] = None) (filter f hbb);
    BP.bad <- BP.ibad < size uhbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b,BP.bb_b);
    return b;
  }
}.

local lemma bsp_bspp_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSP_L(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  call(: true).
  wp.
  conseq (: ={glob A_DB,BP.wL, BP.kL, BP.lrb, BP.corL, BP.regL, BP.votL, BP.b_b,BP.bb_b} /\
            (forall (x: ident*mask*code), 
             x.`1 \in BP.corL{1} => BP.lrb{1}.[x] = None)/\
            (forall x, BP.lrb{1}.[x] <> None =>
               BP.votL{1}.[x.`1] = Some ((oget BP.lrb{1}.[x]).`2 - (oget BP.wL{1}.[x.`1])))/\
            (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               (oget BP.lrb{1}.[x]).`1 = x.`1)/\
            (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 /\
               verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)
            )=>/>.  
  progress.  
  pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
  pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]).
  pose f2:= (predC (mem corL_R \o get_id)).
  pose f3:= (mem regL_R \o get_id).
  pose s:= (fun (x : ident * mask * code) =>
                 if lrb_R.[x] <> None then oget lrb_R.[x] else x).
  pose s':= fun (x : ident * mask * code) =>
                if lrb_R.[x] <> None then oget votL_R.[x.`1]
                else x.`2 - oget wL_R.[x.`1].
  rewrite -?filter_predI. 
  (* honest voters *)
  have ->: map g (filter (predI f2 f3) (map s (filter f1 bb_b_R))) = 
           map s' (filter (predI (predI f1 f2) f3) bb_b_R).
    rewrite filter_map.
    have ->: preim s (predI f2 f3) = (predI f2 f3).
      rewrite /preim fun_ext /(==); move => x.
      rewrite /predI /f2 /f3 /s  /predC /get_id  /(\o) //=.
      have ->: (if lrb_R.[x] <> None then oget lrb_R.[x] else x).`1 = x.`1.
        case (lrb_R.[x] <> None). 
        + exact (H1 x).
        + done.
      by done.
    rewrite -?filter_predI.
    have ->: (predI (predI f2 f3) f1) = (predI (predI f1 f2) f3).
      by rewrite fun_ext /predI /(==); move => x; smt(@Logic).
    rewrite -map_comp.
    rewrite (eq_map (g\o s) s').
      move => x; rewrite /(\o) /g /s /s' //=. 
      case (lrb_R.[x] <> None). 
      + move => Hx.
        by rewrite (H0 x Hx) oget_some -(H1 x Hx). 
      - by done.
    by done.
    (* corrupt voters *)
    pose f2':= (mem corL_R \o get_id).
    have ->: map g (filter (predI f2' f3) (map s (filter f1 bb_b_R))) = 
             map g (filter (predI (predI f1 f2') f3) bb_b_R).
      rewrite filter_map.
      have ->: preim s (predI f2' f3) = (predI f2' f3).
        rewrite /preim fun_ext /(==); move => x.
        rewrite /predI /f2' /f3 /s  /predC /get_id  /(\o) //=.
        have ->: (if lrb_R.[x] <> None then oget lrb_R.[x] else x).`1 = x.`1.
          case (lrb_R.[x] <> None). 
          + exact (H1 x).
          + done.
        by done.
      rewrite -?filter_predI.
      have ->: (predI (predI f2' f3) f1) = (predI (predI f1 f2') f3).
        by rewrite fun_ext /predI /(==); move => x; smt(@Logic).
      have ->: forall (L: (ident * mask*code) list), 
               (forall x, x \in L => lrb_R.[x] = None) => 
         map s  L = L. 
         + by elim =>//=; smt.
         + move => x. rewrite mem_filter /p /predI /f2' /get_id /(\o) //=.  
           move => [[[Hf1 Hc] Hf3] _]. 
           by rewrite (H x Hc).
      by done.

    by done.
 
  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b} /\
          (forall (x: ident*mask*code), 
             !x.`1 \in BP.regL{1} \/ !x.`1 \in Voters => BP.lrb{1}.[x] = None)/\
          (forall (x: ident*mask*code), 
             x.`1 \in BP.corL{1} => BP.lrb{1}.[x] = None)/\
          (forall x, BP.lrb{1}.[x] <> None =>
               BP.votL{1}.[x.`1] = Some ((oget BP.lrb{1}.[x]).`2 - (oget BP.wL{1}.[x.`1])))/\
          (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               (oget BP.lrb{1}.[x]).`1 = x.`1)/\
          (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 /\
               verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)).
  + proc.
    sp; if =>//=. 
    inline*; wp.
    call(: true).
    rnd; auto; progress. 
    + smt (mem_cat). 
    + smt (getP).
  + proc.
    auto=>/>; progress. 
    move: H8; rewrite mem_cat //=.
    case(x.`1 \in BP.corL{2}). 
    + by move => Hm; rewrite (H0 x Hm). 
    - rewrite //=; move => Hn Hx.
      by cut := H1 x; rewrite Hx H6.      
  + proc. 
    sp; if =>//=.
    inline*; wp.
    call(: true).
    auto=>/>; progress.
    + smt (getP).
    + smt (getP). 
    + move: H8.
      case (result_R).
      + move => Hresult.
        pose A:= (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
              token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}])).
        rewrite ?getP //=. 
        case (x= A). 
        + rewrite /A; move => HxA.
          have ->: x.`1 = id{2} by rewrite HxA. 
          move => HA_none.
          by rewrite oget_some /A //=  mask_sub_def
                    -mask_addA mask_addfN mask_addf0. 
        - rewrite /A; move => HxA Hx_none. 
          rewrite (H1 x Hx_none).
          case (x.`1  = id{2}). 
          + smt. 
          - done.
      - move => Hresult.
        pose A:= (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
                  token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}])).
        pose B:= (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
              token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}])).
        rewrite ?getP. 
        case (x= B). 
        + rewrite /A; move => HxA.
          have ->: x.`1 = id{2} by rewrite HxA. 
          move => HA_none.
          by rewrite oget_some /A //=  mask_sub_def
                    -mask_addA mask_addfN mask_addf0. 
        - rewrite /A; move => HxA Hx_none. 
          rewrite (H1 x Hx_none).
          case (x.`1  = id{2}). 
          + smt. 
          - done. 
    + smt (getP). smt (getP). smt (getP).  
  + by proc; wp.
  + by proc; wp.    
  + by proc; wp.   
  inline*; wp.   
  call(: true).
  while(={i, BP.kL}); first by sim. 
  auto=>/>; progress.
  + rewrite DInterval.dinter_ll. smt (@Int n_pos). 
  + by rewrite map0P.
  + by move: H3; rewrite map0P.
  + by move: H3; rewrite map0P.
  + by move: H3; rewrite map0P.
  + by move: H3; rewrite map0P.
qed.

local lemma bspp_badsplit_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res /\ !BP.bad] +
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad].
proof.
  have ->: Pr[BSPP_L(MV(E), A_DB, LR).main() @ &m : res] =
           Pr[BSPP_L(MV(E), A_DB, LR).main() @ &m : (res /\ !BP.bad) \/ (res /\ BP.bad)].
  rewrite Pr[mu_eq] //=. smt (@Logic).
  rewrite Pr[mu_or] //=. smt.
qed.

(* replace all honest votes *)
module BV(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h (filter f hbb);

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
              has (fun (x: ident*mask*code),
                         BP.lrb.[x] = None) (filter f hbb);
    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad;
  }
}.

(* there is no fake token for honest voters, so use the left vote *)
local lemma badsplit_bv_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res /\ !BP.bad] =
  Pr[BV(MV(E),A_DB,LR).main() @ &m : res ].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc. 

  seq 26 23: (={glob A_DB, BP.bad, BP.b_b, BP.bb_b}/\ (!BP.bad{1} => ={r})).  
  wp.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.b_b, BP.bb_b, BP.lrb, BP.kL, BP.votL, BP.wL})=>/>.
  progress.
  + by rewrite has_find size_map.
  + pose f1:= (fun (x : ident * mask * code) =>
                verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose f2:= (mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id).
    rewrite -?filter_predI.
    pose pc:= (predI (predI f1 f2) f3).
    pose ph:= (predI (predI f1 (predC f2)) f3).
    pose C:= map (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1])
               (filter pc (first_id bb_b_R)).
    have ->:  map (fun (x : ident * mask * code) =>
                 if lrb_R.[x] <> None then oget votL_R.[x.`1]
                 else x.`2 - oget wL_R.[x.`1]) 
            (filter ph bb_b_R) =
            map (fun (x : ident * mask * code) => oget votL_R.[x.`1])
            (filter ph bb_b_R).   
      rewrite -(eq_in_map (fun (x : ident * mask * code) =>
                          if lrb_R.[x] <> None then oget votL_R.[x.`1]
                          else x.`2 - oget wL_R.[x.`1])
                     (fun (x : ident * mask * code) => oget votL_R.[x.`1])
                     (filter ph  bb_b_R)).
        move => x Hxm. 
        rewrite //=. 
        move: H. 
          rewrite -/f1 -/f2 -/f3 -?filter_predI -/ph.
          rewrite size_map.
          rewrite -has_find hasPn //=.
          move => Hall.
        cut Hlrb:= Hall x Hxm.
        by rewrite Hlrb.
      by done.
  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.b_b,BP.bb_b, BP.b_0, BP.b_b}); first 6 by sim.
  inline*; wp; call(: true).
  while( ={i, BP.kL}); first by sim. 
  auto=>/>; progress. 
  + by rewrite DInterval.dinter_ll; smt (n_pos).

  case (!BP.bad{1}). 
  + by call(: true); auto=>/>. 
  - call{1} (Aa2_ll (<: BSPP_L(MV(E), A_DB, LR).O)). 
    call{2} (Aa2_ll (<: BSPP_L(MV(E), A_DB, LR).O)).
    by auto=>/>.
qed.

(* RIGHT SIDE of DB goes to BV, too *)
(* inline tally, and replace encryption by credential *)
module BT_R(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, m, t, v, ubb, w, vbb, hbb, cbb, uhbb, fbb2;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.kk  <- [];
    BP.lrm <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    fbb <- filter ((mem BP.regL)\o get_id) vbb;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                       oget  BP.b_0.[x.`1] in
            map h hbb;
    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    i <- 0;
    ubb <- [];
    fbb2 <- filter ((mem BP.regL)\o get_id) (first_id (uhbb++cbb));
    while (i < size fbb2){
     (id,m,t) <- nth witness fbb2 i;
     if (verify (oget BP.kL.[id]) m t) {
       w <- BP.wL.[id];
       v <- m - oget w;
       ubb <- ubb++[v];
     }
     i <- i +1;
    }

    r <- Rho ubb;

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b,BP.bb_b);
    return b;
  }
}.

(* inline dishonest browser experiment *)
local lemma bpriv_bt_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[DB_R(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BT_R(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  while (={ubb, BP.regL} /\ 0<= i0{1}/\
         i0{1} = i{2} /\ kL{1} = BP.kL{2}/\ fbb0{1} = fbb2{2} /\
         (forall x, x \in fbb0{1}=>
            BP.wL.[x.`1]{2} = decrypt (oget cL.[x.`1]{1}) sk0{1})).
    seq 1 1: (={ubb, BP.regL, m, t} /\
              0 <= i0{1} /\ id1{1} = id{2} /\ fbb0{1} = fbb2{2} /\
              (id1,m,t){1} = nth witness fbb0{1} i0{1} /\
              i0{1} = i{2} /\
              kL{1} = BP.kL{2} /\
              (forall x, x \in fbb0{1} => 
                BP.wL{2}.[x.`1] = decrypt (oget cL{1}.[x.`1]) sk0{1}) /\
              i0{1} < size fbb0{1}).
      by auto=>/>; smt(@Core).
    if =>//=. 
    + exists * (glob E){1}, sk0{1}, cL{1}, id1{1};
      elim* => ge skx wx ix.
      wp; call{1} (dec_decrypt ge skx (oget wx.[ix])).
      auto=>/>; progress. 
      + rewrite -(H1 (id{2}, m{2}, t{2}) _).  
          by rewrite H0 (mem_nth witness) H H2. 
        by done.
      + rewrite (addz_ge0 _ _ H lez01). (* smt (@Int). *)
    - auto=>/>; progress; 
        first by rewrite (addz_ge0 _ _ H lez01).
  wp; while{1} (0<=i{1} /\
                vbb{1} = filter (fun (x: ident*mask*code),
                         verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3) (take i{1} BP.bb_b{1}))
               (size BP.bb_b{1} - i{1}); progress.
    auto; progress.
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int). 
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int).
  wp; conseq (: ={glob A_DB, BP.regL, BP.cL, BP.lrb, BP.corL, 
                  BP.kL, BP.b_0,BP.b_b,BP.bb_b, BP.votL}/\
                            (forall x, x \in BP.regL{1} => 
                              BP.wL.[x]{2} = decrypt (oget BP.cL.[x]{1}) BP.sk{1})
                )=>/>.
  progress. 
  + by rewrite take0. 
  + by rewrite lezNgt subz_gt0 in H1. 
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + move: H2; rewrite mem_filter /(\o) /get_id. 
    move => [Hm Hr].
    by rewrite (H _ Hm).
  + move: H2.
    have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b}/\
          keypair BP.pk{2} BP.sk{2}/\
          (forall (x : ident), x \in BP.regL{1} => 
            BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1})).
  + proc. 
    sp; if=>//=.
    inline*; wp.   
    seq 3 3: ( o{1} = None /\
               ={id, o, id0, pk, w0, glob LR, glob E, BP.corL, BP.regL, 
                 BP.cL, BP.pk, BP.wL, BP.sk, BP.votL, BP.kL, BP.lrb,
                BP.bb_b, BP.b_0, BP.b_b} /\
                keypair BP.pk{2} BP.sk{2} /\
               (forall (x : ident), x \in BP.regL{1} => 
                 BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1}) /\
               (id{1} \in Voters) /\ ! (id{1} \in BP.regL{1}) /\
               id0{1} = id{1} /\ pk{1} = BP.pk{1}).  
      by rnd; auto=>/>. 
    exists * BP.pk{1}, BP.sk{2}, w0{1};
       elim* => pkx skx mx.
    pose kp:= keypair pkx skx. 
    cut em_kp: kp \/ !kp by rewrite orbN. 
    elim em_kp. 
    + move => h.
      call{1} (enc_decrypt_two skx pkx mx h).
      auto; progress. 
      move: H4; rewrite ?mem_cat ?mem_seq1; move => Hm. 
      smt (getP). 
    - move => h.
      conseq(_: _ ==> !(keypair BP.pk{2} BS.sk{2}))=>/>.
      call{1} Ee_ll; call{2} Ee_ll. 
      by auto=>/>.
  + by proc; auto=>/>. 
  + by proc; sp; if=>//=; inline*; wp; call(: true); wp.
  + by proc; wp.
  + by proc; wp.          
  + by proc; wp. 
  wp; call{1} (kgen_keypair). 
  while (={i, BP.kL}); first by sim.
  by auto. 
qed.


module BSP_R(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb, vbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.lrm <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

  
    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    fbb <- filter ((mem BP.regL)\o get_id) vbb;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    cbb <- filter ((mem BP.corL) \o get_id) fbb;

    uhbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            let h = fun (x: ident*mask*code),
                        oget  BP.b_0.[x.`1] in
            map g (map h hbb); 

    ucbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g cbb; 

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.


local lemma bt_bsp_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BT_R(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSP_R(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  wp; while{1} ( 0 <= i{1} /\
                 let f = fun (x: ident*mask*code), 
                              verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 in
                 let g = fun (x: ident*mask*code),
                             (x.`2 - oget BP.wL{1}.[x.`1]) in
                 ubb{1} = map g (filter f (take i{1} fbb2{1})))
               (size fbb2{1} -i{1}); progress.
    auto=>/>; progress. 
    + rewrite (addz_ge0 _ _ H lez01).  
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1.     
    + smt(@Int). 
    + rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1. 
    + smt(@Int).

  wp; call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
                BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b}/\
             uniq (map get_id BP.bb_b{1})/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => x.`1 = (oget BP.lrb{1}.[x]).`1)/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => 
                   verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 
                          (oget BP.lrb{1}.[x]).`3)/\
             (forall (x: ident*mask*code),
                x \in BP.bb_b{1} => !x.`1 \in BP.corL{1} =>
                BP.b_0.[x.`1]{1} <> None) /\
             (forall x, BP.votL{1}.[x] <> None <=> BP.b_0{1}.[x] <> None) /\
             (forall (x:ident*mask*code), 
               BP.b_0{1}.[x.`1] <> None => x.`1 = (oget BP.b_0{1}.[x.`1]).`1)/\
             (forall x, x \in filter (predC (mem BP.corL{1} \o get_id)) BP.bb_b{1} =>
               BP.b_0{1}.[x.`1] <> None) /\
             (forall x, BP.b_0{1}.[x] <> None => 
               (predI (fun (x : ident * mask * code) =>
                            verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3)
                      (mem BP.regL{1} \o get_id)) 
                      (oget BP.b_0{1}.[x]))
             (* /\ (forall x, BP.votL{1}.[x] <> None => x \in BP.regL{1})*) ).
  + proc; sp; if=>//=; inline*; wp; call(: true); auto; progress.
    + smt. 
    + (* cut Hx:= H7 x _; first by rewrite (H3 x) H12.*)
      rewrite /(\o) mem_cat //=. smt.
  + proc; auto; progress.
    + smt (mem_cat). 
    + move: H11. rewrite mem_filter .
      case (get_id x = id{2}). smt.
      move => Hx. 
      have ->: predC (mem (BP.corL{2} ++ [id{2}]) \o get_id) x = 
               predC (mem BP.corL{2} \o get_id) x.
        by rewrite /predC /(\o) mem_cat //= Hx //=. print mem_filter.
      rewrite -(mem_filter (predC (mem BP.corL{2} \o get_id)) x BP.bb_b{2}). 
      by exact (H5 x). 
  + proc; sp; if =>//=; inline*; wp; call(: true); auto; progress. 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H11; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H0 x Hx). 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H11; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H1 x Hx). 
    + smt (getP). smt (getP). smt(getP). smt(getP). smt (getP).
      smt (getP). smt (getP).
  + proc; auto; progress.
    + by rewrite map_cat cat_uniq H //=.
    + smt (mem_cat).
    + smt (@List).
  + proc; auto; progress.
    + by rewrite map_cat cat_uniq H //=.
    + smt (mem_cat). 
    + smt (@List).
  + by proc.
  wp; call(: true).
  while(={i, BP.kL}); first by sim.
  auto=>/>; progress. 
  + by move: H1; rewrite map0P. 
  + by move: H1; rewrite map0P.
  + by rewrite map0P.
  + by rewrite map0P.
  + by move: H1; rewrite map0P.
  + by move: H1; rewrite map0P.
  + by move: H1; rewrite map0P.
  + by rewrite take0.
  + by rewrite lezNgt subz_gt0 in H15.  
  + rewrite take_oversize; first by rewrite StdOrder.IntOrder.lerNgt. 
    pose v:= fun (x : ident * mask * code) =>
                       verify (oget kL_R.[x.`1]) x.`2 x.`3.
    pose r:= (mem regL_R \o get_id).
    pose c:= (mem corL_R \o get_id).
    pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]). 
    pose b0:= (fun (x : ident * mask * code) => oget b_0_R.[x.`1]).
    rewrite -?filter_predI.   
    pose vr:= (predI v r).
    have ->: (predI (predI (predC c) r) v) = predI (predC c) vr.
      rewrite /vr /predI fun_ext /(==); move => x; smt.
    have ->: (predI (predI c r) v) = predI c vr.
      rewrite /vr /predI fun_ext /(==); move => x; smt.
    pose L:= (filter vr bb_b_R).
    have ->: (filter (predI (predC c) vr) bb_b_R) = filter (predC c) L. 
      by rewrite filter_predI.
    have ->: (filter (predI c vr) bb_b_R) = filter c L. 
      by rewrite filter_predI.
    rewrite uniq_first. 
    rewrite map_cat cat_uniq.
    have Huf1: uniq (map get_id (filter c L));
      first by rewrite /L -filter_predI (uniq_map_filter _ _ _ H6). 
    rewrite Huf1 //=.
    have Hf: forall (A: (ident * mask * code) list), 
                 (forall x, x \in A => (b0 x).`1 = x.`1) =>
                 map (get_id \o b0) A = map get_id A.
         elim =>//=. smt.
    have Huf2: uniq (map get_id (map b0 (filter (predC c) L))).
      rewrite /L -filter_predI.
      rewrite -map_comp. 
      rewrite Hf.
      move => x; rewrite mem_filter /b0. move => [[px py] Hm].
      cut := H11 x _. by rewrite (H9 x Hm _); first by move: px; rewrite /predC /c //=.
      move=> Hkl. by rewrite -Hkl. 
      rewrite /L  (uniq_map_filter _ _ _ H6). 
    rewrite Huf2 //=.
    rewrite -map_comp. 
    rewrite Hf.
    move => x; rewrite mem_filter /b0 /L mem_filter. move => [pc [pvr Hm]].
    cut := H11 x _. by rewrite (H9 x Hm _); first by move: pc; rewrite /predC /c //=.
    move=> Hkl. by rewrite -Hkl. 
    rewrite hasP //=  negb_exists //=.
    move => a. 
    case (a \in map get_id (filter c L)).
    + rewrite mapP //=. elim => z. rewrite mem_filter /get_id /c /predC //= /get_id /(\o). 
      move => [[Hz HL] Haz].
      rewrite mapP ?negb_exists  //=.
      move => y. rewrite mem_filter.
    rewrite negb_and ?mapP ?negb_exists /get_id //=. smt.
    - by rewrite mapP //=. search filter map. 
    rewrite filter_cat (filter_map b0 vr) -?filter_predI.
    have ->: (predI (predI vr c) vr) = (predI vr c).
      by rewrite /predI fun_ext /(==); move => x; smt( @Logic). 
    have ->: (predI (predI (preim b0 vr) (predC c)) vr) = 
             (predI (predI (preim b0 vr) vr) (predC c)). 
      pose a:= (preim b0 vr). 
      pose b:= predC c.
      rewrite /predI fun_ext /(==); move => x; smt (@Logic).
      rewrite filter_predI.
(*    have Hadmit: forall x, x \in filter (predC c) bb_b_R =>
      b_0_R.[x.`1] <> None by admit.
    have Hadmit2: forall x, b_0_R.[x] <> None => vr (oget b_0_R.[x]) by admit. *)
    move: H12 H13; rewrite -/v -/r -/c -/vr; move => Hadmit Hadmit2.
    rewrite (eq_in_filter (predI (preim b0 vr) vr) vr).
      move => x Hmx.
      cut := Hadmit2 x.`1 _; first by rewrite (Hadmit x Hmx).
      rewrite /preim /b0. smt.
    have ->: (predI (predC c) vr) = (predI vr (predC c)).
      by rewrite /predI fun_ext /(==) ; move => x; smt (@Logic).
    rewrite -filter_predI.
    have ->: (predI vr (predC c)) = (predI vr (predC c))
      by rewrite fun_ext /(==) /predI; move => x; smt (@Logic).
    have ->: (predI vr c) = (predI c vr).
      by rewrite fun_ext /(==) /predI; move => x; smt (@Logic).
    by rewrite map_cat. 
qed.


module BSPP_R(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1]  in
            map h (filter f hbb);

    (* find the position of an element that has valid token,
       but was not generated by Ovote, and is for a different message *)
    BP.ibad<- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
              find (fun (x: ident*mask*code),
                         BP.lrb.[x] = None) (filter f hbb);
    BP.bad <- BP.ibad < size uhbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b,BP.bb_b);
    return b;

  }
}.

local lemma bsp_bspp_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSP_R(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  call(: true).
  wp.
  conseq (: ={glob A_DB,BP.wL, BP.kL, BP.lrb, BP.corL, BP.regL, BP.votL, BP.b_b,BP.bb_b} /\
            (forall x,
               x \in BP.bb_b{1} => predC (mem BP.corL{1} \o get_id) x =>
               BP.votL{1}.[x.`1] = Some ((oget BP.b_0{1}.[x.`1]).`2-(oget BP.wL{1}.[x.`1])))/\ 
            (forall x,
               x \in BP.bb_b{1} => predC (mem BP.corL{1} \o get_id) x =>
                BP.b_0{1}.[x.`1] <> None)/\
            (forall (x:ident*mask*code), 
               BP.b_0{1}.[x.`1] <> None => x.`1 = (oget BP.b_0{1}.[x.`1]).`1)
         )=>/>.  
  progress.  
  pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
  pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]).
  pose f2:=(mem corL_R \o get_id).
  pose f3:= (mem regL_R \o get_id).
  pose s:= (fun (x : ident * mask * code) => oget b_0_L.[x.`1]).
  pose s':= (fun (x : ident * mask * code) => oget votL_R.[x.`1]).
  rewrite -?filter_predI. 
  (* corrupt part *)
  have ->: (predI (predI f2 f3) f1) = (predI (predI f1 f2) f3). 
    rewrite fun_ext /(==) /predI; move => x; smt (@Logic).
  (* honest part *)
  rewrite -map_comp.
  have ->: (predI (predI (predC f2) f3) f1) = (predI (predI f1 (predC f2)) f3).
    by rewrite fun_ext /(==) /predI; move => x; smt (@Logic). 
  have ->: map (g \o s) (filter (predI (predI f1 (predC f2)) f3) bb_b_R) =
           map s' (filter (predI (predI f1 (predC f2)) f3) bb_b_R). print eq_in_map.
   rewrite -(eq_in_map (g \o s) s' ). 
   move => x.
   rewrite mem_filter /g /s /s' /(\o) //=.
   move => [[[Hp1x Hp2x] Hp3x] Hmx].
   cut Hv:= H x Hmx _; first rewrite -/f2 Hp2x.
   rewrite Hv oget_some.
   cut Hb01:= H1 x _. by rewrite (H0 x Hmx _); first rewrite -/f2 Hp2x.
   by rewrite -Hb01.   
  by done. 

  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b} /\
          (forall (x: ident), 
             !x \in BP.regL{1} \/ !x \in Voters => BP.b_0{1}.[x] = None)/\
          (forall x, BP.votL{1}.[x] <> None <=> BP.b_0{1}.[x] <> None) /\
          (forall x,
               BP.b_0{1}.[x] <> None =>
               BP.votL{1}.[x] = Some ((oget BP.b_0{1}.[x]).`2-(oget BP.wL{1}.[x])))/\ 
            (forall x,
               x \in BP.bb_b{1} => predC (mem BP.corL{1} \o get_id) x =>
                BP.b_0{1}.[x.`1] <> None)/\
            (forall (x:ident*mask*code), 
               BP.b_0{1}.[x.`1] <> None => x.`1 = (oget BP.b_0{1}.[x.`1]).`1)).
  + proc.
    sp; if =>//=. 
    inline*; wp.
    call(: true).
    rnd; auto; progress. 
    + smt (mem_cat). 
    + smt (getP).
  + proc.
    auto=>/>; progress. 
    + smt (mem_cat).
  + proc. 
    sp; if =>//=.
    inline*; wp.
    call(: true).
    auto=>/>; progress.
    + smt (getP).
    + smt (getP).
    + smt (getP). smt. smt. smt (getP). 
  + proc; auto; progress.  
    + case (x \in BP.bb_b{2}). smt. smt.
  + proc; auto; progress.  
    + case (x \in BP.bb_b{2}). smt. smt.     
  + by proc; wp.  
  inline*; wp.   
  call(: true).
  while(={i, BP.kL}); first by sim. 
  auto=>/>; progress.
  + rewrite DInterval.dinter_ll. smt (@Int n_pos). 
  + by rewrite map0P.
  + by rewrite map0P.
  + by rewrite map0P.
  + by move: H3; rewrite map0P.
  + by move: H3; rewrite map0P.
  + by rewrite (H9 x.`1 _); first by rewrite (H10 x H12 H13).
qed.


local lemma bspp_badsplit_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res] =
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res /\ !BP.bad] +
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad].
proof.
  have ->: Pr[BSPP_R(MV(E), A_DB, LR).main() @ &m : res] =
           Pr[BSPP_R(MV(E), A_DB, LR).main() @ &m : (res /\ !BP.bad) \/ (res /\ BP.bad)].
  rewrite Pr[mu_eq] //=. smt (@Logic).
  rewrite Pr[mu_or] //=. smt.
qed.

(*
(* replace all honest votes *)
module BV(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h (filter f hbb);

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
              has (fun (x: ident*mask*code),
                         BP.lrb.[x] = None) (filter f hbb);
    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad;
  }
}. *)

(* there is no fake token for honest voters, so use the left vote *)
local lemma badsplit_bv_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res /\ !BP.bad] =
  Pr[BV(MV(E),A_DB,LR).main() @ &m : res ].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc. 

  seq 26 23: (={glob A_DB, BP.bad, BP.b_b, BP.bb_b}/\ (!BP.bad{1} => ={r})).  
  wp.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.b_b, BP.bb_b, BP.lrb, BP.kL, BP.votL, BP.wL})=>/>.
  progress.
  + by rewrite has_find size_map.
(*  + pose f1:= (fun (x : ident * mask * code) =>
                verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose f2:= (mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id).
    rewrite -?filter_predI.
    pose pc:= (predI (predI f1 f2) f3).
    pose ph:= (predI (predI f1 (predC f2)) f3).
    pose C:= map (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1])
               (filter pc (first_id bb_b_R)).
    have ->:  map (fun (x : ident * mask * code) =>
                 if lrb_R.[x] <> None then oget votL_R.[x.`1]
                 else x.`2 - oget wL_R.[x.`1]) 
            (filter ph bb_b_R) =
            map (fun (x : ident * mask * code) => oget votL_R.[x.`1])
            (filter ph bb_b_R).   
      rewrite -(eq_in_map (fun (x : ident * mask * code) =>
                          if lrb_R.[x] <> None then oget votL_R.[x.`1]
                          else x.`2 - oget wL_R.[x.`1])
                     (fun (x : ident * mask * code) => oget votL_R.[x.`1])
                     (filter ph  bb_b_R)).
        move => x Hxm. 
        rewrite //=. 
        move: H. 
          rewrite -/f1 -/f2 -/f3 -?filter_predI -/ph.
          rewrite size_map.
          rewrite -has_find hasPn //=.
          move => Hall.
        cut Hlrb:= Hall x Hxm.
        by rewrite Hlrb.
      by done. *)
  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.b_b,BP.bb_b, BP.b_0, BP.b_b}); first 6 by sim.
  inline*; wp; call(: true).
  while( ={i, BP.kL}); first by sim. 
  auto=>/>; progress. 
  + by rewrite DInterval.dinter_ll; smt (n_pos).

  case (!BP.bad{1}). 
  + by call(: true); auto=>/>. 
  - call{1} (Aa2_ll (<: BSPP_R(MV(E), A_DB, LR).O)). 
    call{2} (Aa2_ll (<: BSPP_R(MV(E), A_DB, LR).O)).
    by auto=>/>.
qed. 

(*local lemma bspp_bspp_LR(LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad] =
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad].
proof.
  byequiv=>/>.  
  proc.
  seq 26 26: (={glob A_DB, BP.bad, BP.b_b, BP.bb_b} /\ 
              (!BP.bad{2} => ={r}))=>/>.
  wp 20 20.
  conseq (: ={hbb, fbb, BP.bb_b, BP.b_b, BP.lrb, BP.corL, BP.votL, 
              BP.kL, BP.wL, glob A_DB}).
  progress.
  + smt. 
  + pose fv:= (fun (x : ident * mask * code) => 
                 verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose fc:= (fun (x : ident * mask * code) => oget votL_R.[x.`1]).
    pose fd:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]).
    pose fvd:= (fun (x : ident * mask * code) =>
        if lrb_R.[x] <> None then oget votL_R.[x.`1]
        else x.`2 - oget wL_R.[x.`1]).
    move: H; rewrite -/fv -/fc. 
    rewrite size_map -has_find. rewrite hasPn //=.
    move => Hfx.
    have ->: map fvd (filter fv hbb_R) = map fc (filter fv hbb_R).
    rewrite -eq_in_map.
      move => x Hxm. 
      by rewrite /fvd /fc (Hfx x Hxm) //=.
    by done.
  sim.
  sim.
  case (!BP.bad{1}). 
  + by call(: true); auto=>/>. 
  - call{1} (Aa2_ll (<: BSPP_R(MV(E), A_DB, LR).O)). 
    call{2} (Aa2_ll (<: BSPP_R(MV(E), A_DB, LR).O)).
  auto; progress. by auto=>/>.
*)

(* -----------------------------------------------------------------*)
(* guess bad oracle *)
clone  Ex_Plug_and_Pray as WV with
  type tin  <- unit,
  type tres <- bool,
  op bound  <- size Voters 
proof * . (* proof bound_pos. *)
realize bound_pos. by apply n_pos. (* link q_pos and n_pos *) qed. 

module Bbad(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var i, id, fbb, hbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    BP.bb <- BP.bb_b;
    fbb <- filter ((mem BP.regL)\o get_id) BP.bb;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;

    BP.bad <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
              has (fun (x: ident*mask*code),
                         BP.lrb.[x] = None) (filter f hbb);

    return BP.bad;
  }
}.


local lemma badsplit_bbad_L (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_L(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad] <=
  Pr[Bbad(MV(E),A_DB,LR).main() @ &m : BP.bad ].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc.
  conseq (: ={BP.bad})=>/>.  
  call{1} (Aa2_ll (<: BSPP_L(MV(E), A_DB, LR).O)). 
  wp; progress.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.b_b, BP.bb_b, BP.lrb, BP.kL, BP.votL, BP.wL} /\
            uniq BP.regL{1} /\ 
            (forall x, x\in BP.regL{1} => x \in Voters))=>/>.
  progress.
  + have Hr: size regL_R <= size Voters. 
      rewrite uniq_leq_size.
      + by rewrite H. 
      + by rewrite /(<=) /H1. 
 
    pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose f2:= predC (mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id).
    rewrite -?filter_predI.
    pose p:= (predI (predI f1 f2) f3). 
    by rewrite size_map has_find.
    
  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.b_b, BP.bb_b, BP.b_b} /\
          uniq BP.regL{1} /\
          (forall x, x\in BP.regL{1} => x \in Voters)).
  + proc.
    sp; if=>//=.
    inline*; wp; call(: true).    
    rnd; auto=>/>; progress.  
    + by rewrite cat_uniq //=.
    + smt (mem_cat).
  + by proc; wp.     
  + by proc; sp; if=>//=; inline*; wp; call(: true); auto.
  + by proc; wp.
  + by proc; wp.      
  + by proc; wp.  
  call(: ={glob E}); first by sim.
  while (={i, BP.kL}); first by sim.
  wp; rnd. 
  by auto=>/>.
qed.


local lemma badsplit_bbad_R (LR<: LorR{A_DB, BP, E}) &m:
  Pr[BSPP_R(MV(E),A_DB,LR).main() @ &m : res /\ BP.bad] <=
  Pr[Bbad(MV(E),A_DB,LR).main() @ &m : BP.bad ].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc.
  conseq (: ={BP.bad})=>/>.  
  call{1} (Aa2_ll (<: BSPP_R(MV(E), A_DB, LR).O)). 
  wp; progress.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.b_b, BP.bb_b, BP.lrb, BP.kL, BP.votL, BP.wL} /\
            uniq BP.regL{1} /\ 
            (forall x, x\in BP.regL{1} => x \in Voters))=>/>.
  progress.
  + have Hr: size regL_R <= size Voters. 
      rewrite uniq_leq_size.
      + by rewrite H. 
      + by rewrite /(<=) /H1. 
 
    pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose f2:= predC (mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id).
    rewrite -?filter_predI.
    pose p:= (predI (predI f1 f2) f3). 
    by rewrite size_map has_find.
    
  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.b_b, BP.bb_b, BP.b_b} /\
          uniq BP.regL{1} /\
          (forall x, x\in BP.regL{1} => x \in Voters)).
  + proc.
    sp; if=>//=.
    inline*; wp; call(: true).    
    rnd; auto=>/>; progress.  
    + by rewrite cat_uniq //=.
    + smt (mem_cat).
  + by proc; wp.     
  + by proc; sp; if=>//=; inline*; wp; call(: true); auto.
  + by proc; wp.
  + by proc; wp.      
  + by proc; wp.  
  call(: ={glob E}); first by sim.
  while (={i, BP.kL}); first by sim.
  wp; rnd. 
  by auto=>/>.
qed.

module BBbad(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var i, id, fbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    SB.ikt <- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter (predI ((mem BP.regL)\o get_id)
                         (predI (fun (x: ident*mask*code),
                                      BP.lrb.[x] = None)
                                (predC ((mem BP.corL) \o get_id)))) 
           BP.bb_b; 
    SB.ikt <- NewFMap.oflist (map (fun (x: ident*mask*code), (x.`1, (x.`2,x.`3))) fbb);
    BP.bad <- let f = fun (x: ident) (y: mask * code), 
                        verify (oget BP.kL.[x]) y.`1 y.`2 in
              find f SB.ikt <> None;


    return BP.bad;
  }
}.


local lemma has_to_has['a] (L :'a list) g k:
  has g (filter k L) => has g L.
proof. 
  rewrite ?hasP. 
  elim => x. rewrite mem_filter. 
  move => [[Hk Hm] Hg].
  by exists x. 
qed.

local lemma bbad_bkey (LR<: LorR{SB, A_DB, BP, E}) &m:
  Pr[Bbad(MV(E),A_DB,LR).main() @ &m : BP.bad ]<=
  Pr[BBbad(MV(E),A_DB,LR).main() @ &m : BP.bad].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc.
  wp; progress.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.b_b, BP.bb_b, BP.lrb, BP.kL, BP.votL, BP.wL}/\
           uniq (map get_id BP.bb_b{1}))=>/>.
  progress.
  + move: H0.
    pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
    pose f2:= predC (mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id).
    pose g:= (fun (x0 : ident * mask * code) => lrb_R.[x0] = None).
    rewrite -?filter_predI.
    pose L:= bb_b_R.
    pose s:= (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3))).
    have ->: (predI f3 (predI g f2)) = (predI g (predI f2 f3))
      by rewrite /predI fun_ext /(==); move => x; smt (@Logic).
    have ->: (predI (predI f1 f2) f3) = (predI f1 (predI f2 f3))
      by rewrite /predI fun_ext /(==); move => x; smt (@Logic).      
    have ->: forall (X: (ident*mask*code) list),
             has g (filter (predI f1 (predI f2 f3)) X) =
             has f1 (filter (predI g (predI f2 f3)) X).
      by elim =>//=; move => x X; smt (@Logic @List).         
    pose S:= (filter (predI g (predI f2 f3)) L).
    rewrite -find_none. rewrite ?hasP /f1 //=. 
    elim => x [HxS HxT].
    exists x.`1. 
    have ->: (x.`1 \in dom (NewFMap.oflist (map s S))).
      rewrite dom_oflist -map_comp /s /(\o) //=. 
      smt (mapP).
    rewrite //= get_oflist. 
    cut := mem_assoc_uniq (map s S) x.`1 (x.`2,x.`3) _ . 
    + (* uniq ids in S *)
      rewrite -map_comp /s /(\o) //=.
      rewrite /S. 
      by rewrite uniq_map_filter -/get_id /L H. 
    have ->: (x.`1, (x.`2, x.`3)) \in map s S = x \in S.
      rewrite /s; smt (mapP). 
    rewrite HxS //=.
    move => HaS.
    by rewrite HaS oget_some.

  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_b} /\
          uniq BP.regL{1} /\ uniq (map get_id BP.bb_b{1}) /\
          (forall x, x\in BP.regL{1} => x \in Voters)).
  + proc.
    sp; if=>//=.
    inline*; wp; call(: true).
    by rnd; auto =>/>; progress; smt (cat_uniq).

  + proc.
    by auto.     
  + proc.
    sp; if=>//=.
    inline*; wp; call(: true).
    by auto.
  + proc; auto; progress.
    by rewrite map_cat cat_uniq //=.
  + proc; auto; progress.
    by rewrite map_cat cat_uniq //=.
  + by proc; wp.      
  call(: ={glob E}); first by sim.
  while (={i, BP.kL}); first by sim.
  wp; rnd. wp.
  by auto=>/>.
qed.


(* move the find on bb to find the key *)
module BKey(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var i, id, fbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    SB.ikt <- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];
    BP.jbad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter (predI ((mem BP.regL)\o get_id)
                         (predI (fun (x: ident*mask*code),
                                      BP.lrb.[x] = None)
                                (predC ((mem BP.corL) \o get_id)))) 
           BP.bb_b; 
     SB.ikt <- NewFMap.oflist (map (fun (x: ident*mask*code), (x.`1, (x.`2,x.`3))) fbb);
     BP.bad <- let f = fun (x: ident) (y: mask * code), 
                        verify (oget BP.kL.[x]) y.`1 y.`2 in
               find f SB.ikt <> None;
     BP.ibad <- let f = fun (x: ident) (y: mask * code), 
                        verify (oget BP.kL.[x]) y.`1 y.`2 in
                index (oget (find f SB.ikt)) (map fst BP.kk);

    return BP.bad;
  }
}.

local lemma find_to_find (L : (ident* mask*code) list) 
                         (S: (ident * idkey) list) 
                         (g: ident * mask * code -> bool):
  (forall x, x \in L => exists b, (x.`1,b) \in S) =>
  find g L < size L => 
  find (fun (x : ident * idkey) => 
            x.`1 = (nth witness L (find g L)).`1) S < size S.
proof.
  move => HxLS HfL.
  pose a:= (nth witness L (find g L)).
  have HaL: a \in L; first by rewrite /a mem_nth HfL find_ge0. 
  cut := HxLS a HaL. elim => b HbS.
  rewrite -has_find hasP. exists (a.`1, b).
  by rewrite HbS.
qed.
 
local lemma take_drop_uniq' (L :'a list) x i:
   uniq L =>
   x \in take (i+1) L =>
     !x \in drop (i+1) L.
proof.
  cut HL:= cat_take_drop (i+1) L.
  move=> HuL; rewrite -HL in HuL.
  move: HuL. rewrite cat_uniq. 
  elim => Hut [Hhas Hud].
  move: Hhas; rewrite hasPn; move => Hhas Htake.
  by cut := Hhas x; rewrite Htake //=.  
qed. 
 
local lemma bbad_bkey_two (LR<: LorR{SB,A_DB, BP, E}) &m:
  Pr[BBbad(MV(E),A_DB,LR).main() @ &m : BP.bad]<=
  Pr[BKey(MV(E),A_DB,LR).main() @ &m : BP.bad /\ 0<= BP.ibad < size Voters].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc.
  conseq (: ={BP.bad} /\
            (BP.bad{1} => 0 <= BP.ibad{2} < size Voters))=>/>. 
    progress; smt. 
  wp; progress.
  conseq (: ={glob A_DB, BP.regL, BP.corL, BP.bb_b, BP.b_b, 
              BP.lrb, BP.kL, BP.votL, BP.wL, BP.kk} /\
           size BP.kk{1} = size Voters /\
            (*(forall x, x\in BP.kk{1} => x.`1 \in Voters)*)
           (forall a, a \in BP.regL{1} => exists b, (a,b) \in BP.kk{1}))=>/>.
  progress. 
  + by rewrite find_ge0. 
  + move: H1.
    pose L:= (NewFMap.oflist
     (map (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3)))
        (filter
           (predI (mem regL_R \o get_id)
              (predI (fun (x : ident * mask * code) => lrb_R.[x] = None)
                 (predC (mem corL_R \o get_id)))) ( bb_b_R)))).
    pose g:= (fun (x : ident) (y : mask * code) => verify (oget kL_R.[x]) y.`1 y.`2).
    pose K:= (map (fun (p : ident * idkey) => p.`1) kk_R).
    move => Hf.
    have Hi: index (oget (find g L)) K < size K.
      rewrite index_mem. 
      rewrite -find_none in Hf. search oflist has.
      have Ho: forall x, x\in dom L => x \in K. 
        move => x.
        rewrite /L dom_oflist -map_comp.
        have ->: ((fun (p : ident * (mask * code)) => p.`1) \o
                 fun (x0 : ident * mask * code) => (x0.`1, (x0.`2, x0.`3))) = get_id
        by rewrite /get_id /(\o).
      rewrite mapP.
      elim => y [HyF Hyx].
      move: HyF. rewrite mem_filter.
      elim => [[HmR _] HyB].
      move: HmR; rewrite /(\o) //= -Hyx /K mapP //=.
      move => HmR.
      cut:= H0 x HmR.
      elim => b HbM. 
      by exists (x,b); rewrite HbM.
    move: Hf; rewrite has_find; elim => x [Hx Hd].
    by rewrite Hx oget_some (Ho x Hd).
    rewrite /K -H.  
    smt. (* slow *)

  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_b} /\
          (forall a, a \in Voters => BP.kL{1}.[a] <> None)/\
          (forall a, a \in BP.regL{1} => a \in Voters)).
  + proc.
    sp; if=>//=.
    inline*; wp; call(: true).
    rnd; auto; progress. 
    rewrite mem_cat mem_seq1 in H5.
    case (a \in BP.regL{2}).  
    + by move => Hm; rewrite (H0 a Hm). 
    - by move => Hm; move: H5; rewrite Hm //=; move => Ha.
  + by proc; wp.     
  + by proc; sp; if=>//=; inline*; wp; call(: true); auto.
  + by proc; wp.
  + by proc; wp.
  + by proc; wp.

  call(: ={glob E}); first by sim.
  while (={i, BP.kL, BP.kk}/\
         0<= i{1} /\
         (forall a, a \in (take i{1} Voters) => 
             BP.kL{1}.[a] <> None)/\
         (forall x, x \in (drop i{1} Voters) =>
            BP.kL{1}.[x] = None)/\
         (forall a,  BP.kL{1}.[a] <> None => 
            exists b, (a,b) \in BP.kk{1}) /\
         (size BP.kk{1} = size (take i{1} Voters))).
    inline*; wp.
    rnd; auto; progress.
    + by rewrite (addz_ge0 _ _ H lez01).
    + move: H8; rewrite (take_nth witness); first by rewrite H H4. 
      rewrite mem_rcons. smt (getP). 
    + have Hxm: x \in drop i{2} Voters. 
         rewrite (drop_nth witness i{2}); first by rewrite H H4.     
         by rewrite -cat1s mem_cat H8.
       cut Hxn:= H1 x Hxm.
       cut := take_drop_uniq' Voters (nth witness Voters i{2}) i{2} _ _.  
       + by apply Voters_uniq.    
       + rewrite (take_nth witness); first by rewrite H H4. 
         by rewrite mem_rcons mem_head. 
       smt(getP).
    + move: H8. rewrite ?getP //= oget_some. 
      case(a = nth witness Voters i{2}).
      + move => Ha. exists kL.
        by rewrite -Ha mem_cat.
      - move => Ha Hn.
        cut := H2 a Hn. elim => b Hb.
        by exists b; rewrite mem_cat Hb. 
    + rewrite (take_nth witness); first by rewrite H H4. 
      by rewrite size_rcons size_cat H3.
    
  wp; rnd{2}; rnd. 
  auto=>/>; progress. 
  + rewrite DInterval.dinter_ll; smt (@Int n_pos). 
  + by move: H2; rewrite take0.
  + by rewrite map0P.
  + by rewrite map0P.
  + by rewrite take0. 
  + smt.
  + by rewrite H8 take_oversize; first by rewrite lezNgt H2.
  + cut Hv:= H10 a H11.
    by cut := (H7 a _); first by rewrite (H9 a Hv).
qed.

local lemma badsplit_guess (LR<: LorR{A_DB, BP, E}) &m:
  1%r/ (size Voters) %r * 
  Pr[BKey(MV(E),A_DB,LR).main() @ &m : BP.bad /\ 0<= BP.ibad < size Voters]
  = 
  Pr[WV.Guess(BKey(MV(E),A_DB,LR)).main() @ &m :
        BP.bad /\ 0<= BP.ibad <size Voters /\ fst res = BP.ibad].
proof.
  print glob BKey(MV(E),A_DB). 
  cut := WV.PBound (BKey(MV(E),A_DB,LR))
          (fun g b => let (ikt, jbad, ibad, bow,kk, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv16, gv17, gv18, gE, gA, gLR)= g in
             (bow /\ 0<=  ibad < size Voters))
          (fun g b => let (ikt, jbad, ibad, bow, kk, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv16, gv17, gv18, gE, gA, gLR)= g in
           ibad) () &m. 
  simplify. 
  move => ->. 
  rewrite Pr[mu_eq]; progress.
  + by rewrite H2 H0 //= H1. 
  + by rewrite H0 H1. 
qed.

module BIbad(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var i, id, fbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk <- BP.kk ++ [(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter (predI ((mem BP.regL)\o get_id)
                         (predI (fun (x: ident*mask*code),
                                      BP.lrb.[x] = None)
                                (predC ((mem BP.corL) \o get_id)))) 
           (BP.bb_b); 
     SB.ikt <- NewFMap.oflist (map (fun (x: ident*mask*code), (x.`1, (x.`2,x.`3))) fbb);
     BP.bad <- (let f = fun (x: ident) (y: mask * code), 
                        verify (oget BP.kL.[x]) y.`1 y.`2 in
               has f SB.ikt) /\
               BP.ibad = (let f = fun (x: ident) (y: mask * code), 
                                      verify (oget BP.kL.[x]) y.`1 y.`2 in
                          index (oget (find f SB.ikt)) (map fst BP.kk));

    return BP.bad;
  }
}.

local lemma guess_ibad (LR<: LorR{SB, A_DB, BP, E}) &m:
  Pr[WV.Guess(BKey(MV(E),A_DB,LR)).main() @ &m :
        BP.bad /\ 0<= BP.ibad <size Voters /\ fst res = BP.ibad] <= 
  Pr[BIbad(MV(E),A_DB,LR).main() @ &m : BP.bad].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>. 
  proc.
  inline*. 
  wp;  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_b}); first 6 by sim.
  wp; call(: true). 
  while (={BP.kL, BP.kk}/\ i0{1}=i{2}); first by sim.
  wp; rnd{1}; rnd{1}; wp; rnd.
  auto; progress. 
  + smt(@DInterval n_pos).
  + smt (@NewFMap).
qed.

module Bnth(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var i, id, fbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk <- BP.kk ++ [(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    fbb <- filter (predI ((mem BP.regL)\o get_id)
                         (predI (fun (x: ident*mask*code),
                                      BP.lrb.[x] = None)
                                (predC ((mem BP.corL) \o get_id)))) 
           BP.bb_b; 
     SB.ikt <- NewFMap.oflist (map (fun (x: ident*mask*code), (x.`1, (x.`2,x.`3))) fbb);
     BP.bad <- let x= nth witness Voters BP.ibad in
               let y = oget SB.ikt.[x] in
               SB.ikt.[x] <> None  /\
               verify (oget BP.kL.[x]) y.`1 y.`2;

    return BP.bad;
  }
}.

local lemma find_get ['a 'b] (L: ('a,'b) fmap) f:
  find f L <> None => L.[oget (find f L)] <> None by
  smt (@NewFMap).

local lemma ibad_bnth (LR<: LorR{SB, A_DB, BP, E}) &m:
  Pr[BIbad(MV(E),A_DB,LR).main() @ &m : BP.bad] <=
  Pr[Bnth(MV(E),A_DB,LR).main() @ &m : BP.bad].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>. 
  proc.
  inline*; wp.
  call(:  ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_b}); first 6 by sim.
  wp; call(: true). 
  while (={i, BP.kL, BP.kk}/\ 0<= i{1} /\ 
         (map fst BP.kk{1}) = (take i{1} Voters)). 
    auto=>/>; progress. 
    + by rewrite (addz_ge0 _ _ H lez01). 
    + rewrite (take_nth witness); first by rewrite H H1. 
      rewrite map_cat -cats1. smt (getP).
  wp; rnd.
  auto; progress. 
  + by rewrite take0.
  + move: H H0 H3 H5.
  pose L:= (NewFMap.oflist
                 (map (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3)))
                    (filter
                       (predI (mem regL_R \o get_id)
                          (predI
                             (fun (x : ident * mask * code) =>
                                lrb_R.[x] = None)
                             (predC (mem corL_R \o get_id))))
                       (bb_b_R)))). 
  pose f:= (fun (x : ident) (y : mask * code) =>
            verify (oget kL_R.[x]) y.`1 y.`2).
  have Ho:  map fst kk_R = Voters by smt. 
  rewrite Ho.
  pose K:= Voters.
  pose i:= (index (oget (find f L)) K).
  move => H H0 H3 H5. 
  cut := H5; rewrite find_none.
  rewrite /i.
  pose a:= find f L. 
  rewrite nth_index. smt.
  by rewrite /a; move => Hf; rewrite find_get.  

  + move: H H0 H3 H5.
  pose L:= (NewFMap.oflist
                 (map (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3)))
                    (filter
                       (predI (mem regL_R \o get_id)
                          (predI
                             (fun (x : ident * mask * code) =>
                                lrb_R.[x] = None)
                             (predC (mem corL_R \o get_id))))
                       (bb_b_R)))). 
  pose f:= (fun (x : ident) (y : mask * code) =>
            verify (oget kL_R.[x]) y.`1 y.`2).
  have Ho:  map fst kk_R = Voters by smt. 
  rewrite Ho.
  pose K:= Voters.
  pose i:= (index (oget (find f L)) K).
  move => H H0 H3 H5. 
  have ->: (verify (oget kL_R.[nth witness K i]) (oget L.[nth witness K i]).`1
                   (oget L.[nth witness K i]).`2) =
           f (nth witness K i) (oget L.[nth witness K i]).
    by rewrite /f.
  have Hs: size K = size Voters by smt.  
  have Hi: i < size K. smt. 
  cut := Hi. rewrite /i index_mem. 
  pose a:= oget (find f L).  
  move: Hi; rewrite /i -/a; move => Hi.
  move => HaK.
  have ->: (nth witness K (index a K)) = a. 
    by rewrite nth_index 1: HaK.
  by rewrite /a get_find H5.
qed.


(* LINK IBAD to key generation, and remove the find *)
module TOA(V:VotingScheme, A: DB_Adv, LR: LorR, O:TSec_Oracle) ={

  module OK ={
    proc reg    = DB_Oracles (V,LR).reg 
    proc castH  = DB_Oracles (V,LR).castH
    proc castC  = DB_Oracles (V,LR).castC
    proc board  = DB_Oracles (V,LR).board

    proc corrupt(id: ident): (cred *idkey) option={
    var k; 
    var w <- None;

      if ( id \in Voters /\ id \in BP.regL /\ BP.votL.[id] = None){
        if (id = BP.id){
          k <- O.corrupt();
          w <- Some (oget BP.wL.[id], k);
        }else{
          w <- Some (oget BP.wL.[id], oget BP.kL.[id]);
        }
        if ( !id \in BP.corL ){
          BP.corL <- BP.corL ++ [id];
        }
      }
      return w;   
    }
    proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
      var b0, t0, b1, t1, w, k, l_or_r, t, b;
      var o <- None;

      if ( id \in Voters /\ id \in BP.regL /\  BP.votL.[id] = None/\ !id \in BP.corL){
        w <- BP.wL.[id];
        b0     <@ V.vote(id, v0, oget w);
        b1     <@ V.vote(id, v1, oget w);
        l_or_r <@ LR.l_or_r();
        b <- l_or_r?b0:b1; 
        if (id = BP.id){
        (* the id that creates the fake token *)
          b <- l_or_r?b0:b1; 
          t <@ O.token(b.`2); 
          BP.votL.[id] <- v0;
          BP.lrb.[(l_or_r?(b0.`1,b0.`2, oget t):(b1.`1,b1.`2, oget t))] <- (b0.`1,b0.`2, oget t);
          BP.b_0.[id] <- (b0.`1,b0.`2,oget t);
          BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,oget t):(b1.`1,b1.`2,oget t));
          o <- Some (l_or_r?(b0.`1,b0.`2,oget t):(b1.`1,b1.`2,oget t));
         }else{
          (* a different id *)
          k <- BP.kL.[id];
          BP.votL.[id] <- v0;
          t0     <@ V.token(b0.`2, oget k);
          t1     <@ V.token(b1.`2, oget k);
          BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
          BP.b_0.[id] <- (b0.`1,b0.`2,t0);
          BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
          o <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
        }
      }
      return o; 
    }(* end vote *)
  }

  proc pre(): unit ={
    var i, id;

    BP.kk <- [];
    i <-0;
    BP.kL <- map0;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    SB.ikt <- map0;

    BP.bad <- false;
    BP.ibad<$[0..size Voters-1];

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    while (i < BP.ibad){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk <- BP.kk ++ [(id, oget BP.kL.[id])];
      i <- i + 1;
    }
    BP.id <- nth witness Voters BP.ibad;

  }

  proc ftoken(): mask*code ={
    var i, id, m, t, fbb;
    (* i = BP.ibad gen key BTS.k *)
    i <- BP.ibad+1;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk <- BP.kk ++ [(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(OK).a1(BP.pk);
    fbb <- filter (predI ((mem BP.regL)\o get_id)
                         (predI (fun (x: ident*mask*code),
                                      BP.lrb.[x] = None)
                                (predC ((mem BP.corL) \o get_id)))) 
           (BP.bb_b); 
     SB.ikt <- NewFMap.oflist (map (fun (x: ident*mask*code), (x.`1, (x.`2,x.`3))) fbb);
     (* BP.id = nth witness (map fst BP.kk) BP.ibad *)
     (m,t) <- oget SB.ikt.[BP.id];
     BP.bad <- let x= nth witness Voters BP.ibad in
               let y = oget SB.ikt.[x] in
               verify (oget BP.kL.[x]) y.`1 y.`2;

    return (m,t);

  }
}.


local lemma nth_token (LR<: LorR{BTS, SB, A_DB, BP, E}) &m:
  Pr[Bnth(MV(E),A_DB,LR).main() @ &m : BP.bad] <=
  Pr[TSec_Exp(TOA(MV(E),A_DB,LR)).main() @ &m : res].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>. 
  proc.
  inline*.
  (* swap{1} kL, kk, ibad, i, while *) 
  swap{1} 3 -2. swap{1} 10 -9. swap{1} 8 -7. swap{1} [15..16] -11.
  (* swap{2} kk,i,kL,  ibad,  while, BP.id, BTS.k,i0, while,  qv, cor *)
  swap{2} [5..7] -4. swap{2} 14 -13. swap{2} [21..25] - 14. swap{2} [5..6] 5.
  seq 5 9 : (={glob A_DB, glob E, glob LR, BP.ibad} /\
             map fst BP.kk{1} = Voters /\
             BP.kk{1} = take BP.ibad{1} BP.kk{2} ++ 
                        [(BP.id,BTS.k)]{2} ++ drop BP.ibad{1} BP.kk{2}/\
             (oget BP.kL{1}.[BP.id{2}]) = BTS.k{2} /\
             (forall x, x<> BP.id{2} => BP.kL{1}.[x] = BP.kL{2}.[x])/\
             0 <= BP.ibad{2} < size Voters). 
    splitwhile{1} 5: i< BP.ibad.
    unroll{1} 6.
    rcondt{1} 6.
    + progress.
      while (i <= BP.ibad /\ BP.ibad < size Voters).
      + auto=>/>; progress. smt (@Int). 
      wp; rnd.
      auto=>/>; progress. smt (@DInterval). smt(@DInterval). smt  (@Int).
    seq 5 5: ( ={glob A_DB, glob E, glob LR, BP.ibad, BP.kk, BP.kL} /\ 
               size BP.kk{1} = BP.ibad{2}/\
               uniq (map fst BP.kk{1}) /\ i{1} = BP.ibad{2} /\ 0<= BP.ibad{1}/\
               (forall a, a \in (take i{1} Voters) => a \in map fst BP.kk{1})/\
               (forall x, x \in (drop i{1} Voters) => !x \in map fst BP.kk{1})/\
               BP.ibad{2} < size Voters/\
               map fst BP.kk{1} = take i{1} Voters ).
    while (={i, BP.ibad, BP.kk, BP.kL} /\
             0<= i{1} <= BP.ibad{1} /\ BP.ibad{1} < size Voters /\
             size BP.kk{1} = i{1} /\
             uniq (map fst BP.kk{1}) /\
            (forall a, a \in (take i{1} Voters) => a \in map fst BP.kk{1})/\
            (forall x, x \in (drop i{1} Voters) => !x \in map fst BP.kk{1})/\
            map fst BP.kk{1} = take i{1} Voters).
      wp; rnd; auto=>/>; progress.
      + smt (@Int). smt (@Int). smt (@Int @List). (* slow *)
      + rewrite map_cat cat_uniq H2 //=.
        have Hxm: nth witness Voters (size BP.kk{2}) \in drop (size BP.kk{2}) Voters. 
          rewrite (drop_nth witness (size BP.kk{2})); first smt(@Int).    
          by rewrite mem_head.  
        by rewrite (H4 _ Hxm).
      + rewrite getP_eq map_cat mem_cat //=.
        move: H9; rewrite (take_nth witness); first by smt (@Int).
        rewrite mem_rcons.
        smt(@List).
      + rewrite getP_eq map_cat mem_cat //=.
        have Hxm: x \in drop (size BP.kk{2}) Voters. 
          rewrite (drop_nth witness (size BP.kk{2})); first smt (@Int).    
          by rewrite -cat1s mem_cat H9. 
        cut Hxn:= H4 x Hxm.
        rewrite Hxn //=. 
        cut := take_drop_uniq' Voters (nth witness Voters (size BP.kk{2})) (size BP.kk{2}) _ _.  
        + apply Voters_uniq.       
        + rewrite (take_nth witness) ; first by rewrite H H6.
          by rewrite mem_rcons mem_head.
        smt (@List).
      + rewrite (take_nth witness); first rewrite H H6.  
        by rewrite map_cat getP_eq //= cats1 H5. 
      + smt (@ Int).
    auto=>/>; progress.
    + smt (@DInterval). smt(@DInterval). 
    + by rewrite take0. 
    + by rewrite take0.
    + exact n_pos.
    + smt (@Int).
    + smt (@Int). smt (@Int). 

    while ( ={BP.ibad} /\ i{1} = i0{2} /\ 0<= BP.ibad{1} <= i{1} /\
            BP.ibad{1} <= size BP.kk{2} /\ size BP.kk{2} <= i{1}/\
            uniq (map fst BP.kk{1}) /\
            (forall a, a \in (take i{1} Voters) => a \in map fst BP.kk{1})/\
            (forall x, x \in (drop i{1} Voters) => !x \in map fst BP.kk{1})/\
            BP.kk{1} = take BP.ibad{1} BP.kk{2} ++ [(BP.id,BTS.k)]{2} ++ drop BP.ibad{1} BP.kk{2}/\
            map fst BP.kk{1} = take i{1} Voters/\
            (oget BP.kL{1}.[BP.id{2}]) = BTS.k{2}/\
            BP.id{2} = nth witness Voters BP.ibad{1}/\
            (forall x, x<> BP.id{2} => BP.kL{1}.[x] = BP.kL{2}.[x])). 
      wp; rnd; auto=>/>; progress.         
      + smt(@Int). smt(@Int size_cat). smt(@Int size_cat).        
      + move: H5 H6 H3 H4.
        pose A:= (take BP.ibad{2} BP.kk{2} ++ [(BP.id{2}, BTS.k{2})] ++
            drop BP.ibad{2} BP.kk{2}).
        move => H5 H6 H3 H4.
        rewrite map_cat cat_uniq H3 //=.
        have Hxm: nth witness Voters i0{2} \in drop i0{2} Voters. 
          rewrite (drop_nth witness i0{2}). by rewrite H8; smt(@Int).
          by rewrite mem_head.  
       by rewrite (H5 _ Hxm).
     + move: H4 H5 H3 H6.
      pose A:= (take BP.ibad{2} BP.kk{2} ++ [(BP.id{2}, BTS.k{2})] ++
            drop BP.ibad{2} BP.kk{2}).
       move => H4 H5 H3 H6.
       rewrite map_cat //=. 
       move: H10; rewrite (take_nth witness); first by smt(@Int).
       rewrite mem_rcons.
       smt(@List).
     + move: H4 H5 H3 H6.
       pose A:= (take BP.ibad{2} BP.kk{2} ++ [(BP.id{2}, BTS.k{2})] ++
            drop BP.ibad{2} BP.kk{2}).
       move => H4 H5 H3 H6.
       rewrite map_cat //=. 
       have Hxm: x \in drop i0{2} Voters. 
         move: H10; rewrite (drop_nth witness i0{2}). smt(@Int).    
         smt. 
       cut Hxn:= H5 x Hxm. 
       cut := take_drop_uniq' Voters (nth witness Voters i0{2}) i0{2} _ _.  
        + exact Voters_uniq.       
        + rewrite (take_nth witness); first by smt(@Int).
          by rewrite mem_rcons. 
       smt(@List).
     + rewrite ?getP_eq oget_some //=.
       have ->: take BP.ibad{2} (BP.kk{2} ++ [(nth witness Voters i0{2}, kL)]) =
                take BP.ibad{2} BP.kk{2}.
         rewrite take_cat. smt(@Int @List).
       rewrite drop_cat. smt(@Int @List).
     + rewrite (take_nth witness); first by smt(@Int). 
       by rewrite -cats1 //= -H6 ?map_cat. 
     + smt.  smt(getP). 
    wp; rnd; wp.

    auto=>/>; progress.
    + smt(@Int). smt(@Int).
    + rewrite map_cat cat_uniq H //=. smt .
    + rewrite map_cat mem_cat //=.
      move: H6; rewrite (take_nth witness); first by smt(@Int).
      rewrite mem_rcons. smt (mem_head).
    + rewrite map_cat mem_cat //=.
      have Hx: x \in drop (size BP.kk{2}) Voters.
        rewrite (drop_nth witness); first by smt(@Int).
        smt (drop_nth).
      rewrite (H2 x Hx) //=.
      cut := take_drop_uniq' Voters (nth witness Voters (size BP.kk{2})) (size BP.kk{2}) _ _.  
        + exact Voters_uniq.        
        + rewrite (take_nth witness); first by rewrite H0 H3. 
          by rewrite mem_rcons mem_head.
        smt.
    + smt.
    + rewrite map_cat //= H4. 
      rewrite (take_nth witness). by rewrite H0 H3. 
      by rewrite cats1. 
    + smt(getP). smt(getP). smt.
  (* end seq for keys: kk *)

  wp; progress.
  conseq (: ={BP.corL, BP.ibad, BP.regL, BP.bb_b}/\
            0 <= BP.ibad{2} < size Voters/\ 
            oget BP.kL{1}.[BP.id{2}] = BTS.k{2}/\
            (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
            (forall (x: mask*code), 
               BP.lrb{1}.[(BP.id{2},x.`1,x.`2)] = None => 
                  !x \in BTS.mt{2})/\
            (!BP.id{2} \in BP.corL{1} => !BTS.cor{2})) =>/>.
  progress.
  + move: H7 H6.
    have ->: (fun (x : ident * mask * code) => lrb_R.[x] = None) =
             (fun (x : ident * mask * code) => lrb_L.[x] = None).
      by rewrite fun_ext /(==); move => x; rewrite (H3 x).
    pose L:= (NewFMap.oflist
         (map (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3)))
            (filter
               (predI (mem regL_R \o get_id)
                  (predI (fun (x : ident * mask * code) => lrb_L.[x] = None)
                     (predC (mem corL_R \o get_id)))) (bb_b_R)))).
    have ->: nth witness Voters BP.ibad{2} = BP.id{2}.   
      rewrite -H (nth_map witness witness). smt.
      rewrite nth_cat. 
      have ->: BP.ibad{2} < size (take BP.ibad{2} BP.kk{2} ++ 
                                  [(BP.id{2}, oget BP.kL{1}.[BP.id{2}])]). 
        smt.
      rewrite //= nth_cat.
      smt.
    smt.
  + move: H7 H6.
    have ->: (fun (x : ident * mask * code) => lrb_R.[x] = None) =
             (fun (x : ident * mask * code) => lrb_L.[x] = None).
      by rewrite fun_ext /(==); move => x; rewrite (H3 x).
    pose g:= (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3))).
    pose f1:= (fun (x : ident * mask * code) => lrb_L.[x] = None).
    pose f2:= (mem regL_R \o get_id).
    pose f3:= (predC (mem corL_R \o get_id)).
    pose B:= ( bb_b_R).
    have ->: forall (S: (ident*mask*code) list),
              (filter (predI f2 (predI f1 f3)) S) =
              (filter (predI f1 (predI f2 f3)) S).
      by elim=>/>; smt.
    rewrite filter_predI.
    pose gi:= (fun (x : ident * (mask * code)) => (x.`1, x.`2.`1, x.`2.`2)).
    have ->: forall (S: (ident* mask*code) list),
               map g (filter f1 S) =
               filter (f1\o gi) (map g S).
      move => S. rewrite filter_map. 
      have ->: (preim g (f1 \o gi)) = f1. 
        rewrite /preim /(\o) /g /gi /f1 //=. 
        by rewrite fun_ext /(==); move => x; smt.
      by done.      
    pose L:= (map g (filter (predI f2 f3) B)).
    have ->: nth witness Voters BP.ibad{2} = BP.id{2}.   
      rewrite -H (nth_map witness witness). smt.
      rewrite nth_cat. 
      have ->: BP.ibad{2} < size (take BP.ibad{2} BP.kk{2} ++ 
                                  [(BP.id{2}, oget BP.kL{1}.[BP.id{2}])]). 
        smt.
      rewrite //= nth_cat.
      smt. 
    rewrite get_oflist. 
    move => H7 H6. 
    have Ho: forall (x : mask * code),
                lrb_L.[(BP.id{2}, x.`1, x.`2)] = None <=> 
                (f1 \o gi) (BP.id{2},(x.`1,x.`2)).
      by rewrite /f1 /gi /(\o) //=.
    have Hol: forall (x : mask * code),
                (f1 \o gi) (BP.id{2},(x.`1,x.`2)) => ! (x \in mt_R). 
      by move => y; rewrite -(Ho y); exact (H4 y).
    pose p:= (f1 \o gi).
    cut := Hol ((oget (assoc (filter p L) BP.id{2})).`1,
               (oget (assoc (filter p L) BP.id{2})).`2) _.
      rewrite //= -/p.
      smt.
    by done.
  + move: H7 H6.
    pose g:= (fun (x : ident * mask * code) => (x.`1, (x.`2, x.`3))).
    pose f1:= (fun (x : ident * mask * code) => lrb_L.[x] = None).
    pose f2:= (mem regL_R \o get_id).
    pose f3:= (predC (mem corL_R \o get_id)).
    pose B:= ( bb_b_R).
    have ->: (predI f2 (predI f1 f3)) = (predI f3 (predI f2 f1)).
      by rewrite /predI fun_ext /(==); move => y; smt(@Logic).
    rewrite filter_predI.
    pose gi:= (fun (x : ident * (mask * code)) => (x.`1, x.`2.`1, x.`2.`2)).
    have ->: forall (S: (ident* mask*code) list),
               map g (filter f3 S) =
               filter (f3\o gi) (map g S).
      move => S. rewrite filter_map. 
      have ->: (preim g (f3 \o gi)) = f3. 
        by rewrite /preim /(\o) /g /gi /f3 //=. 
      by done.      
    pose L:= (map g (filter (predI f2 f1) B)).
    have ->: nth witness Voters BP.ibad{2} = BP.id{2}.   
      rewrite -H (nth_map witness witness). smt.
      rewrite nth_cat. 
      have ->: BP.ibad{2} < size (take BP.ibad{2} BP.kk{2} ++ 
                                  [(BP.id{2}, oget BP.kL{1}.[BP.id{2}])]). 
        smt.
      rewrite //= nth_cat.
      smt. 
    rewrite get_oflist. 
    move => H7 H6.
    pose p:= (f3 \o gi).
    search assoc filter.
    cut := assoc_filter (predC (mem corL_R)) L BP.id{2}.
      have ->: (predC (mem corL_R) \o 
                fun (p0 : ident * (mask * code)) => p0.`1) = 
               p.
      by rewrite /p /f3 /gi /(\o) //=.
    case (predC (mem corL_R) BP.id{2}).
    + rewrite /predC; move => Hc Hx. 
      by rewrite (H5 Hc).
    - move => Hc Hn. 
      move: H6; rewrite -/p; move => Hp. 
      done.

  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
           BP.wL, BP.bb_b, BP.b_b} /\
            (forall (x: mask*code), 
               BP.lrb{1}.[(BP.id{2},x.`1,x.`2)] = None => 
               !x \in BTS.mt{2})/\
          oget BP.kL{1}.[BP.id{2}] = BTS.k{2} /\
          (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
          (forall x, x<> BP.id{2} => BP.kL{1}.[x] = BP.kL{2}.[x])/\
          (BP.votL{1}.[BP.id{2}] = None => BTS.qt{2} <1)/\
          (!BP.id{2} \in BP.corL{1} => !BTS.cor{2})).
  + proc. 
    sp; if =>//=.
    inline*; wp.
    call(: true).
    by auto.
  + proc.  
    sp 1 1; if =>//=.  
    inline*; auto=>/>; progress.
    + by rewrite mem_cat. 
    + by rewrite (H1 id{2}  H7). 
    + smt (mem_cat). 
    + by rewrite (H1 id{2} H7).  
  + proc.   
    sp; if =>//=.
    swap {1} 8 -4. swap{1} 6 -2. swap{1} 8 -2. swap{1} 2 4. 
    (* fixme: BP.lrb{1} <> None <=> BP.lrb{2} <> None, but lrb{1} <> lrb{2} *)
    seq 4 4: (  ={id, v0, v1, w, b0, b1, l_or_r, glob LR, glob E, BP.corL, 
                  BP.regL, BP.cL, BP.pk, BP.sk, BP.votL,BP.wL, BP.bb_b, BP.b_b} /\
                (forall (x : mask * code),
                        BP.lrb{1}.[(BP.id{2}, x.`1, x.`2)] = None => ! (x \in BTS.mt{2})) /\
               (id{1} \in Voters) /\
               (id{1} \in BP.regL{1}) /\
               BP.votL{1}.[id{1}] = None /\ ! (id{1} \in BP.corL{1}) /\
               w{1} = BP.wL{1}.[id{1}]/\
               oget BP.kL{1}.[BP.id{2}] = BTS.k{2} /\
               (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
               (forall x, x<> BP.id{2} => BP.kL{1}.[x] = BP.kL{2}.[x])/\
               (BP.votL{1}.[BP.id{2}] = None => BTS.qt{2} <1)/\
               b0{1}.`1 = id{2} /\
               b1{1}.`1 = id{2} /\
               (!BP.id{2} \in BP.corL{1} => !BTS.cor{2})/\
               b{1} = o{2} /\ o{2} = None).
     inline*; wp.
     call(: true).      
     by auto; smt.
   sp.
   if{2} =>/>.
   + (* BP.id *)
     inline*.
     rcondt{2} 4; progress.
     + by auto; smt.    
     wp; auto; progress. 
     + by rewrite oget_some; case (l_or_r{2}). 
     + by rewrite oget_some; case (l_or_r{2}). 
     + rewrite mem_cat //=; smt (getP). 
     + smt(getP). smt(getP). smt (getP).
   - (* not BP.id{2} *)
     inline *; wp.
     by auto; progress; smt(getP).
  + by proc; auto.
  + by proc; auto. 
  + by proc; wp.
  wp; call(: true); wp. 
  by auto=>/>. 
qed.
         
local lemma intermediary  &m:
  `|Pr[DB_L(MV(E),A_DB,Left).main() @ &m : res] - 
    Pr[DB_R(MV(E),A_DB,Right).main() @ &m : res]| 
<= 
  `|Pr[BV(MV(E),A_DB,Left).main() @ &m : res ] - 
    Pr[BV(MV(E),A_DB,Right).main() @ &m : res ]| + 
 (size Voters)%r * 
   Pr[TSec_Exp(TOA(MV(E),A_DB,Left)).main() @ &m : res]+ 
 (size Voters)%r *
   Pr[TSec_Exp(TOA(MV(E),A_DB,Right)).main() @ &m : res].
proof.
  have Hwhy: forall (a b c: real), 0%r<= a => b <= c => a * b <= a * c by smt.
  pose BV_L:=Pr[BV(MV(E), A_DB, Left).main() @ &m : res].
  pose BV_R:=Pr[BV(MV(E), A_DB, Right).main() @ &m : res].
  pose TS_L:=Pr[TSec_Exp(TOA(MV(E), A_DB, Left)).main() @ &m : res].
  pose TS_R:= Pr[TSec_Exp(TOA(MV(E), A_DB, Right)).main() @ &m : res].
  
  rewrite (db_db_2 &m).
  rewrite (bpriv_bt_L Left &m) (bpriv_bt_R Right &m).
  rewrite (bt_bsp_L Left &m) (bt_bsp_R Right &m).
  rewrite (bsp_bspp_L Left &m) (bsp_bspp_R Right &m).
  rewrite (bspp_badsplit_L Left &m) (bspp_badsplit_R Right &m).

  rewrite (badsplit_bv_L Left &m) (badsplit_bv_R Right &m).
  rewrite -/BV_L -/BV_R.  
  pose L2:= Pr[BSPP_L(MV(E), A_DB, Left).main() @ &m : res /\ BP.bad].
  pose R2:= Pr[BSPP_R(MV(E), A_DB, Right).main() @ &m : res /\ BP.bad].

  have HL2: L2 <= (size Voters)%r * TS_L.
    cut := badsplit_bbad_L Left &m.
      rewrite -/L2.
    cut := bbad_bkey Left &m.
    cut := bbad_bkey_two Left &m.
    cut := badsplit_guess Left &m.
    cut := guess_ibad Left &m.
    cut := ibad_bnth Left &m.
    cut := nth_token Left &m.
    pose A:= Pr[Bbad(MV(E), A_DB, Left).main() @ &m : BP.bad].
    pose B:= Pr[BBbad(MV(E), A_DB, Left).main() @ &m : BP.bad].
    pose C:= Pr[BKey(MV(E), A_DB, Left).main() @ &m : BP.bad /\ 0 <= BP.ibad < size Voters].
    pose D:= Pr[WV.Guess(BKey(MV(E), A_DB, Left)).main() @ &m :
                BP.bad /\ 0 <= BP.ibad < size Voters /\ res.`1 = BP.ibad].
    pose E:= Pr[BIbad(MV(E), A_DB, Left).main() @ &m : BP.bad].
    pose F:= Pr[Bnth(MV(E), A_DB, Left).main() @ &m : BP.bad].
    rewrite -/TS_L.
    move => Hfl Hef Hde Hcd Hbc Hab Hla.
    have Hone: L2 <= C by smt.
    have Htwo: C = (size Voters)%r *D. rewrite -Hcd. 
      have ->: (size Voters)%r * (1%r / (size Voters)%r * C) =
               ((size Voters)%r * (1%r /( size Voters)%r)) * C  by smt.
      have ->: ((size Voters)%r * (1%r / (size Voters)%r)) = 1%r. smt.
      smt.
    have Hthr: D <= TS_L by smt.
    have Hfou: C <= (size Voters)%r * TS_L.
      rewrite Htwo Hwhy. smt. by rewrite Hthr.
    smt.
  have HR2: R2 <= (size Voters)%r * TS_R. 
    cut := badsplit_bbad_R Right &m.
      rewrite -/R2.
    cut := bbad_bkey Right &m.
    cut := bbad_bkey_two Right &m.
    cut := badsplit_guess Right &m.
    cut := guess_ibad Right &m.
    cut := ibad_bnth Right &m.
    cut := nth_token Right &m.
    pose A:= Pr[Bbad(MV(E), A_DB, Right).main() @ &m : BP.bad].
    pose B:= Pr[BBbad(MV(E), A_DB, Right).main() @ &m : BP.bad].
    pose C:= Pr[BKey(MV(E), A_DB, Right).main() @ &m : BP.bad /\ 0 <= BP.ibad < size Voters].
    pose D:= Pr[WV.Guess(BKey(MV(E), A_DB, Right)).main() @ &m :
                BP.bad /\ 0 <= BP.ibad < size Voters /\ res.`1 = BP.ibad].
    pose E:= Pr[BIbad(MV(E), A_DB, Right).main() @ &m : BP.bad].
    pose F:= Pr[Bnth(MV(E), A_DB, Right).main() @ &m : BP.bad].
    rewrite -/TS_L.
    move => Hfl Hef Hde Hcd Hbc Hab Hla.
    have Hone: R2 <= C by smt.
    have Htwo: C = (size Voters)%r *D. rewrite -Hcd. 
      have ->: (size Voters)%r * (1%r / (size Voters)%r * C) =
               ((size Voters)%r * (1%r /( size Voters)%r)) * C  by smt.
      have ->: ((size Voters)%r * (1%r / (size Voters)%r)) = 1%r. smt.
      smt.
    have Hthr: D <= TS_R by smt.
    have Hfou: C <= (size Voters)%r * TS_R.
      rewrite Htwo Hwhy. smt. by rewrite Hthr.
    smt.
  have ->: BV_L + L2 - (BV_R + R2) = (BV_L - BV_R) + (L2-R2) by smt.
  have Hleft: `|BV_L - BV_R + (L2 - R2)| <= `|BV_L - BV_R| + `|(L2 - R2)| by smt.
  have HL2_0: 0%r <= L2
    by rewrite /L2; byphoare=>//=. 
  have HR2_0: 0%r <= R2
    by rewrite /R2; byphoare=>//=. 
  have Hright: `|L2 - R2| <= L2 + R2. 
    case (0%r <= L2 -R2). 
    + smt (@Real).  
    - smt(@RealExtra).

  smt (@Real).
qed. 


(* reduction to CPA *)
(* BPRIV Oracles *) 
module BR(V: VotingScheme, E: EncScheme, LR: LorR): DB_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x,c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <$ dmask;
      x <$ dmask;
      c <@ E.enc(BP.pk,w);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt = DB_Oracles(V,LR).corrupt
  proc vote    = DB_Oracles(V,LR).vote
  proc castH   = DB_Oracles(V,LR).castH
  proc castC   = DB_Oracles(V,LR).castC
  proc board   = DB_Oracles(V,LR).board
}.

local module B(V: VotingScheme, A: DB_Adv, LR: LorR, O: DB_Oracles) = {

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
      
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) ( BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g cbb;

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad;
  }
}.

local lemma bv_b_rand (LR <: LorR{A_DB, BP, E}) &m:
  Pr[BV(MV(E),A_DB,LR).main() @ &m : res ] = 
  Pr[B(MV(E),A_DB,LR, BR(MV(E),E,LR)).main() @ &m : res].
proof.
  byequiv =>/>; proc.
  wp; call(: true).
  wp; call(: ={glob E, BP.lrb, BP.regL, BP.wL, BP.cL, 
               BP.kL, BP.votL, BP.corL, BP.pk, BP.bb_b, BP.b_b}).
  + proc.
    sp; if=>//=.
    inline*; wp.
    call(: true).
    swap{2} 1 1.
    rnd; rnd{2}; wp.
    by auto; smt.
  + by proc; sim.
  + by proc; sim.
  + by proc; auto.
  + by proc; auto.
  + by proc; wp.

  inline*; wp.
  call(: true).
  while (={i, BP.kL}); first by sim.
  auto=>/>; progress. 
  + pose a:= (fun (x : ident * mask * code) => oget votL_R.[x.`1]).
    pose b:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]). 
    pose f1:= (fun (x : ident * mask * code) =>
               verify (oget kL_R.[x.`1]) x.`2 x.`3). 
    pose f2n:= (predC (mem corL_R \o get_id)). 
    pose f2p:=(mem corL_R \o get_id).
    pose f3:= (mem regL_R \o get_id). 
    rewrite -?filter_predI. 
    have ->: (predI (predI f1 f2n) f3) = (predI f2n (predI f1 f3)).
      rewrite /predI fun_ext /(==) //=; smt.
    have ->: (predI (predI f1 f2p) f3) = (predI f2p (predI f1 f3)).
      by rewrite /predI fun_ext /(==) //=; smt.
    by done.
  + pose a:= (fun (x : ident * mask * code) => oget votL_R.[x.`1]).
    pose b:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]). 
    pose f1:= (fun (x : ident * mask * code) =>
           verify (oget kL_R.[x.`1]) x.`2 x.`3). 
    pose f2n:= (predC (mem corL_R \o get_id)). 
    pose f3:= (mem regL_R \o get_id). 
    rewrite -?filter_predI. 
    have ->: (predI (predI f1 f2n) f3) = (predI f2n (predI f1 f3)).
      by rewrite /predI fun_ext /(==) //=; smt.
    by done.
qed.

module (BIND(A: DB_Adv, BLR: LorR): CPA_Adv) (O: CPA_Oracles) ={
  module BO = {
    proc reg (id: ident): cipher option={
      var w, x, c;
      var o<- None;

      if (id \in Voters /\ !id \in BP.regL){
        w <$ dmask;
        x <$ dmask;
        c <@ O.enc(w, x);
        (* BP.cL = map snd BS.encL *)
        BP.wL.[id] <- w;
        BP.cL.[id] <- c;
        BP.regL <- BP.regL ++[id];
        o <- Some c;
      }
      return o;
    }

    proc corrupt(id: ident): (cred *idkey) option ={
      var w<- None;

      if ( id \in Voters /\ id \in BP.regL/\ BP.votL.[id] = None ){
        w <- Some (oget BP.wL.[id], oget BP.kL.[id]); 
        if (!id \in BP.corL){
          BP.corL<- BP.corL ++ [id]; 
        }
      }
      return  w;   
    }

    proc vote(id : ident, v0 v1 : vote) : (ident *mask*code) option = {
      var b0, b1, w, l_or_r, k, t0,t1;
      var b <- None;

      if ( id \in Voters /\ 
           id \in BP.regL /\
           BP.votL.[id] = None/\
          !id \in BP.corL){
        (* credential and key *)
        w <- BP.wL.[id];
        k <- BP.kL.[id];
        b0     <- (id, v0 + oget w);
        t0     <- token (oget k) (v0 + oget w); 
        b1     <- (id, v1 + oget w);
        t1     <- token (oget k) (v1 + oget w);
        BP.votL.[id] <- v0;
        l_or_r <@ BLR.l_or_r();
        BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
        BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
        b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      }
      return b;
    }
    proc castH(b: ident*mask*code): unit ={
      if (b.`1 \in BP.regL /\ !b.`1 \in BP.corL /\
          BP.votL.[b.`1] <> None /\ !b.`1 \in map get_id BP.bb_b){
        BP.bb_b <- BP.bb_b ++ [b];
      }
    }
  
    proc castC(b: ident*mask*code): unit ={
      if(b.`1 \in BP.corL /\ !b.`1 \in map get_id BP.bb_b){
        BP.bb_b <- BP.bb_b ++ [b];
      }
    }

    proc board(): (ident * mask*code) list ={
      return BP.bb_b;
    }

  }(* end BO *)

  proc main(pk: pkey):bool ={
    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    BP.pk <- pk;
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    A(BO).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) (BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g cbb;

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A(BO).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad;
  }
  
}.


local lemma b_rand_cpa_left (LR<: LorR{A_DB, BP, BS,E}) &m:
  Pr[B(MV(E),A_DB,LR, BR(MV(E),E,LR)).main() @ &m : res] =
  Pr[CPA(E,BIND(A_DB,LR), Left).main() @ &m : res /\ size BS.encL <= size Voters].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR}==> _)=>/>.
  proc. 
  inline*; wp.
  call(: true); wp.

  conseq (: ={BP.kL, glob A_DB, BP.b_b,  BP.bb_b, BP.wL, BP.corL, BP.regL, BP.votL, BP.lrb} /\
            size BS.encL{2} <= size Voters)=>/>.  
  call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, BP.lrb, 
            BP.pk, BP.bb_b, BP.b_b}/\
         BP.pk{1} = BS.pk{2} /\         
         size BS.encL{2} = size BP.regL{2} /\
         (mem BP.regL{2} <= mem Voters) /\
           uniq BP.regL{2}).
  + proc. 
    sp; if=>//=.
    inline *; wp.
    call (: true).
    wp; rnd; rnd.
    auto; progress.
    + by rewrite ?size_cat H //=. 
    + rewrite /(<=).
      move => z.
      rewrite mem_cat mem_seq1.
      move => Hmemz. 
      smt.
    + by rewrite cat_uniq H1 //=.   
  + by proc; wp. 
  + proc. 
    inline*; sp; if=>//=.
    wp; call(: true); wp. 
    by auto.
  + by proc; wp.
  + by proc; wp. 
  + by proc; wp.
  wp; swap{1} 14 -5.  
  while (={i, BP.kL}); first by auto.  
  wp; call{1} kgen_keypair.
  auto=>/>; progress. 
  by rewrite H3 (uniq_leq_size _ _ H5 H4).
qed.


module BX(V: VotingScheme, E: EncScheme, LR: LorR): DB_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <$ dmask;
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt = DB_Oracles(V,LR).corrupt
  proc vote    = DB_Oracles(V,LR).vote
  proc castH   = DB_Oracles(V,LR).castH
  proc castC   = DB_Oracles(V,LR).castC
  proc board   = DB_Oracles(V,LR).board
}.

local lemma bx_rand_cpa_right (LR<: LorR{A_DB, BP, BS,E}) &m:
  Pr[B(MV(E),A_DB,LR, BX(MV(E),E,LR)).main() @ &m : res] =
  Pr[CPA(E,BIND(A_DB,LR), Right).main() @ &m : res /\ size BS.encL <= size Voters].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc. 
  inline*; wp.
  call(: true); wp.
  conseq (: ={BP.kL, glob A_DB, BP.b_b, BP.bb_b, BP.wL, BP.corL, BP.regL, BP.votL, BP.lrb} /\
            size BS.encL{2} <= size Voters)=>/>.  
  call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, BP.lrb, 
            BP.pk, BP.bb_b, BP.b_b}/\
         BP.pk{1} = BS.pk{2} /\         
         size BS.encL{2} = size BP.regL{2} /\
         (mem BP.regL{2} <= mem Voters) /\
           uniq BP.regL{2}).
  + proc. 
    sp; if=>//=.
    inline *; wp.
    call (: true).
    wp; rnd; rnd.
    auto; progress.
    + by rewrite ?size_cat H.
    + rewrite /(<=).
      move => z.
      rewrite mem_cat mem_seq1.
      move => Hmemz. 
      smt.
    + by rewrite cat_uniq. 
  + by proc; wp. 
  + proc. 
    inline*; sp; if=>//=.
    wp; call(: true); wp.
    by auto.
  + by proc; wp.
  + by proc; wp. 
  + by proc; wp.
     
  wp; swap{1} 14 -5.  
  while (={i, BP.kL}); first by auto.  
  wp; call{1} kgen_keypair.
  auto=>/>; progress. 
  by rewrite H3 (uniq_leq_size _ _ H5 H4).
qed.

local lemma b_rand_bx_rand (LR<: LorR{A_DB, BP, BS, E}) &m:
  `| Pr[B(MV(E),A_DB,LR, BR(MV(E),E,LR)).main() @ &m : res] -
     Pr[B(MV(E),A_DB,LR, BX(MV(E),E,LR)).main() @ &m : res] | 
<=
  `| Pr[CPA(E,BIND(A_DB,LR), Left).main() @ &m : res /\ size BS.encL <= size Voters]-
     Pr[CPA(E,BIND(A_DB,LR), Right).main() @ &m : res /\ size BS.encL <= size Voters]|.
proof.
  by rewrite (b_rand_cpa_left LR &m) (bx_rand_cpa_right LR &m).
qed.

local lemma BINDmain_ll (LR<: LorR {A_DB}) (O<: CPA_Oracles{BIND(A_DB,LR)}):
  islossless LR.l_or_r =>
  islossless O.enc =>
  islossless BIND(A_DB,LR,O).main.
proof.
  move => LR_ll Oenc_ll.
  proc. 
  call (Aa2_ll (<: BIND(A_DB, LR, O).BO) ).
  wp; progress.
  wp; call (Aa1_ll (<: BIND(A_DB, LR, O).BO) _ _ _ _ _).  
  + proc.  
    sp; if =>//=.
    wp; call Oenc_ll; rnd; rnd.
    by auto; progress; smt.

  + by proc; wp. 
  + proc. 
    sp; if=>//=.
    wp; call LR_ll; wp.
    by auto; smt (drand_lossless).
  + by proc; wp.
  + by proc; wp.
  while{1} (true) (size Voters - i).
    by auto=>/>; smt.
  by auto; smt.
qed.

(* Lemma apply the hybrid argument *)
local lemma hybrid (LR<: LorR {A_DB, BP, BS, E, H.Count, H.HybOrcl, WrapAdv}) &m :
  islossless LR.l_or_r =>
  `|Pr[CPA(E,BIND(A_DB,LR),Left).main() 
            @ &m: res /\ size BS.encL <= size Voters ] -
    Pr[CPA(E,BIND(A_DB,LR),Right).main() 
            @ &m: res /\ size BS.encL <= size Voters ]|
 = (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND(A_DB,LR),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND(A_DB,LR),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
proof.
  move => LR_ll.
  rewrite eq_sym.
  cut Ht: forall (n a b : real), 
          n >0%r => n * a = b <=> a = 1%r/n * b by progress; smt.  
  pose a:= `|Pr[CPA(E,WrapAdv(BIND(A_DB,LR),E),Left).main() 
                      @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
             Pr[CPA(E,WrapAdv(BIND(A_DB,LR),E),Right).main() 
                      @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
  pose b:= `|Pr[CPA(E,BIND(A_DB,LR),Left).main() 
                       @ &m: res /\ size BS.encL <= size Voters ] -
             Pr[CPA(E,BIND(A_DB,LR),Right).main() 
                       @ &m: res /\ size BS.encL <= size Voters ]|.
  rewrite -/a -/b.
  rewrite (Ht _ a b). 
    cut t:= n_pos.
    by smt.
  rewrite /a /b. 

  cut := CPA_multi_enc E  (BIND(A_DB,LR)) Ek_ll Ee_ll Ed_ll _ &m.
  + move => O.
    exact/(BINDmain_ll LR O LR_ll).
          
  by done.
qed.

module BO(V: VotingScheme', E: EncScheme, LR: LorR, H: ARO): DB_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <@ H.o(id);
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, b1, w, t0, t1, k, l_or_r, o;
    var b<- None;
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\ 
         id \in BP.regL /\
         BP.votL.[id] = None/\
        !id \in BP.corL){

      
      w <@ H.o(id);
      k <- BP.kL.[id];
      b0     <@ V(H).vote(id, v0, w);
      t0     <- token (oget k) b0.`2 ;
      b1     <@ V(H).vote(id, v1, w);
      t1     <- token (oget k) b1.`2 ;
      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      o <-Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1);
      b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }
  
  proc corrupt(id: ident): (cred *idkey) option ={
    var x;
    var w <- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\
         id \in BP.regL /\
         BP.votL.[id] = None){
      x<@ H.o(id); 
      w <- Some (x, oget BP.kL.[id]);  
      (* update corrupted list *)
      if (!id \in BP.corL){
        BP.corL <- BP.corL ++ [id];
      }
    }
    return  w;   
  }
  
  proc castH(b: ident*mask*code): unit ={
    if (b.`1 \in BP.regL /\ !b.`1 \in BP.corL /\
        BP.votL.[b.`1] <> None /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  proc castC(b: ident*mask*code): unit ={
    if(b.`1 \in BP.corL /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  proc board(): (ident * mask*code) list ={
    return BP.bb_b;
  }

}.

local module B'(V: VotingScheme', A: DB_Adv, LR: LorR, H: Oracle, O: DB_Oracles) = {

  proc main() : bool = {
    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
      
    H.init();
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A(O).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) (BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A(O).a2(r,BP.b_b, BP.bb_b);
    return b /\ !BP.bad;

  }
}.

(* to much copy-paste, see way to simplify *)
local lemma bx_rand_b_lazy (LR<: LorR{A_DB, BP, BS,E, LO}) &m:
  Pr[B(MV(E),A_DB,LR, BX(MV(E),E,LR)).main() @ &m : res] =
  Pr[B'(MV_RO(E),A_DB,LR,LO, BO(MV_RO(E),E,LR,LO)).main() @ &m : res].
proof.
  byequiv (: ={glob A_DB, glob E, glob LR} ==> _)=>/>.
  proc.
  sim.
  while{2} ( 0 <= i{2} /\
             (forall x, x \in cbb{2} =>
                BP.wL.[x.`1]{1} = LazyRO.RO.m.[x.`1]{2} /\
                (x.`1 \in dom LazyRO.RO.m{2}))/\
             ucbb{2} = map (fun (x: ident*mask*code), x.`2- oget LazyRO.RO.m{2}.[x.`1])
                           (take i{2} cbb{2}))
           (size cbb{2} - i{2}); progress.
    inline *. 
    rcondf 4; progress.
      rnd; wp.
      auto=>/>; progress. 
      cut := H0 (nth witness cbb{hr} i{hr}) _; 
        first by rewrite mem_nth; first by rewrite H H1.
      elim => [Hx Hb]. 
      done.
    wp; rnd; wp.
    auto; progress.  
    + smt(@Int). smt. smt (@Int). by rewrite dmask_lossless. 

  wp; call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, 
                BP.lrb, BP.pk, BP.bb_b, BP.b_b}/\
         (forall x, x \in BP.regL{2} =>
            BP.wL.[x]{1} = LazyRO.RO.m.[x]{2}/\
                (x\in dom LazyRO.RO.m{2}))/\
         (forall id, !id \in BP.regL{2} =>
                ! (id \in dom LazyRO.RO.m{2}))/\
         (forall id, BP.votL.[id]{1} <> None =>
            id \in BP.regL{2})).
  + proc. 
    sp; if =>//=.
    inline *.
    rcondt{2} 3; progress.
      rnd; wp.
      auto; progress; smt.
    wp; call(: true); rnd; wp; rnd.
    auto=>/>; progress. 
    + smt(getP). 
    + move: H6; rewrite mem_cat mem_seq1; move => Hx. 
      smt(getP).
    + move: H6; rewrite mem_cat mem_seq1; move => Hx. 
      smt.
    + move: H6; rewrite mem_cat mem_seq1; move => Hx. 
      smt.
    + move: H6; rewrite ?mem_cat //=. 
      smt.
  
  + proc.
    inline *. sp 1 1.
    if=>//=.
    rcondf{2} 3; progress.
    + auto=>/>; progress. 
      cut := H id{hr} H3.
      by elim => [Hj Ho].
    auto=>/>; progress.
    + by rewrite dmask_lossless.
    + cut := H id{2} H3.
      elim => [Hj Ho]. 
      by rewrite Hj.
    + by rewrite (andWl _ _ (H id{2} H3)).
    
  + proc.
    sp; if =>//=.   
    inline *.  
    rcondf{2} 3; progress.
      by auto=>/>; smt.
    wp; call(: true). 
    wp; rnd{2}; wp. 
    auto=>/>; progress.
    + by rewrite dmask_lossless.
    + smt (getP). smt (getP). smt(getP). smt (getP).
         
  + by proc; wp.
  + by proc; wp.
  + by proc; wp.

  inline *; wp; call(: true).
  while (={i, BP.kL}); first by auto.
  auto=>/>; progress. smt. smt. 
  + move: H6; rewrite ?mem_filter /predI. smt.
  + smt (mem_filter).
  + smt (take0).
  + smt . 
  + rewrite take_oversize; first by smt.  
    move: H8.
    pose L:= (filter (mem corL_R \o get_id)
     (filter
        (predI
           (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3) (mem regL_R \o get_id))
        ( bb_b_R))). 
    move => H8.  
    rewrite -(eq_in_map _ _ L).
    move => x HxL. 
    by rewrite //= (andWl _ _ (H8 x HxL)).
qed.

local module (D (V: VotingScheme') (E: EncScheme) (LR: LorR): Dist) (H : ARO) = {
  module O = BO(V,E, LR,H)

  proc distinguish() : bool ={
    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A_DB(O).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) ( BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A_DB(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad;

  }
}.

local lemma b_ind_eo (LR<: LorR{A_DB,E, BP, EO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,EO, BO(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[IND(EO, D(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(EO, D(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 11. 
  by sim.
qed.

local lemma b_ind_lo (LR<: LorR{A_DB,E, BP, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,LO, BO(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[IND(LO, D(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(LazyRO.RO, D(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 11. 
  by sim.
qed.

local lemma bpriv_b (LR<: LorR{A_DB,E, BP, EO, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,LO, BO(MV_RO(E),E,LR,LO)).main() @ &m : res]=
  Pr[B'(MV_RO(E),A_DB,LR,EO, BO(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  rewrite  (b_ind_eo LR &m) ( b_ind_lo LR &m).
  byequiv  (: ={glob D(MV_RO(E),E, LR)} ==> _)=>/>.
  cut Hu:= (eagerRO (D(MV_RO(E),E,LR)) _). 
  + by move => _; rewrite dmask_lossless. 
  by exact/Hu.
qed.


(* removing the eager call in registration *)
module CO(V: VotingScheme', E: EncScheme, LR: LorR, H: ARO): DB_Oracles = {
 
  proc reg (id: ident): cipher option={
    var x,c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc vote = BO(V, E, LR, H).vote
  proc corrupt = BO(V, E, LR, H).corrupt
  proc castH   = BO(V,E,LR,H).castH
  proc castC   = BO(V,E,LR,H).castC
  proc board   = BO(V,E,LR,H).board
}.


local lemma b_eager_rem_call (LR<: LorR{A_DB,E, BP, EO, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,EO, BO(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[B'(MV_RO(E),A_DB,LR,EO, CO(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DB, glob LR} ==> _)=>/>.
  proc.
  sim.
  call(: ={glob LR, glob RO, glob E, BP.lrb, BP.kL, BP.corL, BP.votL, 
           BP.regL, BP.pk, BP.bb_b, BP.b_b}). 
  + proc. 
    inline *; wp.
    sp; if=>//=.
    by wp; call(: true); rnd; wp.   
  + sim. sim. sim. sim. sim.
  conseq(: ={glob LR, glob RO, glob E, BP.corL, BP.regL, BP.cL, 
             BP.pk, BP.lrb, BP.kL, BP.votL, BP.bb_b, BP.b_b})=>/>.
   progress. 
  by sim.
qed.

local module (D2 (V: VotingScheme') (E: EncScheme) (LR: LorR): Dist) (H : ARO) = {
  module O = CO(V,E, LR,H)

  proc distinguish() : bool ={
    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
   
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A_DB(O).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) (BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A_DB(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad; 
  }
}.

local lemma b_ind_eo_back (LR<: LorR{A_DB,E, BP, EO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,EO, CO(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[IND(EO, D2(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(EO, D2(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 11. 
  by sim.
qed.

local lemma b_ind_lo_back (LR<: LorR{A_DB,E, BP, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,LO, CO(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[IND(LO, D2(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(LazyRO.RO, D2(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 11. 
  by sim.
qed.

local lemma bpriv_b_back (LR<: LorR{A_DB,E, BP, EO, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,LO, CO(MV_RO(E),E,LR,LO)).main() @ &m : res]=
  Pr[B'(MV_RO(E),A_DB,LR,EO, CO(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  rewrite  (b_ind_eo_back LR &m) ( b_ind_lo_back LR &m).
  byequiv  (: ={glob D2(MV_RO(E),E, LR)} ==> _)=>/>.
  cut Hu:= (eagerRO (D2(MV_RO(E),E,LR)) _). 
  + by move => _; rewrite dmask_lossless.
  by exact/Hu.
qed.

(* removing the eager call in registration *)
module BOB(V: VotingScheme, E: EncScheme, LR: LorR): DB_Oracles = {

   proc reg (id: ident): cipher option={
    
    var x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt(id: ident): (cred *idkey) option ={
    var x;
    var w<- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\ id \in BP.regL /\
         BP.votL.[id] = None){
      x <$ dmask;  
      (* update corrupted list *)
      if (!id \in BP.corL){
        BP.corL <- BP.corL ++ [id];
        BP.wL.[id] <- x;
      }
      w<- Some (oget BP.wL.[id], oget BP.kL.[id]);
    }
    return  w;   
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, t0, b1, t1, w, k, l_or_r;
    var b <- None;
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\  id \in BP.regL /\
        BP.votL.[id] = None/\ !id \in BP.corL){
      (* credential and ciphertext *)
      w <$ dmask;
      BP.wL.[id]<- w;
      k <- BP.kL.[id];

      (* create ballots *)
      b0     <@ V.vote(id, v0, w);
      t0     <@ V.token(b0.`2, oget k);
      b1     <@ V.vote(id, v1, w);
      t1     <@ V.token(b1.`2, oget k);

      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_0.[id] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1); 
      b <- Some ( l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }

  proc castH(b: ident*mask*code): unit ={
     if (b.`1 \in BP.regL /\ !b.`1 \in BP.corL /\
        BP.votL.[b.`1] <> None /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  proc castC(b: ident*mask*code): unit ={
    if(b.`1 \in BP.corL /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  proc board(): (ident * mask*code) list ={
    return BP.bb_b;
  }
  
  
}.

local module BOL(V: VotingScheme, A: DB_Adv, LR: LorR, O: DB_Oracles) = {

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
   
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
           filter (predI f ((mem BP.regL)\o get_id)) (BP.bb_b);

    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h hbb;

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <- oget BP.wL.[id];
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    BP.bad <- let g = fun (x: ident*mask*code),
                        BP.lrb.[x] = None in
              has g hbb;

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b /\ !BP.bad; 
  }
}.

local lemma remove_ro (LR<: LorR{ E, A_DB, BP, LO}) &m:
  Pr[B'(MV_RO(E),A_DB,LR,LO, CO(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[BOL(MV(E),A_DB, LR, BOB(MV(E),E,LR)).main() @ &m : res].
proof.
  byequiv (: ={glob A_DB, glob LR, glob E} ==> _)=>/>.
  proc.
  call(: true).
  wp.
  inline*. 
  while (={i, cbb, ucbb} /\ 0<= i{1} /\
         (forall id, id \in (map get_id cbb{2}) =>
            BP.wL.[id]{2} = LO.m.[id]{1}/\
                (id \in dom LO.m{1}))).
    inline *. 
    rcondf{1} 4; progress.
      rnd; wp.
      auto; progress; smt.
    wp; rnd{1}; wp.
    auto; progress; smt.
  wp; call(: ={ glob LR, glob E, BP.corL, BP.regL, BP.votL,
                BP.pk, BP.kL, BP.lrb, BP.bb_b,BP.b_b}/\
         
         (forall id, id \in BP.corL{2} =>
            BP.wL.[id]{2} = LO.m.[id]{1}/\
                (id \in dom LO.m{1}))/\
         (forall id, id \in Voters /\ 
                     BP.votL{1}.[id] = None/\
                     ! id \in BP.corL{2} =>
                ! (id \in dom LO.m{1}))).
  + proc. 
    sp; if =>//=. 
    wp; call(: true); rnd.
    by auto; progress.
  
  + proc.
    inline *. 
    sp. if =>//=. 
    seq 2 1:  (w{2} = None /\
               w{1} = None /\
               ={id, glob LR, glob E, BP.corL, BP.regL, BP.votL, BP.pk, BP.kL, BP.lrb,
                 BP.bb_b, BP.b_b} /\
               (forall (id0 : ident),
                   id0 \in BP.corL{2} =>
                   BP.wL{2}.[id0] = LazyRO.RO.m{1}.[id0] /\ (id0 \in dom LazyRO.RO.m{1})) /\
               (forall (id0 : ident),
                   (id0 \in Voters) /\ BP.votL{1}.[id0] = None /\ ! (id0 \in BP.corL{2}) =>
                   ! (id0 \in dom LazyRO.RO.m{1})) /\
               (id{1} \in Voters) /\ (id{1} \in BP.regL{1}) /\ BP.votL{1}.[id{1}] = None/\
               y{1}= x{2} /\ x0{1} =id{1}).
     by rnd; wp.
    if =>//=. progress. smt. smt.
    rcondt{1} 4; progress. 
        by auto; progress; smt. 
    auto; progress.
    + smt. 
    + move: H5; rewrite mem_cat mem_seq1 //=; move => Hm. smt(getP).
    + move: H5; rewrite mem_cat mem_seq1 //=; move => Hm. smt(@NewFMap).
    + move: H7; rewrite mem_cat mem_seq1 //=; move => Hm. 
      smt(@NewFMap).
     
    by auto =>/>; smt. 
    
  + proc.
    sp; if =>//=.   
    inline *.  
    rcondt{1} 3; progress.
      by auto=>/>; smt.
    wp; call(: true); wp; rnd; wp.
    auto;progress. 
    + smt. smt. smt. smt. smt. smt.
  + by proc; wp.
  + by proc; wp.
  + by proc; wp.

  inline *; wp; call(: true).
  while (={BP.kL,i}); first by sim.
  auto=>/>; progress. 
  + smt. 
  + cut := H2 id0 _.
      move: H4; rewrite mapP. 
      elim => y. rewrite mem_filter.
      elim => [[Hm _ ] Hid].
      smt.
    by done.
  + cut := H2 id0 _.
      move: H4; rewrite mapP. 
      elim => y. rewrite mem_filter.
      elim => [[Hm _ ] Hid].
      smt.
    by done.
qed.

local lemma left_right &m:
  Pr[BOL(MV(E),A_DB, Left, BOB(MV(E),E,Left)).main() @ &m : res] =
  Pr[BOL(MV(E),A_DB, Right, BOB(MV(E),E,Right)).main() @ &m : res].
proof.
  byequiv =>/>.
  proc.
  call(: true).
  wp; while (={i, cbb, ucbb}/\ 0<= i{1}/\
             (forall id, 
               id \in map get_id cbb{1} => 
               BP.wL{1}.[id] =  BP.wL{2}.[id])).
    auto; progress. 
    + rewrite (H0 (nth witness cbb{2} i{2}).`1 _). 
        rewrite mapP /get_id. smt.
      smt. 
    + smt.
  wp. conseq (: ={glob A_DB, BP.b_b, BP.bb_b, BP.kL, BP.corL, BP.votL, BP.kL, BP.regL, BP.pk}/\
                (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
                (forall id, id \in BP.corL{1} => 
                    BP.wL{1}.[id] =  BP.wL{2}.[id])).
  progress.
  + cut := H0 id0 _.
    move: H1; rewrite mapP. 
      elim => y. rewrite mem_filter.
      elim => [[Hm _ ] Hid].
      smt.
    by done.
  + have ->: (fun (x : ident * mask * code) => lrb_R.[x] = None) =
             (fun (x : ident * mask * code) => lrb_L.[x] = None).
      by rewrite fun_ext /(==); move => x; rewrite (H x).
    pose A:= (filter (predC (mem corL_R \o get_id))
        (filter
           (predI
              (fun (x : ident * mask * code) =>
                 verify (oget kL_R.[x.`1]) x.`2 x.`3) (mem regL_R \o get_id))
           (first_id bb_b_R))).
  smt.
  wp; call(: ={glob E, BP.kL, BP.corL, BP.votL, BP.kL, BP.regL, BP.pk, BP.bb_b, BP.b_b}/\
             (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
             (forall id, id \in BP.corL{1} => 
                    BP.wL{1}.[id] =  BP.wL{2}.[id])).
  + proc. 
    sp; if =>//=; auto.
    call(: true); rnd.
    by auto.
  + proc. 
    sp; if=>/>; auto; progress. 
    + smt. 
    + rewrite mem_cat mem_seq1 in H7. smt.
    + smt.
  + proc. 
    sp; if=>//; auto. 
    inline*; wp. (* swap {1} 21 -10. wp; rnd; wp; rnd; wp. *)
    rnd (fun x, x- v1{1}+v0{1}) (fun x, x + v1{1}-v0{1}).
    auto; progress. smt. smt. smt. smt. smt.  
    + move : H10.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL)).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
               token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2}))).
      smt (getP).
    + move : H9.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL) ).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2})) ). 
      smt (getP).
    + move : H10.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL) ).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2})) ). 
      smt (getP).
    + move : H10.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL) ).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2})) ). 
      smt (getP). 
    + smt (getP).
  + by proc; wp.
  + by proc; wp. 
  + by proc; wp.
  inline*; wp.
  call(: true). 
  while (={BP.kL, i}); first by sim.
  by auto=>/>; progress. 
qed.

lemma dish_browser &m:
  `|  Pr[DB_L(MV(E),A_DB,Left).main() @ &m : res] -
      Pr[DB_R(MV(E),A_DB,Right).main() @ &m : res] | 
<=
  (size Voters)%r * Pr[TSec_Exp(TOA(MV(E), A_DB, Left)).main() @ &m : res] +
  (size Voters)%r * Pr[TSec_Exp(TOA(MV(E), A_DB, Right)).main() @ &m : res]+
  (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND(A_DB,Left),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND(A_DB,Left),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|+
  (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND(A_DB,Right),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND(A_DB,Right),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
proof.
  cut := intermediary &m.
  pose A:= `|Pr[DB_L(MV(E), A_DB, Left).main() @ &m : res] -
             Pr[DB_R(MV(E), A_DB, Right).main() @ &m : res]|.
  pose B1:= (size Voters)%r * Pr[TSec_Exp(TOA(MV(E), A_DB, Left)).main() @ &m : res].
  pose B2:= (size Voters)%r * Pr[TSec_Exp(TOA(MV(E), A_DB, Right)).main() @ &m : res].

  rewrite (bv_b_rand Left &m) (bv_b_rand Right &m).
  cut HLeft:= (b_rand_bx_rand Left &m).
  cut HRight:= (b_rand_bx_rand Right &m).
  rewrite (hybrid Left &m _) in HLeft; first by proc.
  rewrite (hybrid Right &m _) in HRight; first by proc.

  pose a:= Pr[B(MV(E), A_DB, Left, BR(MV(E), E, Left)).main() @ &m : res].
  pose b:=Pr[B(MV(E), A_DB, Right, BR(MV(E), E, Right)).main() @ &m : res].
  pose c:= (size Voters)%r *
`|Pr[CPA(E, WrapAdv(BIND(A_DB, Left), E), Left).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1] -
  Pr[CPA(E, WrapAdv(BIND(A_DB, Left), E), Right).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1]|.
  pose d:= (size Voters)%r *
`|Pr[CPA(E, WrapAdv(BIND(A_DB, Right), E), Left).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1] -
  Pr[CPA(E, WrapAdv(BIND(A_DB, Right), E), Right).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1]|.
  pose e:= Pr[B(MV(E), A_DB, Left, BX(MV(E), E, Left)).main() @ &m : res].
  pose f:=Pr[B(MV(E), A_DB, Right, BX(MV(E), E, Right)).main() @ &m : res].
  move : HLeft HRight; rewrite -/a -/b -/c -/d -/e -/f.
  move => HL HR.
  cut Htrig: forall (a b c d: real), `|a - b| <= `|a -c| + `|c - d| + `|b-d| by smt.
  cut HO:= Htrig a b e f.
  have Hef: e = f.
    rewrite /e /f.
    rewrite ( bx_rand_b_lazy Left &m) ( bx_rand_b_lazy Right &m).
    rewrite ( bpriv_b Left &m) ( bpriv_b Right &m).
    rewrite ( b_eager_rem_call Left &m) ( b_eager_rem_call Right &m).
    rewrite -( bpriv_b_back Left &m) -( bpriv_b_back Right &m).
    rewrite ( remove_ro Left &m) ( remove_ro Right &m).
    by rewrite (left_right &m).
  move => HA. 
  have Hab_cd: `|a - b| <= c +d. 
    move: HO; rewrite Hef //=. 
    smt (@Real).
  smt(@Real).
qed. 

end section Dishonest_Browser.
