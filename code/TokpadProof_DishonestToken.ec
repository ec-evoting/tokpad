require import NewFMap List Int IntExtra Distr Real AllCore FSet.
require import LeftOrRight.
require (*  *) TokpadProof_DishonestBrowser LazyEager Encryption.
require (*  *) Ex_Plug_and_Pray.

clone include TokpadProof_DishonestBrowser.

section Dishonest_Token.
declare module E: EncScheme {BTS, SB, BP, BS, EO, LO, H.Count, H.HybOrcl, WrapAdv}.
declare module A_DT: DT_Adv {BTS, SB, BP, BS, E, EO, LO, H.Count, H.HybOrcl, WrapAdv}.

(* lossless *)
axiom Ek_ll:
  islossless E.kgen.
axiom Ee_ll:
  islossless E.enc.
axiom Ed_ll:
  islossless E.dec.

 axiom ADa1_ll (O <: DT_Oracles { A_DT }):
  islossless O.reg =>
  islossless O.corrupt =>
  islossless O.vote =>
  islossless O.castH=>
  islossless O.castC=>
  islossless A_DT(O).a1.

axiom ADa2_ll (O <: DT_Oracles { A_DT }):
  islossless A_DT(O).a2. 

(* axiom on Voters *)
axiom Voters_uniq:
  uniq Voters.

axiom kgen_keypair:
  equiv [E.kgen ~ E.kgen:  ={glob E} ==> 
          ={glob E,  res} /\ 
          keypair res{2}.`1 res{2}.`2].
  
axiom dec_decrypt (ge: (glob E)) (skx: skey) (cx: cipher):
  phoare [E.dec:  
          (glob E) =ge /\ arg = (skx, cx)
           ==>   
          (glob E) =ge /\
          res = decrypt cx skx] = 1%r.

axiom enc_decrypt_two (skx: skey) (pkx:pkey) (mx: cred):
  keypair pkx skx =>
  equiv [E.enc ~ E.enc: 
          ={glob E, arg} /\ arg{1} = (pkx, mx)
           ==>   
          ={glob E, res}/\
          Some mx = decrypt res{1} skx].


local lemma uniq_last (L: (ident* mask*code) list):
  uniq (map get_id L) =>
  last_id L = L.
proof.
  elim: L=>//=. 
  move => x L HuL.
  elim => [HnM Hu]. 
  have ->: !has (pred1 x.`1 \o get_id) L.
    rewrite hasPn /get_id /(\o) /pred1.
    move: HnM; rewrite mapP /get_id negb_exists //=.
    smt (@Logic).
  by rewrite //= (HuL Hu).
qed.

local lemma uniq_first (L: (ident* mask*code) list):
  uniq (map get_id L) =>
  first_id L = L.
proof.
  rewrite /first_id. rewrite -rev_uniq -map_rev.
  move => Hu. 
  by rewrite (uniq_last (rev L) Hu) revK. 
qed.


local lemma has_to_has['a] (L :'a list) g k:
  has g (filter k L) => has g L.
proof. 
  rewrite ?hasP. 
  elim => x. rewrite mem_filter. 
  move => [[Hk Hm] Hg].
  by exists x. 
qed.

local lemma find_to_find (L : (ident* mask*code) list) 
                         (S: (ident * idkey) list) 
                         (g: ident * mask * code -> bool):
  (forall x, x \in L => exists b, (x.`1,b) \in S) =>
  find g L < size L => 
  find (fun (x : ident * idkey) => 
            x.`1 = (nth witness L (find g L)).`1) S < size S.
proof.
  move => HxLS HfL.
  pose a:= (nth witness L (find g L)).
  have HaL: a \in L; first by rewrite /a mem_nth HfL find_ge0. 
  cut := HxLS a HaL. elim => b HbS.
  rewrite -has_find hasP. exists (a.`1, b).
  by rewrite HbS.
qed.
 
local lemma take_drop_uniq' (L :'a list) x i:
   uniq L =>
   x \in take (i+1) L =>
     !x \in drop (i+1) L.
proof.
  cut HL:= cat_take_drop (i+1) L.
  move=> HuL; rewrite -HL in HuL.
  move: HuL. rewrite cat_uniq. 
  elim => Hut [Hhas Hud].
  move: Hhas; rewrite hasPn; move => Hhas Htake.
  by cut := Hhas x; rewrite Htake //=.  
qed. 
 
local lemma find_get ['a 'b] (L: ('a,'b) fmap) f:
  find f L <> None => L.[oget (find f L)] <> None by
  smt (@NewFMap).

 
(* inline tally, and replace encryption by credential *)
module BT2(V: VotingScheme, A: DT_Adv, LR: LorR, O: DT_Oracles) = {

  proc main() : bool = {

    var r, b, i, id, fbb, m, t, v, ubb, w, vbb, bb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.mas0<- map0;
    BP.masb<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_b <- [];
    BP.bb_0 <- [];
    BP.b_b <- map0;
    BP.b_0 <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);
    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;
    fbb <- filter ((mem BP.regL)\o get_id) (first_id bb);
    i <- 0;
    ubb <- [];

    while (i < size fbb){
     (id,m,t) <- nth witness fbb i;
     if (verify (oget BP.kL.[id]) m t) {
       w <- BP.wL.[id];
       v <- m - oget w;
       ubb <- ubb++[v];
     }
     i <- i +1;
    }

    r <- Rho ubb;

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

local lemma bpriv_bt_2 (LR<: LorR{A_DT, BP, E}) &m:
  Pr[DT(MV(E),A_DT,LR).main() @ &m : res] =
  Pr[BT2(MV(E),A_DT,LR,DT_Oracles(MV(E),LR)).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  while (={ubb, BP.regL, fbb} /\ 0<= i0{1}/\
         i0{1} = i{2} /\ kL{1} = BP.kL{2}/\
         (forall x, x \in fbb{1}=>
            BP.wL.[x.`1]{2} = decrypt (oget cL.[x.`1]{1}) sk0{1})).
    seq 1 1: (={ubb, BP.regL, fbb, m, t} /\
              0 <= i0{1} /\ id1{1} = id{2} /\
              (id1,m,t){1} = nth witness fbb{1} i0{1} /\
              i0{1} = i{2} /\
              kL{1} = BP.kL{2} /\
              (forall x, x \in fbb{1} => 
                BP.wL{2}.[x.`1] = decrypt (oget cL{1}.[x.`1]) sk0{1}) /\
              i0{1} < size fbb{1} /\ i{2} < size fbb{2}).
      by auto=>/>; smt.
    if =>//=. 
    + exists * (glob E){1}, sk0{1}, cL{1}, id1{1};
      elim* => ge skx wx ix.
      wp; call{1} (dec_decrypt ge skx (oget wx.[ix])).
      by auto; progress; smt.
    - auto; progress; smt.
  wp. 
  wp; while{1} (0<=i{1} /\
                vbb{1} = filter (fun (x: ident*mask*code),
                         verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3) (take i{1} BP.bb_b{1}))
               (size BP.bb_b{1} - i{1}); progress.
    auto; progress.
    + smt(@Int). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int). smt (@Int).
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= cats1.
    + smt (@Int).
  wp; conseq (: _ ==> ={glob A_DT, BP.regL, BP.cL, BP.lrb, BP.kL, BP.b_b,
                              BP.bb_b, BP.corL}/\
                            (forall x, x \in BP.regL{1} => 
                             BP.wL.[x]{2} = decrypt (oget BP.cL.[x]{1}) BP.sk{1}))=>/>.
  progress.  
  + by rewrite take0. 
  + smt (@Int). 
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + move: H2; rewrite mem_filter /(\o) /get_id. 
    move => [Hm Hr].
    by rewrite (H _ Hm).
  + move: H2.
    have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  + have ->: take i_L bb_b_R = bb_b_R; first by rewrite take_oversize; smt(@Int).
    by done.
  call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.masb, BP.bb_b, BP.b_b}/\
          keypair BP.pk{2} BP.sk{2}/\
          (forall (x : ident), x \in BP.regL{1} => 
            BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1})).
  + proc. 
    sp; if=>//=.
    inline*; wp.   
    seq 3 3: ( o{1} = None /\
               ={id, o, id0, pk, w0, glob LR, glob E, BP.corL, BP.regL, 
                 BP.cL, BP.pk, BP.wL, BP.sk, BP.votL, BP.kL, BP.lrb, 
                  BP.masb, BP.bb_b,BP.b_b} /\
                keypair BP.pk{2} BP.sk{2} /\
               (forall (x : ident), x \in BP.regL{1} => 
                 BP.wL{2}.[x] = decrypt (oget BP.cL{1}.[x]) BP.sk{1}) /\
               (id{1} \in Voters) /\ ! (id{1} \in BP.regL{1}) /\
               id0{1} = id{1} /\ pk{1} = BP.pk{1}).  
      by rnd; auto=>/>. 
    exists * BP.pk{1}, BP.sk{2}, w0{1};
       elim* => pkx skx mx.
    pose kp:= keypair pkx skx.
    cut em_kp: kp \/ !kp by smt. 
    elim em_kp. 
    + move => h.
      call{1} (enc_decrypt_two skx pkx mx h).
      auto; progress. 
      move: H4; rewrite ?mem_cat ?mem_seq1; move => Hm. 
      smt(getP). 
    - move => h.
      conseq(_: _ ==> !(keypair BP.pk{2} BS.sk{2}))=>/>.
      call{1} Ee_ll; call{2} Ee_ll.
      by auto; progress; smt. 
  + by proc; auto=>/>. 
  + proc. 
    sp; if=>//=. 
    inline*; wp.    
    call(: true).
    by auto. 
  + by proc; auto.
  + by proc; auto.
  + by proc; wp.
  wp; call{1} (kgen_keypair). 
  while (={i, BP.kL}); first by sim.
  by auto. 
qed.

  
module BSP2(V: VotingScheme, A: DT_Adv, LR: LorR, O: DT_Oracles) = {

  proc main() : bool = {

    var r, b, i, id, vbb, bb, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.mas0<- map0;
    BP.masb<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_b <- [];
    BP.bb_0 <- [];
    BP.b_b <- map0;
    BP.b_0 <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    vbb <- filter (fun (x: ident*mask*code),
                   verify (oget BP.kL.[x.`1]) x.`2 x.`3) BP.bb_b;
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;

    fbb <- filter ((mem BP.regL)\o get_id) bb;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    cbb <- filter ((mem BP.corL) \o get_id) fbb;

    uhbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g hbb; 

    ucbb <- let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g cbb;

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.


local lemma bt_bsp_2 (LR<: LorR{A_DT, BP, E}) &m:
  Pr[BT2(MV(E),A_DT,LR,DT_Oracles(MV(E),LR)).main() @ &m : res] =
  Pr[BSP2(MV(E),A_DT,LR,DT_Oracles(MV(E),LR)).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR}==> _)=>/>.
  proc. 
  inline*; sim. 
  wp; while{1} ( 0 <= i{1} /\
                 let f = fun (x: ident*mask*code), 
                              verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 in
                 let g = fun (x: ident*mask*code),
                             (x.`2 - oget BP.wL{1}.[x.`1]) in
                 ubb{1} = map g (filter f (take i{1} fbb{1})))
               (size fbb{1} -i{1}); progress.
    auto=>/>; progress. 
    + smt(@Int). 
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1.     
    + smt(@Int). smt(@Int).
    + rewrite (take_nth witness); first by rewrite H H0.
      by rewrite filter_rcons /verify //= H1 //= map_rcons -cats1. 
    + smt(@Int).

  wp; call (: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
                BP.kL, BP.wL, BP.lrb, BP.masb, BP.bb_b, BP.b_b, BP.b_0} /\
            uniq (map get_id BP.bb_b{1})/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => x.`1 = (oget BP.lrb{1}.[x]).`1)/\
             (forall (x:ident*mask*code), 
               BP.lrb{1}.[x] <> None => 
                   verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)).
  + by proc; sp; if=>//=; inline*; wp; call(: true); auto.
  + by proc; wp.
  + proc; sp; if =>//=; inline*; wp; call(: true); auto; progress. 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H6; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H0 x Hx). 
    + rewrite getP.
      case (x = if result_R then
         (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}]))
       else
         (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
          token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}]))). 
      + by rewrite oget_some; case(result_R). 
      - move => Hd. 
        move: H6; rewrite getP Hd //=; move => Hx. 
        by rewrite -(H1 x Hx). 
  + proc. auto; progress.
    rewrite map_cat cat_uniq H//=.
  + proc. auto; progress.
    by rewrite map_cat cat_uniq H //=.
  + by proc; wp.
  wp; call(: true).
  while(={i, BP.kL}); first by sim.
  auto=>/>; progress.
  + smt. smt.
  + by rewrite take0.
  + smt(@Int).  
  + rewrite take_oversize; first by rewrite StdOrder.IntOrder.lerNgt.  
    pose c:= fun (x: ident*mask*code),
             if lrb_R.[x] <> None then oget lrb_R.[x] else x.
    pose v:= fun (x : ident * mask * code) =>
                       verify (oget kL_R.[x.`1]) x.`2 x.`3.
    pose L:= filter (mem regL_R \o get_id)
             (map c (filter v bb_b_R)).
    rewrite -map_cat (* -filter_cat*). 
    pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]). 

    have Hp: perm_eq L (filter (predC (mem corL_R \o get_id)) L ++
                        filter (mem corL_R \o get_id) L) 
      by rewrite perm_eq_sym perm_filterC.
    have Hpp: perm_eq (map g (filter v L)) 
                      (map g (filter v (filter (predC (mem corL_R \o get_id)) L ++
                                        filter (mem corL_R \o get_id) L))).
      by rewrite perm_eq_map perm_eq_filter Hp. 
    have Hpx: perm_eq (map g L) 
                      (map g (filter (predC (mem corL_R \o get_id)) L ++
                                        filter (mem corL_R \o get_id) L)).
      by rewrite perm_eq_map Hp. 
    rewrite uniq_first. 
    have Huf: uniq (map get_id (filter v bb_b_R));
      first by rewrite (uniq_map_filter _ _ _ H3). 
    rewrite -map_comp.  
    have ->: (forall (L: (ident*mask*code) list),
              (forall x, x \in L => x.`1 = (c x).`1)=>
              map (get_id \o c) L = map get_id L).
    + move => A HA. 
      rewrite -eq_in_map /(\o) /get_id. 
      by move => a Ha; rewrite (HA a Ha).
    + move => x. rewrite mem_filter. move => [Hvx Hmx].
      rewrite /c. 
      case (lrb_R.[x] <> None).
      + by move => Hv; rewrite -(H4 x Hv).
      + by done.
    + by done.
    have ->: (filter v (filter (mem regL_R \o get_id) (map c (filter v bb_b_R)))) =
              L.
      rewrite /L.
    rewrite ?filter_map. 
    rewrite -?filter_predI. 
    rewrite (eq_in_filter (predI (predI (preim c v) (preim c (mem regL_R \o get_id))) v)
                          (predI (preim c (mem regL_R \o get_id)) v) bb_b_R).
      move => x Hxm. rewrite /predI /preim.
      progress. 
      rewrite /c.
      case (lrb_R.[x] <> None).
      + by move => Hv; rewrite /v -(H4 x Hv) (H5 x Hv).
      + by done.
    by done.
    by rewrite (rho_perm _ _ Hpx).                     
qed.

module BSPP2(V: VotingScheme, A: DT_Adv, LR: LorR) = {

  module O = DT_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.masb <- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        if BP.lrb.[x] <> None 
                        then oget BP.votL.[x.`1]
                        else (x.`2 - oget BP.wL.[x.`1])  in
            map h (filter f hbb);

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

local lemma bsp_bspp_2 (LR<: LorR{A_DT, BP, E}) &m:
  Pr[BSP2(MV(E),A_DT,LR,DT_Oracles(MV(E),LR)).main() @ &m : res] =
  Pr[BSPP2(MV(E),A_DT,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR}==> _)=>/>.
  proc. 
  call(: true).
  wp.
  conseq (: ={glob A_DT,BP.wL, BP.kL, BP.lrb, BP.corL, BP.regL, BP.votL, BP.bb_b, BP.b_b} /\
            (forall (x: ident*mask*code), 
             x.`1 \in BP.corL{1} => BP.lrb{1}.[x] = None)/\
            (forall x, BP.lrb{1}.[x] <> None =>
               BP.votL{1}.[x.`1] = Some ((oget BP.lrb{1}.[x]).`2 - (oget BP.wL{1}.[x.`1])))/\
            (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               (oget BP.lrb{1}.[x]).`1 = x.`1)/\
            (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 /\
               verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)
            )=>/>.  
  progress.  
  pose f1:= (fun (x : ident * mask * code) =>
              verify (oget kL_R.[x.`1]) x.`2 x.`3).
  pose g:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]).
  pose f2:= (predC (mem corL_R \o get_id)).
  pose f3:= (mem regL_R \o get_id).
  pose s:= (fun (x : ident * mask * code) =>
                 if lrb_R.[x] <> None then oget lrb_R.[x] else x).
  pose s':= fun (x : ident * mask * code) =>
                if lrb_R.[x] <> None then oget votL_R.[x.`1]
                else x.`2 - oget wL_R.[x.`1].
  rewrite -?filter_predI. 
  (* honest voters *)
  have ->: map g (filter (predI f2 f3) (map s (filter f1 bb_b_R))) = 
           map s' (filter (predI (predI f1 f2) f3) bb_b_R).
    rewrite filter_map.
    have ->: preim s (predI f2 f3) = (predI f2 f3).
      rewrite /preim fun_ext /(==); move => x.
      rewrite /predI /f2 /f3 /s  /predC /get_id  /(\o) //=.
      have ->: (if lrb_R.[x] <> None then oget lrb_R.[x] else x).`1 = x.`1.
        case (lrb_R.[x] <> None). 
        + exact (H1 x).
        + done.
      by done.
    rewrite -?filter_predI.
    have ->: (predI (predI f2 f3) f1) = (predI (predI f1 f2) f3).
      by rewrite fun_ext /predI /(==); move => x; smt(@Logic).
    rewrite -map_comp.
    rewrite (eq_map (g\o s) s').
      move => x; rewrite /(\o) /g /s /s' //=. 
      case (lrb_R.[x] <> None). 
      + move => Hx.
        by rewrite (H0 x Hx) oget_some -(H1 x Hx). 
      - by done.
    by done.
    (* corrupt voters *)
    pose f2':= (mem corL_R \o get_id).
    have ->: map g (filter (predI f2' f3) (map s (filter f1 bb_b_R))) = 
             map g (filter (predI (predI f1 f2') f3) bb_b_R).
      rewrite filter_map.
      have ->: preim s (predI f2' f3) = (predI f2' f3).
        rewrite /preim fun_ext /(==); move => x.
        rewrite /predI /f2' /f3 /s  /predC /get_id  /(\o) //=.
        have ->: (if lrb_R.[x] <> None then oget lrb_R.[x] else x).`1 = x.`1.
          case (lrb_R.[x] <> None). 
          + exact (H1 x).
          + done.
        by done.
      rewrite -?filter_predI.
      have ->: (predI (predI f2' f3) f1) = (predI (predI f1 f2') f3).
        by rewrite fun_ext /predI /(==); move => x; smt(@Logic).
      have ->: forall (L: (ident * mask*code) list), 
               (forall x, x \in L => lrb_R.[x] = None) => 
         map s  L = L. 
         + by elim =>//=; smt.
         + move => x. rewrite mem_filter /p /predI /f2' /get_id /(\o) //=.  
           move => [[[Hf1 Hc] Hf3] _]. 
           by rewrite (H x Hc).
      by done.

    by done.
 
  call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb, BP.bb_b, BP.b_0, BP.b_b, BP.masb} /\
          (forall (x: ident*mask*code), 
             !x.`1 \in BP.regL{1} \/ !x.`1 \in Voters => BP.lrb{1}.[x] = None)/\
          (forall (x: ident*mask*code), 
             x.`1 \in BP.corL{1} => BP.lrb{1}.[x] = None)/\
          (forall x, BP.lrb{1}.[x] <> None =>
               BP.votL{1}.[x.`1] = Some ((oget BP.lrb{1}.[x]).`2 - (oget BP.wL{1}.[x.`1])))/\
          (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               (oget BP.lrb{1}.[x]).`1 = x.`1)/\
          (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3 /\
               verify (oget BP.kL{1}.[x.`1]) (oget BP.lrb{1}.[x]).`2 (oget BP.lrb{1}.[x]).`3)).
  + proc.
    sp; if =>//=. 
    inline*; wp.
    call(: true).
    rnd; auto; progress. smt. smt.
  + proc.
    auto=>/>; progress. 
    move: H8; rewrite mem_cat //=. 
    case(x.`1 \in BP.corL{2}). 
    + by move => Hm; rewrite (H0 x Hm). 
    - rewrite //=; move => Hn Hx.
      by cut := H1 x; rewrite Hx H6. 
  + proc. 
    sp; if =>//=.
    inline*; wp.
    call(: true).
    auto=>/>; progress.
    + smt (getP).
    + smt (getP). 
    + move: H8.
      case (result_R).
      + move => Hresult.
        pose A:= (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
              token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}])).
        rewrite ?getP //=. 
        case (x= A). 
        + rewrite /A; move => HxA.
          have ->: x.`1 = id{2} by rewrite HxA. 
          move => HA_none.
          by rewrite oget_some /A //=  mask_sub_def
                    -mask_addA mask_addfN mask_addf0. 
        - rewrite /A; move => HxA Hx_none. 
          rewrite (H1 x Hx_none).
          by case(x.`1 = id{2}); smt. 
      - move => Hresult.
        pose A:= (id{2}, v0{2} + oget BP.wL{2}.[id{2}],
                  token (oget BP.kL{2}.[id{2}]) (v0{2} + oget BP.wL{2}.[id{2}])).
        pose B:= (id{2}, v1{2} + oget BP.wL{2}.[id{2}],
              token (oget BP.kL{2}.[id{2}]) (v1{2} + oget BP.wL{2}.[id{2}])).
        by rewrite getP; smt. 
    + smt (getP). smt (getP). smt (getP).  
  + proc; by auto.
  + proc; auto; progress.   
  + by proc; wp.    
  inline*; wp.   
  call(: true).
  while(={i, BP.kL}); first by sim. 
  by auto=>/>; smt.
qed.
      
module BSP3(V: VotingScheme, A: DT_Adv, LR: LorR) = {

  module O = DT_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, fbb, hbb, cbb, uhbb, ucbb;

    BP.wL <- map0;
    BP.cL <- map0;
    BP.kL <- map0;
    BP.lrb<- map0;
    BP.votL<- map0;
    BP.mas0<- map0;
    BP.masb<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.kk  <- [];
    BP.bb_b <- [];
    BP.bb_0 <- [];
    BP.b_b <- map0;
    BP.b_0 <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      BP.kk<- BP.kk ++[(id, oget BP.kL.[id])];
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    fbb <- filter ((mem BP.regL)\o get_id) BP.bb_b;
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let h = fun (x: ident*mask*code),
                        oget BP.votL.[x.`1] in
            map h (filter f hbb);

    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f cbb);

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.


local lemma bt_bsp_3 (LR<: LorR{A_DT, BP, E}) &m:
  Pr[BSPP2(MV(E),A_DT,LR).main() @ &m : res] =
  Pr[BSP3(MV(E),A_DT,LR).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR}==> _)=>/>.
  proc. 
  inline*; sim.
  wp; progress.
  conseq (: ={BP.kL, BP.wL, glob A_DT, BP.corL, BP.lrb, BP.bb_b, BP.regL, BP.votL,
             BP.b_b }/\
            (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
                  (oget BP.lrb{1}.[x]).`1 = x.`1)/\
            (forall x, 
               x \in BP.bb_b{1} => 
               x.`1 \in BP.regL{1} =>
               !x.`1 \in BP.corL{1} => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3=>
               BP.lrb{1}.[x] <> None) 
            ).
  progress.
  pose f1:= (fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1]).
  pose f2:= (fun (x : ident * mask * code) => oget votL_R.[x.`1]).
  pose g_t:= (fun (x : ident * mask * code) => verify (oget kL_R.[x.`1]) x.`2 x.`3).
  pose g_h:= (predC (mem corL_R \o get_id)).
  pose g_r:= (mem regL_R \o get_id).
  rewrite -?filter_predI.
  rewrite -eq_in_map.
  move => x HxM.
  rewrite /f1 /f2 //=.
  move: HxM. rewrite mem_filter.
  elim => Hp Hf.
  rewrite (H0 x Hf _ _).
  + move: Hp. rewrite /predI /g_h. smt.
  + move: Hp. rewrite /predI /g_r. smt.
  + move: Hp. rewrite /predI /g_t. smt.
  by done.


 call(: ={glob LR, glob E, BP.corL, BP.regL, BP.cL, BP.pk, BP.sk, BP.votL, 
            BP.kL, BP.wL, BP.lrb,  BP.masb, BP.bb_b, BP.b_b, BP.b_0} /\
          (forall (x: ident*mask*code),
               BP.lrb{1}.[x] <> None => 
                  (oget BP.lrb{1}.[x]).`1 = x.`1)/\
          (forall x, x \in BP.corL{1} => x \in BP.regL{1} /\ x \in Voters)/\
          (forall x, 
               x \in BP.bb_b{1} => 
               x.`1 \in BP.regL{1} =>
               !x.`1 \in BP.corL{1} => 
               verify (oget BP.kL{1}.[x.`1]) x.`2 x.`3=>
               BP.lrb{1}.[x] <> None) /\
          (forall (x: ident*mask*code), 
             !x.`1 \in BP.regL{1} \/ !x.`1 \in Voters => 
                BP.lrb{1}.[x] = None /\ !x \in BP.bb_b{2})/\
          (forall (x: ident*mask*code), 
              BP.votL{2}.[x.`1] = None \/ x.`1 \in BP.corL{1} => BP.lrb{1}.[x] = None)/\
          (forall x, 
              BP.votL{1}.[x] <> None =>
              BP.lrb{1}.[(x, oget BP.masb{2}.[x], 
                       token (oget BP.kL{2}.[x]) (oget BP.masb{2}.[x]))] <> None)).
   + proc. 
     sp; if=>/>; auto.
     inline*; wp.
     call(: true); auto; progress. 
     + smt ( mem_cat getP).
     + smt (mem_cat). smt(mem_cat). smt (mem_cat). smt (mem_cat).
    (* + smt (getP). smt (mem_cat). *)
   + proc.
     auto; progress. 
     + smt(mem_cat). smt (mem_cat). smt (mem_cat). smt(mem_cat).
   + proc.
     sp; if=>//=; auto.
     inline*; wp. call(: true); wp.
     auto=>/>; progress.
     + smt (getP). smt (getP). smt(getP). smt. smt(getP). 
     + smt.
   + proc.
     auto; progress. smt(mem_cat). smt. smt(mem_cat).
   + proc.
     auto; progress. 
     + move: H10; rewrite mem_cat //=; move=> Hm.
       case (x \in BP.bb_b{2}). 
       + smt. 
       - move => Hnm.
         move: Hm; rewrite Hnm //=. move => Hx. 
         have Hx1: x.`1 = b.`1{2} by smt.
         have Hx2: x.`2 = oget BP.masb{2}.[b.`1{2}] by smt. 
         move :H11; rewrite /verify. move => Hx3.
         smt. 
     + smt. smt (mem_cat).
   + by proc; wp.
  wp; call(: true); while (={i, BP.kL, BP.kk}); first by sim.
  auto; progress. 
  smt(map0P). smt(map0P). smt(map0P). smt(map0P). 
qed. 

(* reduction to CPA *)
(* BPRIV Oracles *) 
module BR2(V: VotingScheme, E: EncScheme, LR: LorR): DT_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x,c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <$ dmask;
      x <$ dmask;
      c <@ E.enc(BP.pk,w);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt = DT_Oracles(V,LR).corrupt
  proc vote    = DT_Oracles(V,LR).vote
  proc castH   = DT_Oracles(V,LR).castH
  proc castC   = DT_Oracles(V,LR).castC
  proc board   = DT_Oracles(V,LR).board
}.

local module B2(V: VotingScheme, A: DT_Adv, LR: LorR, O: DT_Oracles) = {

  proc main() : bool = {

    var r, b, i, id,  uhbb, ucbb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;
    BP.masb<- map0;
    BP.mas0 <- map0;
  
    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
      
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f BP.bb_b);

    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f BP.bb_b);

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

local lemma bv_b_rand_2 (LR <: LorR{A_DT, BP, E}) &m:
  Pr[BSP3(MV(E),A_DT,LR).main() @ &m : res] = 
  Pr[B2(MV(E),A_DT,LR, BR2(MV(E),E,LR)).main() @ &m : res].
proof.
  byequiv =>/>; proc.
  wp; call(: true).
  wp; call(: ={glob E, BP.lrb, BP.regL, BP.wL, BP.cL, 
               BP.kL, BP.votL, BP.corL, BP.pk, BP.bb_b, BP.b_b, 
               BP.b_0, BP.masb, BP.mas0 }).
  + proc.
    sp; if=>//=.
    inline*; wp.
    call(: true).
    swap{2} 1 1.
    rnd; rnd{2}; wp.
    by auto; smt.
  + by proc; sim. 
  + by proc; sim.
  + by proc; auto. 
  + by proc; sim.
  + by proc; wp.

  inline*; wp.
  call(: true).
  while (={i, BP.kL}); first by sim.
  auto=>/>; progress.
  pose a:= fun (x : ident * mask * code) => oget votL_R.[x.`1].
  pose b:= fun (x : ident * mask * code) => x.`2 - oget wL_R.[x.`1].
  rewrite -?filter_predI  /predI /predC /get_id /(\o) //=. 
  have ->: (fun (x : ident * mask * code) =>
           (verify (oget kL_R.[x.`1]) x.`2 x.`3 /\ ! (x.`1 \in corL_R)) /\
           (x.`1 \in regL_R)) = 
           (fun (x : ident * mask * code) =>
           verify (oget kL_R.[x.`1]) x.`2 x.`3 /\
           (x.`1 \in regL_R) /\ ! (x.`1 \in corL_R)).
    rewrite fun_ext /(==); move => x. smt.
  have ->: (fun (x : ident * mask * code) =>
           (verify (oget kL_R.[x.`1]) x.`2 x.`3 /\  (x.`1 \in corL_R)) /\
           (x.`1 \in regL_R)) = 
           (fun (x : ident * mask * code) =>
           verify (oget kL_R.[x.`1]) x.`2 x.`3 /\
           (x.`1 \in regL_R) /\  (x.`1 \in corL_R)).
    rewrite fun_ext /(==); move => x. smt.
  by done.
qed.

module (BIND2(A: DT_Adv, BLR: LorR): CPA_Adv) (O: CPA_Oracles) ={
  module BO = {
    proc reg (id: ident): cipher option={
      var w, x, c;
      var o<- None;

      if (id \in Voters /\ !id \in BP.regL){
        w <$ dmask;
        x <$ dmask;
        c <@ O.enc(w, x);
        (* BP.cL = map snd BS.encL *)
        BP.wL.[id] <- w;
        BP.cL.[id] <- c;
        BP.regL <- BP.regL ++[id];
        o <- Some c;
      }
      return o;
    }

    proc corrupt(id: ident): (cred *idkey) option ={
      var w<-None;

      if ( id \in Voters /\ id \in BP.regL/\ BP.votL.[id] = None ){
        w <- Some (oget BP.wL.[id], oget BP.kL.[id]); 
        if (!id \in BP.corL){
          BP.corL<- BP.corL ++ [id]; 
        }
      }
      return  w;   
    }

    proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
      var b0, b1, w, l_or_r, k, t0,t1;
      var b <- None;
      if ( id \in Voters /\ 
           id \in BP.regL /\
           BP.votL.[id] = None/\
          !id \in BP.corL){
        (* credential and key *)
        w <- BP.wL.[id];
        k <- BP.kL.[id];
        b0     <- (id, v0 + oget w);
        t0     <- token (oget k) (v0 + oget w); 
        b1     <- (id, v1 + oget w);
        t1     <- token (oget k) (v1 + oget w);
        BP.votL.[id] <- v0;
        l_or_r <@ BLR.l_or_r();
        BP.mas0.[id] <- b0.`2;
        BP.masb.[id] <- (l_or_r?b0.`2:b1.`2);
        BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
        BP.b_0.[id] <- (b0.`1,b0.`2,t0);
        BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
        b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      }
      return b;
    }

    proc castC (b: ident*mask*code): unit ={
      if (b.`1 \in Voters  /\  b.`1 \in BP.regL  /\ 
          b.`1 \in BP.corL /\  !b.`1 \in map get_id BP.bb_b){
        BP.bb_b <- BP.bb_b ++ [b];
      }
    }
  
    proc castH(b: ident*mask*code): unit ={
      if (b.`1 \in Voters /\ b.`1 \in BP.regL /\ 
          BP.votL.[b.`1] <> None/\ !b.`1 \in map get_id BP.bb_b/\
          b.`2 = oget BP.masb.[b.`1]){
        BP.bb_b <- BP.bb_b ++ [b];
      }
    }
      
    proc board (): (ident*mask*code) list ={
      return BP.bb_b;
    }
  }(* end BO *)

  proc main(pk: pkey):bool ={
    var r, b, i, id, uhbb, ucbb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;
    BP.masb <- map0;
    BP.mas0 <- map0;
    
    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    BP.pk <- pk;
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    A(BO).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f BP.bb_b);

    ucbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (x.`2 - oget BP.wL.[x.`1]) in
            map g (filter f (BP.bb_b));    

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A(BO).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
  
}.


local lemma b_rand_cpa_left_2 (LR<: LorR{A_DT, BP, BS,E}) &m:
  Pr[B2(MV(E),A_DT,LR, BR2(MV(E),E,LR)).main() @ &m : res] =
  Pr[CPA(E,BIND2(A_DT,LR), Left).main() @ &m : res /\ size BS.encL <= size Voters].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR}==> _)=>/>.
  proc. 
  inline*; wp.
  call(: true); wp.

  conseq (: ={BP.kL, glob A_DT, BP.bb_b, BP.wL, BP.corL, BP.regL, BP.votL, BP.lrb,
              BP.bb_b, BP.b_b} /\
            size BS.encL{2} <= size Voters)=>/>.  

  call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, BP.lrb, 
            BP.pk, BP.bb_b, BP.b_b,  BP.masb}/\
         (forall x, x \in BP.corL{1} => x \in BP.regL{1} /\ x \in Voters)/\
         BP.pk{1} = BS.pk{2} /\         
         size BS.encL{2} = size BP.regL{2} /\
         (mem BP.regL{2} <= mem Voters) /\
           uniq BP.regL{2}).
  + proc. 
    sp; if=>//=.
    inline *; wp.
    call (: true).
    wp; rnd; rnd.
    auto; progress.
    + smt. smt. smt (size_cat).
    + rewrite /(<=).
      move => z.
      rewrite mem_cat mem_seq1.
      move => Hmemz. 
      smt.
    + smt(cat_uniq).   
  + proc; auto=>/>; progress. 
    smt (mem_cat). 
    move: H7; rewrite mem_cat.
    case (x \in BP.corL{2}).
    + smt.
    + smt.
  + proc. 
    inline*; sp; if=>//=.
    wp; call(: true); wp. 
    by auto.
  + proc; auto=>/>; progress.
    by move: H3; rewrite H5 H4 (andWl _ _ (H b.`1{2} H4)) (andWr _ _ (H b.`1{2} H4)) //=.
  + by proc; wp. 
  + by proc; wp.
  wp; swap{1} 16 -5.  
  while (={i, BP.kL}); first by auto.  
  wp; call{1} kgen_keypair.
  auto=>/>; progress. 
  by rewrite H4 (uniq_leq_size _ _ H6 H5).
qed.


module BX2(V: VotingScheme, E: EncScheme, LR: LorR): DT_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <$ dmask;
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt = DT_Oracles(V,LR).corrupt
  proc vote    = DT_Oracles(V,LR).vote
  proc castH   = DT_Oracles(V,LR).castH
  proc castC   = DT_Oracles(V,LR).castC
  proc board   = DT_Oracles(V,LR).board
}.

local lemma bx_rand_cpa_right_2 (LR<: LorR{A_DT, BP, BS,E}) &m:
  Pr[B2(MV(E),A_DT,LR, BX2(MV(E),E,LR)).main() @ &m : res] =
  Pr[CPA(E,BIND2(A_DT,LR), Right).main() @ &m : res /\ size BS.encL <= size Voters].
proof.
  byequiv (: ={glob A_DT, glob E, glob LR} ==> _)=>/>.
  proc. 
  inline*; wp.
  call(: true); wp.
  conseq (: ={BP.kL, glob A_DT, BP.bb_b, BP.wL, BP.corL, BP.regL, BP.votL, BP.lrb,
              BP.bb_b, BP.b_b} /\
            size BS.encL{2} <= size Voters)=>/>.  
call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, BP.lrb, 
            BP.pk, BP.bb_b, BP.b_b,  BP.masb}/\
         (forall x, x \in BP.corL{1} => x \in BP.regL{1} /\ x \in Voters)/\
         BP.pk{1} = BS.pk{2} /\         
         size BS.encL{2} = size BP.regL{2} /\
         (mem BP.regL{2} <= mem Voters) /\
           uniq BP.regL{2}).
  + proc. 
    sp; if=>//=.
    inline *; wp.
    call (: true).
    wp; rnd; rnd.
    auto; progress.
    + smt. smt. smt (size_cat).
    + rewrite /(<=).
      move => z.
      rewrite mem_cat mem_seq1.
      move => Hmemz. 
      smt.
    + smt(cat_uniq).   
  + proc; auto=>/>; progress. 
    smt (mem_cat). 
    move: H7; rewrite mem_cat.
    case (x \in BP.corL{2}).
    + smt.
    + smt.
  + proc. 
    inline*; sp; if=>//=.
    wp; call(: true); wp. 
    by auto.
  + proc; auto=>/>; progress.
    by move: H3; rewrite H5 H4 (andWl _ _ (H b.`1{2} H4)) (andWr _ _ (H b.`1{2} H4)) //=.
  + by proc; wp. 
  + by proc; wp.
  wp; swap{1} 16 -5.  
  while (={i, BP.kL}); first by auto.  
  wp; call{1} kgen_keypair.
  auto=>/>; progress. 
  by rewrite H4 (uniq_leq_size _ _ H6 H5).
qed.


local lemma b_rand_bx_rand_2 (LR<: LorR{A_DT, BP, BS, E}) &m:
  `| Pr[B2(MV(E),A_DT,LR, BR2(MV(E),E,LR)).main() @ &m : res] -
     Pr[B2(MV(E),A_DT,LR, BX2(MV(E),E,LR)).main() @ &m : res] | 
<=
  `| Pr[CPA(E,BIND2(A_DT,LR), Left).main() @ &m : res /\ size BS.encL <= size Voters]-
     Pr[CPA(E,BIND2(A_DT,LR), Right).main() @ &m : res /\ size BS.encL <= size Voters]|.
proof.
  by rewrite (b_rand_cpa_left_2 LR &m) (bx_rand_cpa_right_2 LR &m).
qed.

local lemma BINDmain_ll2 (LR<: LorR {A_DT}) (O<: CPA_Oracles{BIND2(A_DT,LR)}):
  islossless LR.l_or_r =>
  islossless O.enc =>
  islossless BIND2(A_DT,LR,O).main.
proof.
  move => LR_ll Oenc_ll.
  proc. 
  call (ADa2_ll (<: BIND2(A_DT, LR, O).BO) ).
  wp; progress.
  wp; call (ADa1_ll (<: BIND2(A_DT, LR, O).BO) _ _ _ _ _).  
  + proc.  
    sp; if =>//=.
    wp; call Oenc_ll; rnd; rnd.
    by auto; progress; smt.

  + by proc; wp. 
  + proc. 
    sp; if=>//=.
    wp; call LR_ll; wp.
    by auto; smt (drand_lossless).
  + by proc; wp.
  + by proc; wp.

  while{1} (true) (size Voters - i).
    by auto=>/>; smt.
  by auto; smt.
qed.

(* Lemma apply the hybrid argument *)
local lemma hybrid2 (LR<: LorR {A_DT, BP, BS, E, H.Count, H.HybOrcl, WrapAdv}) &m :
  islossless LR.l_or_r =>
  `|Pr[CPA(E,BIND2(A_DT,LR),Left).main() 
            @ &m: res /\ size BS.encL <= size Voters ] -
    Pr[CPA(E,BIND2(A_DT,LR),Right).main() 
            @ &m: res /\ size BS.encL <= size Voters ]|
 = (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND2(A_DT,LR),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND2(A_DT,LR),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
proof.
  move => LR_ll.
  rewrite eq_sym.
  cut Ht: forall (n a b : real), 
          n >0%r => n * a = b <=> a = 1%r/n * b by progress; smt.  
  pose a:= `|Pr[CPA(E,WrapAdv(BIND2(A_DT,LR),E),Left).main() 
                      @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
             Pr[CPA(E,WrapAdv(BIND2(A_DT,LR),E),Right).main() 
                      @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
  pose b:= `|Pr[CPA(E,BIND2(A_DT,LR),Left).main() 
                       @ &m: res /\ size BS.encL <= size Voters ] -
             Pr[CPA(E,BIND2(A_DT,LR),Right).main() 
                       @ &m: res /\ size BS.encL <= size Voters ]|.
  rewrite -/a -/b.
  rewrite (Ht _ a b). 
    cut t:= n_pos.
    by smt.
  rewrite /a /b. 

  cut := CPA_multi_enc E  (BIND2(A_DT,LR)) Ek_ll Ee_ll Ed_ll _ &m.
  + move => O.
    exact/(BINDmain_ll2 LR O LR_ll).
          
  by done.
qed.

module BO2(V: VotingScheme', E: EncScheme, LR: LorR, H: ARO): DT_Oracles = {
 
  proc reg (id: ident): cipher option ={
    var w, x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      w <@ H.o(id);
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, b1, w, t0, t1, k, l_or_r;
    var b <- None;
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\ 
         id \in BP.regL /\
         BP.votL.[id] = None/\
        !id \in BP.corL){

      
      w <@ H.o(id);
      k <- BP.kL.[id];
      b0     <@ V(H).vote(id, v0, w);
      t0     <- token (oget k) b0.`2;
      b1     <@ V(H).vote(id, v1, w);
      t1     <- token (oget k) b1.`2;
      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      BP.mas0.[id] <- b0.`2;
      BP.masb.[id] <- (l_or_r?b0.`2:b1.`2);
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_0.[id] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }
  
  proc corrupt(id: ident): (cred *idkey) option={
    var x;
    var w<- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\
         id \in BP.regL /\
         BP.votL.[id] = None){
      x<@ H.o(id);
      w <- Some (x, oget BP.kL.[id]);  
      (* update corrupted list *)
      if (!id \in BP.corL){
        BP.corL <- BP.corL ++ [id];
      }
    }
    return  w;   
  }
  proc castC (b: ident*mask*code): unit ={
    if (b.`1 \in Voters  /\  b.`1 \in BP.regL  /\ 
        b.`1 \in BP.corL /\  !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
  
  proc castH(b: ident*mask*code): unit ={
    if (b.`1 \in Voters /\ b.`1 \in BP.regL /\ 
        BP.votL.[b.`1] <> None/\ !b.`1 \in map get_id BP.bb_b/\
        b.`2 = oget BP.masb.[b.`1]){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
      
  proc board (): (ident*mask*code) list ={
    return BP.bb_b;
  }
}.

local module B2'(V: VotingScheme', A: DT_Adv, LR: LorR, H: Oracle, O: DT_Oracles) = {

  proc main() : bool = {
    var r, b, i, id, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.masb <- map0;
    BP.mas0 <- map0;
  
    H.init();
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A(O).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f ( BP.bb_b));

    cbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            filter f ( BP.bb_b);
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);
    (* make guess *)
    b <@ A(O).a2(r,BP.b_b,BP.bb_b);
    return b;

  }
}.

(* to much copy-paste, see way to simplify *)
local lemma bx_rand_b_lazy_2 (LR<: LorR{A_DT, BP, BS,E, LO}) &m:
  Pr[B2(MV(E),A_DT,LR, BX2(MV(E),E,LR)).main() @ &m : res] =
  Pr[B2'(MV_RO(E),A_DT,LR,LO, BO2(MV_RO(E),E,LR,LO)).main() @ &m : res].
proof.
  byequiv (: ={glob A_DT, glob E, glob LR} ==> _)=>/>.
  proc.
  sim.
  while{2} ( 0 <= i{2} /\
             (forall x, x \in cbb{2} =>
                BP.wL.[x.`1]{1} = LazyRO.RO.m.[x.`1]{2} /\
                (x.`1 \in dom LazyRO.RO.m{2}))/\
             ucbb{2} = map (fun (x: ident*mask*code), x.`2- oget LazyRO.RO.m{2}.[x.`1])
                           (take i{2} cbb{2}))
           (size cbb{2} - i{2}); progress.
    inline *. 
    rcondf 4; progress.
      rnd; wp.
      auto; progress; smt.
    wp; rnd; wp.
    auto; progress; smt.

  wp; call(: ={ glob LR, glob E, BP.corL, BP.kL, BP.wL, BP.regL, BP.votL, 
                BP.lrb, BP.pk, BP.bb_b, BP.b_b, BP.masb}/\
         (forall x, x \in BP.corL{1} => x \in BP.regL{1} /\ x \in Voters)/\
         (forall x, x \in BP.regL{2} =>
            BP.wL.[x]{1} = LazyRO.RO.m.[x]{2}/\
                (x\in dom LazyRO.RO.m{2}))/\
         (forall id, !id \in BP.regL{2} =>
                ! (id \in dom LazyRO.RO.m{2}))/\
         (forall id, BP.votL.[id]{1} <> None =>
            id \in BP.regL{2})).
  + proc. 
    sp; if =>//=.
    inline *.
    rcondt{2} 3; progress.
      rnd; wp.
      auto; progress; smt.
    wp; call(: true); rnd; wp; rnd.
    auto=>/>; progress. 
    + smt (getP). 
    + smt (getP mem_cat). smt.
    + move: H7; rewrite mem_cat mem_seq1; move => Hx. 
      smt(getP).
    + move: H7; rewrite mem_cat mem_seq1; move => Hx. 
      smt.
    + move: H7; rewrite ?mem_cat //=. 
      smt.
    + smt (mem_cat).  
  + proc.
    inline *. 
    sp 1 1.
    if=>//=.
    rcondf{2} 3; progress.
    + auto=>/>; progress. 
      cut := H0 id{hr} H4.
      by elim => [Hj Ho].
    auto=>/>; progress.
    + by rewrite dmask_lossless.
    + cut := H0 id{2} H4.
      elim => [Hj Ho]. 
      by rewrite Hj.
    + smt (mem_cat). smt (mem_cat).
    + cut := H0 id{2} H4.
      elim => [Hj Ho]. 
      by rewrite Hj. 
    
  + proc.
    sp; if =>//=.   
    inline *.  
    rcondf{2} 3; progress.
      by auto=>/>; progress; smt (@NewFMap).
    wp; call(: true). 
    wp; rnd{2}; wp. 
    auto=>/>; progress. 
    + smt (dmask_lossless).
    + smt (getP). smt (getP). smt (getP). smt (getP). smt (getP).
 
  + proc; auto=>/>; progress.
    by move: H3; rewrite H5 H4 (andWl _ _ (H b.`1{2} H4)) (andWr _ _ (H b.`1{2} H4)) //=.
  + by proc; wp.
  + by proc; wp.

  inline *; wp; call(: true).
  while (={i, BP.kL}); first by auto.
  auto=>/>; progress. 
  + smt (@NewFMap). smt (map0P). 
  + smt(mem_filter).
  + smt (mem_filter).
  + smt (take0).
  + by rewrite lezNgt subz_gt0 in H9. 
  + rewrite take_oversize; first by rewrite lezNgt. 
    move: H8.
    pose L:= (filter
     (fun (x : ident * mask * code) =>
        verify (oget kL_R.[x.`1]) x.`2 x.`3 /\
        (x.`1 \in regL_R) /\ (x.`1 \in corL_R)) (bb_b_R)). 
    move => H8.  
    rewrite -(eq_in_map _ _ L).
    move => x HxL. 
    by rewrite //= (andWl _ _ (H9 x HxL)).
qed.

local module (D3 (V: VotingScheme') (E: EncScheme) (LR: LorR): Dist) (H : ARO) = {
  module O = BO2(V,E, LR,H)

  proc distinguish() : bool ={
    var r, b, i, id, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.masb<- map0;
    BP.mas0<- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A_DT(O).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f ( BP.bb_b));

    cbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            filter f ( BP.bb_b);    
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    (* make guess *)
    b <@ A_DT(O).a2(r, BP.b_b, BP.bb_b);
    return b;

  }
}.

local lemma b_ind_eo_2 (LR<: LorR{A_DT,E, BP, EO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,EO, BO2(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[IND(EO, D3(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(EO, D3(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 12. 
  by sim.
qed.

local lemma b_ind_lo_2 (LR<: LorR{A_DT,E, BP, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,LO, BO2(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[IND(LO, D3(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(LazyRO.RO, D3(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 12. 
  by sim.
qed.

local lemma bpriv_b_2 (LR<: LorR{A_DT,E, BP, EO, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,LO, BO2(MV_RO(E),E,LR,LO)).main() @ &m : res]=
  Pr[B2'(MV_RO(E),A_DT,LR,EO, BO2(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  rewrite  (b_ind_eo_2 LR &m) ( b_ind_lo_2 LR &m).
  byequiv  (: ={glob D3(MV_RO(E),E, LR)} ==> _)=>/>.
  cut Hu:= (eagerRO (D3(MV_RO(E),E,LR)) _). 
  + by move => _; rewrite dmask_lossless. 
  by exact/Hu.
qed.


(* removing the eager call in registration *)
module CO2(V: VotingScheme', E: EncScheme, LR: LorR, H: ARO): DT_Oracles = {
 
  proc reg (id: ident): cipher option={
    var x,c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc vote    = BO2(V, E, LR, H).vote
  proc corrupt = BO2(V, E, LR, H).corrupt
  proc castH   = BO2(V,E,LR,H).castH
  proc castC   = BO2(V,E,LR,H).castC
  proc board   = BO2(V,E,LR,H).board
}.


local lemma b_eager_rem_call_2 (LR<: LorR{A_DT,E, BP, EO, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,EO, BO2(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[B2'(MV_RO(E),A_DT,LR,EO, CO2(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  byequiv (: ={glob E, glob A_DT, glob LR} ==> _)=>/>.
  proc.
  sim.
  call(: ={glob LR, glob RO, glob E, BP.lrb, BP.kL, BP.corL, BP.votL, 
           BP.regL, BP.pk, BP.bb_b, BP.b_b, BP.b_0, BP.bb_0,
           BP.masb, BP.mas0}); [2..6: sim]. 
  + proc. 
    inline *; wp.
    sp; if=>//=.
    by wp; call(: true); rnd; wp.   

  conseq(: ={glob LR, glob RO, glob E, BP.corL, BP.regL, BP.cL, 
             BP.pk, BP.lrb, BP.kL, BP.votL, BP.bb_b, BP.b_b, BP.b_0, BP.bb_0,
             BP.masb, BP.mas0})=>/>.
   progress. 
  by sim.
qed.

local module (D4 (V: VotingScheme') (E: EncScheme) (LR: LorR): Dist) (H : ARO) = {
  module O = CO2(V,E, LR,H)

  proc distinguish() : bool ={
    var r, b, i, id,  uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.masb <- map0;
    BP.mas0 <- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V(H).setup();

    A_DT(O).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f  BP.bb_b);

    cbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            filter f BP.bb_b;    
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <@ H.o(id);
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);
    (* make guess *)
    b <@ A_DT(O).a2(r, BP.b_b, BP.bb_b);
    return b; 
  }
}.

local lemma b_ind_eo_back_2 (LR<: LorR{A_DT,E, BP, EO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,EO, CO2(MV_RO(E),E,LR,EO)).main() @ &m : res] =
  Pr[IND(EO, D4(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(EO, D4(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 12. 
  by sim.
qed.

local lemma b_ind_lo_back_2 (LR<: LorR{A_DT,E, BP, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,LO, CO2(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[IND(LO, D4(MV_RO(E),E,LR)).main() @ &m: res].
proof.
  byequiv=>/>. 
  proc. 
  inline IND(LazyRO.RO, D4(MV_RO(E),E, LR)).D.distinguish. 
  wp; sim.
  swap{2} 1 12. 
  by sim.
qed.

local lemma bpriv_b_back_2 (LR<: LorR{A_DT,E, BP, EO, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,LO, CO2(MV_RO(E),E,LR,LO)).main() @ &m : res]=
  Pr[B2'(MV_RO(E),A_DT,LR,EO, CO2(MV_RO(E),E,LR,EO)).main() @ &m : res].
proof.
  rewrite  (b_ind_eo_back_2 LR &m) ( b_ind_lo_back_2 LR &m).
  byequiv  (: ={glob D4(MV_RO(E),E, LR)} ==> _)=>/>.
  cut Hu:= (eagerRO (D4(MV_RO(E),E,LR)) _). 
  + by rewrite dmask_lossless. 
  by exact/Hu.
qed.

(* removing the eager call in registration *)
module BOB2(V: VotingScheme, E: EncScheme, LR: LorR): DT_Oracles = {

   proc reg (id: ident): cipher option={
    
    var x, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      x <$ dmask;
      c <@ E.enc(BP.pk,x);
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt(id: ident): (cred *idkey) option={
    var x;
    var w<- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\ id \in BP.regL /\
         BP.votL.[id] = None){
      x <$ dmask;  
      (* update corrupted list *)
      if (!id \in BP.corL){
        BP.corL <- BP.corL ++ [id];
        BP.wL.[id] <- x;
      }
      w<- Some (oget BP.wL.[id], oget BP.kL.[id]);
    }
    return  w;   
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, t0, b1, t1, w, k, l_or_r;
    var b <- None;
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\  id \in BP.regL /\
        BP.votL.[id] = None/\ !id \in BP.corL){
      (* credential and ciphertext *)
      w <$ dmask;
      BP.wL.[id]<- w;
      k <- BP.kL.[id];
      (* create ballots *)
      b0     <@ V.vote(id, v0, w);
      t0     <@ V.token(b0.`2, oget k);
      b1     <@ V.vote(id, v1, w);
      t1     <@ V.token(b1.`2, oget k);

      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      BP.mas0.[id] <- b0.`2;
      BP.masb.[id] <- (l_or_r?b0.`2:b1.`2);
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_0.[id] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1)); 
      b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }

  proc castC (b: ident*mask*code): unit ={
    if (b.`1 \in Voters  /\  b.`1 \in BP.regL  /\ 
        b.`1 \in BP.corL /\  !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
  
  proc castH(b: ident*mask*code): unit ={
    if (b.`1 \in Voters /\ b.`1 \in BP.regL /\ 
        BP.votL.[b.`1] <> None/\ !b.`1 \in map get_id BP.bb_b/\
        b.`2 = oget BP.masb.[b.`1]){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
      
  proc board (): (ident*mask*code) list ={
    return BP.bb_b;
  }
  
}.

local module BOL2(V: VotingScheme, A: DT_Adv, LR: LorR, O: DT_Oracles) = {

  proc main() : bool = {

    var r, b, i, id, uhbb, cbb, ucbb, m,c,w;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    BP.masb <- map0;
    BP.mas0<- map0;

    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <$ didkey;
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    uhbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                        x.`1 \in BP.regL /\ !x.`1 \in BP.corL in
            let g = fun (x: ident*mask*code),
                        (oget BP.votL.[x.`1]) in
            map g (filter f  BP.bb_b);

    cbb <- let f = fun (x: ident*mask*code), 
                        verify (oget BP.kL.[x.`1]) x.`2 x.`3 /\
                         x.`1 \in BP.regL /\ x.`1 \in BP.corL in
            filter f  BP.bb_b;  
    i <- 0;
    ucbb<-[];
    while (i < size cbb){
      (id,m,c) <- nth witness cbb i;
      w <- oget BP.wL.[id];
      ucbb <- ucbb ++ [m - w];
      i <- i + 1;
    }

    r <- Rho (uhbb ++ ucbb);

    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b; 
  }
}.

local lemma remove_ro_2 (LR<: LorR{ E, A_DT, BP, LO}) &m:
  Pr[B2'(MV_RO(E),A_DT,LR,LO, CO2(MV_RO(E),E,LR,LO)).main() @ &m : res] =
  Pr[BOL2(MV(E),A_DT, LR, BOB2(MV(E),E,LR)).main() @ &m : res].
proof.
  byequiv (: ={glob A_DT, glob LR, glob E} ==> _)=>/>.
  proc.
  call(: true).
  wp.
  inline*. 
  while (={i, cbb, ucbb} /\ 0<= i{1} /\
         (forall id, id \in (map get_id cbb{2}) =>
            BP.wL.[id]{2} = LO.m.[id]{1}/\
                (id \in dom LO.m{1}))).
    inline *. 
    rcondf{1} 4; progress.
      rnd; wp.
      auto; progress; smt.
    wp; rnd{1}; wp.
    auto; progress; smt.
  wp; call(: ={ glob LR, glob E, BP.corL, BP.regL, BP.votL,
                BP.pk, BP.kL, BP.lrb, BP.bb_b,BP.b_b, BP.b_0,
                BP.masb}/\
         
         (forall id, id \in BP.corL{2} =>
            BP.wL.[id]{2} = LO.m.[id]{1}/\
                (id \in dom LO.m{1}))/\
         (forall id, id \in Voters /\ 
                     BP.votL{1}.[id] = None/\
                     ! id \in BP.corL{2} =>
                ! (id \in dom LO.m{1}))).
  + proc. 
    sp; if =>//=. 
    wp; call(: true); rnd.
    by auto; progress.
  
  + proc.
    inline *. 
    sp. if =>//=. 
    seq 2 1:  (w{2} = None /\
               w{1} = None /\
               ={id, glob LR, glob E, BP.corL, BP.regL, BP.votL, BP.pk, BP.kL, BP.lrb,
                 BP.bb_b, BP.b_b, BP.masb, BP.b_0} /\
               (forall (id0 : ident),
                   id0 \in BP.corL{2} =>
                   BP.wL{2}.[id0] = LazyRO.RO.m{1}.[id0] /\ (id0 \in dom LazyRO.RO.m{1})) /\
               (forall (id0 : ident),
                   (id0 \in Voters) /\ BP.votL{1}.[id0] = None /\ ! (id0 \in BP.corL{2}) =>
                   ! (id0 \in dom LazyRO.RO.m{1})) /\
               (id{1} \in Voters) /\ (id{1} \in BP.regL{1}) /\ BP.votL{1}.[id{1}] = None/\
               y{1}= x{2} /\ x0{1} =id{1}).
     by rnd; wp.
    if =>//=. progress. smt(@NewFMap). smt(@NewFMap).
    rcondt{1} 4; progress. 
        by auto; progress; smt. 
    auto; progress.
    + smt (getP). 
    + smt (mem_cat getP).
    + move: H5; rewrite mem_cat mem_seq1 //=; move => Hm. smt(@NewFMap).
    + move: H7; rewrite mem_cat mem_seq1 //=; move => Hm. smt.
     
    by auto =>/>; smt. 
    
  + proc.
    sp; if =>//=.   
    inline *.  
    rcondt{1} 3; progress.
      by auto=>/>; smt.
    wp; call(: true); wp. 
    auto;progress. 
    + smt(getP). smt(getP). smt(getP). smt(getP). smt. smt. smt. smt.
  + by proc; wp.
  + by proc; wp.
  + by proc; wp.

  inline *; wp; call(: true).
  while (={BP.kL,i}); first by sim.
  auto=>/>; progress.
  + smt (dom0 in_fset0). 
  + cut := H2 id0 _.
      move: H4; rewrite mapP. 
      elim => y. rewrite mem_filter; simplify.
      elim => [[[Hv [Hr Hc]] _ ] Hid].
      by rewrite Hid /get_id Hc.
    by done.
  + cut := H2 id0 _.
      move: H4; rewrite mapP. 
      elim => y. rewrite mem_filter.
      elim => [[[Hv [Hr Hc]] _ ] Hid].
      by rewrite Hid /get_id Hc.
    by done.
qed.


local lemma left_right_2 &m:
  Pr[BOL2(MV(E),A_DT, Left, BOB2(MV(E),E,Left)).main() @ &m : res] =
  Pr[BOL2(MV(E),A_DT, Right, BOB2(MV(E),E,Right)).main() @ &m : res].
proof.
  byequiv =>/>.
  proc.
  call(: true).
  wp; while (={i, cbb, ucbb}/\ 0<= i{1}/\
             (forall id, 
               id \in map get_id cbb{1} => 
               BP.wL{1}.[id] =  BP.wL{2}.[id])).
    auto; progress. 
    + rewrite (H0 (nth witness cbb{2} i{2}).`1 _). 
        rewrite mapP /get_id. smt.
      smt. 
    + smt.
  wp. conseq (: ={glob A_DT, BP.bb_b, BP.kL, BP.corL, BP.votL, BP.kL, BP.regL, BP.pk,
                  BP.b_b}/\
                (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
                (forall id, id \in BP.corL{1} => 
                    BP.wL{1}.[id] =  BP.wL{2}.[id])
                ).
  progress.
  + cut := H0 id0 _.
    move: H1; rewrite mapP. 
      elim => y. rewrite mem_filter.
      elim => [[Hm _ ] Hid].
      smt.
    by done. 
   
  wp; call(: ={glob E, BP.kL, BP.corL, BP.votL, BP.kL, BP.regL, BP.pk, BP.bb_b, BP.b_b, 
             BP.masb}/\
             (forall x, BP.lrb{1}.[x] <> None <=> BP.lrb{2}.[x] <> None)/\
             (forall id, id \in BP.corL{1} => 
                    BP.wL{1}.[id] =  BP.wL{2}.[id])).
  + proc. 
    sp; if =>//=; auto.
    call(: true); rnd.
    by auto.
  + proc. 
    sp; if=>/>; auto; progress. 
    + smt (getP). 
    + smt (getP mem_cat).
    + smt. 
  + proc. 
    sp; if=>//; auto. 
    inline*; wp. 
    rnd (fun x, x- v1{1}+v0{1}) (fun x, x + v1{1}-v0{1}).
    auto; progress. 
    + smt. smt. smt. smt. smt. smt.
    + move : H9.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL) ).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
               token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2}))).
      smt (getP).
    + move : H9.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL)).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2}))). 
      smt (getP).
    + move : H10.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL)).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2}))). 
      smt (getP).
    + move : H10.
      have ->: v1{2} + (wL - v1{2} + v0{2}) = v0{2} + wL by smt.
      pose A:= (id{2}, v0{2} + wL,
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + wL)).
      pose B:=(id{2}, v0{2} + (wL - v1{2} + v0{2}),
                 token (oget BP.kL{2}.[id{2}]) (v0{2} + (wL - v1{2} + v0{2}))). 
      smt (getP).
    + smt (getP).
  + by proc; wp.
  + by proc; wp.
  + by proc; wp.
      
  inline*; wp.
  call(: true). 
  while (={BP.kL, i}); first by sim.
  by auto=>/>; progress. 
qed.

lemma dishonest_token &m:
  `|  Pr[DT(MV(E),A_DT,Left).main() @ &m : res] -
      Pr[DT(MV(E),A_DT,Right).main() @ &m : res] | 
<=
  (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND2(A_DT,Left),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND2(A_DT,Left),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|+
  (size Voters)%r *
  `|Pr[CPA(E,WrapAdv(BIND2(A_DT,Right),E),Left).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ] -
    Pr[CPA(E,WrapAdv(BIND2(A_DT,Right),E),Right).main() 
            @ &m: res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1 ]|.
proof.
  rewrite (bpriv_bt_2 Left &m) (bpriv_bt_2 Right &m).
  rewrite (bt_bsp_2 Left &m) (bt_bsp_2 Right &m).
  rewrite (bsp_bspp_2 Left &m) (bsp_bspp_2 Right &m).
  rewrite (bt_bsp_3 Left &m) (bt_bsp_3 Right &m).
  rewrite (bv_b_rand_2 Left &m) (bv_b_rand_2 Right &m).
  cut HLeft:= (b_rand_bx_rand_2 Left &m).
  cut HRight:= (b_rand_bx_rand_2 Right &m).
  rewrite (hybrid2 Left &m _) in HLeft; first by proc.
  rewrite (hybrid2 Right &m _) in HRight; first by proc.
  pose a:= Pr[B2(MV(E), A_DT, Left, BR2(MV(E), E, Left)).main() @ &m : res].
  pose b:=Pr[B2(MV(E), A_DT, Right, BR2(MV(E), E, Right)).main() @ &m : res].
  pose c:= (size Voters)%r *
`|Pr[CPA(E, WrapAdv(BIND2(A_DT, Left), E), Left).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1] -
  Pr[CPA(E, WrapAdv(BIND2(A_DT, Left), E), Right).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1]|.
  pose d:= (size Voters)%r *
`|Pr[CPA(E, WrapAdv(BIND2(A_DT, Right), E), Left).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1] -
  Pr[CPA(E, WrapAdv(BIND2(A_DT, Right), E), Right).main() @ &m :
     res /\ WrapAdv.l <= size Voters /\ size BS.encL <= 1]|.
  pose e:= Pr[B2(MV(E), A_DT, Left, BX2(MV(E), E, Left)).main() @ &m : res].
  pose f:=Pr[B2(MV(E), A_DT, Right, BX2(MV(E), E, Right)).main() @ &m : res].
  move : HLeft HRight; rewrite -/a -/b -/c -/d -/e -/f.
  move => HL HR.
  cut Htrig: forall (a b c d: real), `|a - b| <= `|a -c| + `|c - d| + `|b-d| by smt.
  cut HO:= Htrig a b e f.
  have Hef: e = f.
    rewrite /e /f.
    rewrite ( bx_rand_b_lazy_2 Left &m) ( bx_rand_b_lazy_2 Right &m).
    rewrite ( bpriv_b_2 Left &m) ( bpriv_b_2 Right &m).
    rewrite ( b_eager_rem_call_2 Left &m) ( b_eager_rem_call_2 Right &m).
    rewrite -( bpriv_b_back_2 Left &m) -( bpriv_b_back_2 Right &m).
    rewrite ( remove_ro_2 Left &m) ( remove_ro_2 Right &m).
    by rewrite (left_right_2 &m).
  move: HO; rewrite Hef //=. 
  smt(@Real).
qed. 


end section Dishonest_Token.
