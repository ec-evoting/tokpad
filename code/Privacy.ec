require import Int List NewFMap.
require import LeftOrRight.

(* * Voting scheme syntax *)
(* ---------------------------------------------------------------------- *)
type mask.
type pkey, skey, idkey, ident, cipher, code.
type result.

(* to clean the Vote alg def *)
type cred = mask.
type vote = mask.

(* list of voters *)
op Voters: ident list.

module type VotingScheme = {

  (* election keys *)
  proc setup()                                                 
    : pkey * skey                    

  (* secret credential and public encryption of that credential *)
  proc register (id: ident, pk:pkey)
    : cred * cipher 
 
  (* token key *)
  proc devkey (id: ident)
    : idkey

  (* hide the vote in mask *)
  proc vote(id: ident, v: vote, w: cred)
    : (ident * mask)                     

  (* token  *)
  proc token(m: mask, k:idkey): code

  (* verification of token *)
  proc valid (k:idkey, b: ident*mask*code): bool

  (* election result  *)
  proc tally(bb: (ident * mask * code) list, 
             kL: (ident, idkey) fmap, 
             cL: (ident, cipher) fmap, sk: skey)
    : result                                      
}.

(* Dishonest Browser oracle types *)
module type DB_Oracles = {

  (* registration oracle *)
  proc reg (id: ident)                   : cipher option
  (* corrupt voters oracle *)
  proc corrupt(id: ident)                : (cred * idkey) option
  (* voting oracles *)
  proc vote(id: ident, vo v1 : vote)     : (ident*mask*code) option
  (* cast oracle: honest or dishonest *)
  proc castH(b: ident*mask*code)       : unit 
  proc castC(b: ident*mask*code)       : unit
  (* ballot box *)
  proc board(): (ident*mask*code) list
}.

(* global vars *)
module BP ={
  var wL : (ident, cred)   fmap
  var cL : (ident, cipher) fmap 
  var kL : (ident, idkey)  fmap
  var kk : (ident * idkey) list
  var pk : pkey
  var sk : skey
  var bad: bool
  var ibad: int
  var jbad: int
  var id  : ident

  var regL : ident list
  var corL : ident list
  var votL : (ident, vote ) fmap
  var mas0 : (ident, mask) fmap
  var masb : (ident, mask) fmap
  var lrb  : ((ident * mask * code), (ident * mask * code)) fmap
  var lrm  : (ident*mask, mask) fmap
  
  var b_0  : (ident, ident * mask * code) fmap
  var b_b  : (ident, ident * mask * code) fmap 

  var bb  : (ident * mask * code) list
  var bm  : (ident, ident * mask * code) fmap
  var bb_0 : (ident*mask*code) list
  var bb_b : (ident*mask*code) list
}.

(* get the identity *)
op get_id   (x: ident * 'a * 'b) =  x.`1.
            
(* DB Oracles *)  
module DB_Oracles(V: VotingScheme, LR: LorR): DB_Oracles = {
 
  proc reg (id: ident): cipher option={
    var w, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      (w,c)  <@ V.register(id,BP.pk);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt(id: ident): (cred *idkey) option={
    var w <- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\ id \in BP.regL /\ BP.votL.[id] = None){
      w <- Some (oget BP.wL.[id], oget BP.kL.[id]);  
      (* update corrupted list *)
      if ( !id \in BP.corL ){
        BP.corL <- BP.corL ++ [id];
      }
    }
    return w;   
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, t0, b1, t1, w, k, l_or_r;
    var b <- None;
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\ 
         id \in BP.regL /\
         BP.votL.[id] = None/\
        !id \in BP.corL){
      (* credential and key *)
      w <- BP.wL.[id];
      k <- BP.kL.[id];

      (* create ballots *)
      b0     <@ V.vote(id, v0, oget w);
      t0     <@ V.token(b0.`2, oget k);
      b1     <@ V.vote(id, v1, oget w);
      t1     <@ V.token(b1.`2, oget k);
      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_0.[id] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      b <-Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }
  
  (* cast honest ballots *)
  proc castH(b: ident*mask*code): unit ={
    (* honest voters that have created a ballot and 
       did not submit something before *)
    if (b.`1 \in BP.regL /\ !b.`1 \in BP.corL /\
        BP.votL.[b.`1] <> None /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
  (* cast dishonest balllots *)
  proc castC(b: ident*mask*code): unit ={
    if(b.`1 \in BP.corL /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  (* ballot box *)
  proc board(): (ident * mask*code) list ={
    return BP.bb_b;
  }

}.


(* DB adversary type *)
module type DB_Adv(O: DB_Oracles) = {

  proc a1 (pk: pkey): unit { O.reg O.corrupt O.vote O.castH O.castC O.board}
  (* gets the result and makes a guess *)
  proc a2(r: result, bbv: (ident, ident * mask * code) fmap, 
                     bb: (ident*mask*code) list): bool {}
}.

(* dishonest browser privacy property *)
module DB_L(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, vbb, e,imt;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    (* take all valid ballots *)
    vbb <- [];
    i <- 0;
    while (i < size BP.bb_b){
      imt <- nth witness BP.bb_b i;
      e <@ V.valid(oget BP.kL.[imt.`1],imt);
      if (e) { 
        vbb <- vbb ++ [imt];
      }
      i <- i+1;
    }
    (* compute result *)
    r <@ V.tally(vbb, BP.kL, BP.cL, BP.sk);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

(* dishonest browser privacy property *)
module DB_R(V: VotingScheme, A: DB_Adv, LR: LorR) = {

  module O = DB_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, vbb, e,imt, fbb, hbb, cbb, uhbb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_0<- [];
    BP.bb_b<- [];
    BP.b_0 <- map0;
    BP.b_b <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    (* take all valid ballots *)
    vbb <- [];
    i <- 0;
    while (i < size BP.bb_b){
      imt <- nth witness BP.bb_b i;
      e <@ V.valid(oget BP.kL.[imt.`1],imt);
      if (e) { 
        vbb <- vbb ++ [imt];
      }
      i <- i+1;
    }
    fbb <- filter ((mem BP.regL)\o get_id) vbb;
    (* honest ballots get to become v0 *)
    hbb <- filter (predC ((mem BP.corL) \o get_id)) fbb;
    uhbb <- let h = fun (x: ident*mask*code),
                       oget  BP.b_0.[x.`1] in
            map h hbb;
    (* corrupt ballots get decrypted *)
    cbb <- filter ((mem BP.corL) \o get_id) fbb;
    r <@ V.tally (uhbb++cbb, BP.kL, BP.cL, BP.sk);


    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.

(* Dishonest token Oracles 
   -- is identical with Dishonest Browser 
      except on honest cast queries, where only 
      the code can be changed by the adversary *)  
module type DT_Oracles = {
  (* registration oracle *)
  proc reg (id: ident)                   : cipher option
  (* corrupt voters oracle *)
  proc corrupt(id: ident)                : (cred *idkey) option
  (* voting oracles *)
  proc vote(id: ident, vo v1 : vote)     : (ident*mask*code) option
  (* cast oracle: honest or dishonest *)
  proc castC(b:ident*mask*code)   : unit
  proc castH(b:ident*mask*code)   : unit
 
  proc board():  (ident*mask*code) list
}.
            
module DT_Oracles(V: VotingScheme, LR: LorR): DT_Oracles = {
 
  proc reg (id: ident): cipher option={
    var w, c;
    var o <- None;

    (* valid id for this election and not registered *)
    if (id \in Voters /\ !id \in BP.regL){
      (w,c)  <@ V.register(id,BP.pk);
      BP.wL.[id] <- w;
      BP.cL.[id] <- c;
      BP.regL <- BP.regL ++[id];
      o <- Some c;
    }
    return o;
  }

  proc corrupt(id: ident): (cred *idkey) option={
    var w <- None;

    (* you can corrupt any valid voter 
       that is registered and has not yet voted *)
    if ( id \in Voters /\ id \in BP.regL /\ BP.votL.[id] = None){
      w<- Some (oget BP.wL.[id], oget BP.kL.[id]);  
      (* update corrupted list *)
      if (!id \in BP.corL){
        BP.corL <- BP.corL ++ [id];
      }
    }
    return w;   
  }

  proc vote(id : ident, v0 v1 : vote) : (ident*mask*code) option = {
    var b0, t0, b1, t1, w, k, l_or_r;
    var b <- None;
    
    (* valid id for this election, didn't vote before, and
       is registered and has not been corrupted *)
    if ( id \in Voters /\ 
         id \in BP.regL /\
         BP.votL.[id] = None/\
        !id \in BP.corL){
      (* credential and key *)
      w <- BP.wL.[id];
      k <- BP.kL.[id];

      (* create ballots *)
      b0     <@ V.vote(id, v0, oget w);
      t0     <@ V.token(b0.`2, oget k);
      b1     <@ V.vote(id, v1, oget w);
      t1     <@ V.token(b1.`2, oget k);
      BP.votL.[id] <- v0;
      l_or_r <@ LR.l_or_r();
      BP.mas0.[id] <- b0.`2;
      BP.masb.[id] <- (l_or_r?b0.`2:b1.`2);
      BP.lrb.[(l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1))] <- (b0.`1,b0.`2,t0);
      BP.b_0.[id] <- (b0.`1,b0.`2,t0);
      BP.b_b.[id] <- (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
      b <- Some (l_or_r?(b0.`1,b0.`2,t0):(b1.`1,b1.`2,t1));
    }
    return b;
  }

  proc castC (b: ident*mask*code): unit ={
    if (b.`1 \in BP.corL /\ !b.`1 \in map get_id BP.bb_b){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }
  
  proc castH(b: ident*mask*code): unit ={
    if (b.`1 \in Voters /\ b.`1 \in BP.regL /\ 
        BP.votL.[b.`1] <> None/\ !b.`1 \in map get_id BP.bb_b/\
        b.`2 = oget BP.masb.[b.`1]){
      BP.bb_b <- BP.bb_b ++ [b];
    }
  }

  proc board(): (ident*mask*code) list ={
    return BP.bb_b;
  }
}.


(* dishonest token adversary type *)
module type DT_Adv(O: DT_Oracles) = {

  proc a1 (pk: pkey): unit { O.reg O.corrupt O.vote O.castC O.castH O.board}
  (* gets the result and makes a guess *)
  proc a2(r: result, bbv: (ident, ident*mask*code) fmap, 
                     bb_b: (ident*mask*code) list): bool {}
}.

(* dishonest token property *)
module DT(V: VotingScheme, A: DT_Adv, LR: LorR) = {

  module O = DT_Oracles(V, LR)

  proc main() : bool = {

    var r, b, i, id, imt, e, vbb,bb;

    BP.wL  <- map0;
    BP.cL  <- map0;
    BP.kL  <- map0;
    BP.lrb <- map0;
    BP.votL<- map0;
    BP.mas0<- map0;
    BP.masb<- map0;

    BP.corL<- [];
    BP.regL<- [];
    BP.bb_b <- [];
    BP.bb_0 <- [];
    BP.b_b <- map0;
    BP.b_0 <- map0;
    
    i <- 0;
    while (i < size Voters){
      id <- nth witness Voters i;
      BP.kL.[id] <@ V.devkey(id);
      i <- i + 1;
    }

    (BP.pk, BP.sk) <@ V.setup();

    A(O).a1(BP.pk);

    vbb <- [];
    i <- 0;
    while (i < size BP.bb_b){
      imt <- nth witness BP.bb_b i;
      e <@ V.valid(oget BP.kL.[imt.`1],imt);
      if (e) { 
        vbb <- vbb ++ [imt];
      }
      i <- i+1;
    }
    (* we use lemma TokpadDef.det_tok that says there 
       exists a unique token for each mask.
       when LR=Right, any valid ballot for an honest 
       paticipant must be b_1 *)
    bb <- map (fun (x: ident*mask*code), 
                   if BP.lrb.[x] <> None
                   then oget BP.lrb.[x]
                   else x
              ) vbb;
    (* compute result *)
    r <@ V.tally(bb, BP.kL, BP.cL, BP.sk);

    (* make guess *)
    b <@ A(O).a2(r, BP.b_b, BP.bb_b);
    return b;
  }
}.
