require import Int Bool Real.
require import Distr AllCore List DatatypesExt.
require import LeftOrRight.


(* * Preliminaries *)
(* ---------------------------------------------------------------------- *)

(* types *)
type plain, cipher, pkey, skey.       

(* Global state shared between IND-1-CCA oracles and game *)
module BS = {
  var pk         : pkey
  var sk         : skey
  var qt         : int
  var eq         : bool
  var encL       : cipher list
}.

(* * Definitions *)
(* ---------------------------------------------------------------------- *)

(* encryption scheme *)
module type EncScheme = {
  proc kgen()                 : pkey * skey   
  proc enc(pk:pkey, p:plain)  : cipher       
  proc dec(sk:skey, c:cipher) : plain option 
}.

(* * Correctnes *)
(* ---------------------------------------------------------------------- *)

(* Correctness experiment *)
module Correctness (S: EncScheme) = {

  proc main(p : plain) : bool ={
    var pk, sk, c, p';

    (pk,sk) <@ S.kgen();
    c       <@ S.enc(pk,p);
    p'      <@ S.dec(sk,c);
    return (p' = Some p);
  }
}.

(* * IND-CPA security definitions *)
(* ---------------------------------------------------------------------- *)

(* IND-CPA oracles type *)
module type CPA_Oracles = {
  proc enc(p0 p1 : plain)   : cipher
}.

(* IND-CPA adversary *)
module type CPA_Adv(IO : CPA_Oracles) = {
  proc main (pk : pkey) : bool {IO.enc} 
}.

(* IND-CPA oracles implementation *)
module CPA_Oracles (S : EncScheme, LR : LorR) = {
  
  proc enc(p0 p1 : plain) : cipher = {
    var c, p, l_or_r;
    l_or_r  <@ LR.l_or_r();
    (* encrypt challenge according to b *)
    p  <- l_or_r?p0:p1; 
    c  <@ S.enc(BS.pk, p);
    BS.encL <- BS.encL ++ [c];  
    return c;
  }
}.

(* IND-CPA experiment *)
module CPA (S : EncScheme, A : CPA_Adv,  LR : LorR) = {
  module O = CPA_Oracles(S,LR)

  proc main(): bool = {
    var b';
    BS.encL       <- [];
    (BS.pk,BS.sk) <@ S.kgen();
    b' <@ A(O).main(BS.pk);
    return b';
  }
}.

(* IND-CPA oracles lossless *)

lemma CCA_Oracles_enc_ll  (S <: EncScheme) (LR <: LorR):
  islossless LR.l_or_r =>
  islossless S.enc =>
  islossless CPA_Oracles(S,LR).enc.
proof.
  move=> H_b_ll H_enc_ll; proc.
  by wp; call H_enc_ll; wp; call H_b_ll; auto.
qed.

(* * Typecheck security definitions. *)
(* ---------------------------------------------------------------------- *)

section.
  declare module S: EncScheme.
  declare module A: CPA_Adv.
  
  local lemma corr(m : plain) &m:
    exists eps, 0%r < eps /\
      Pr[Correctness(S).main(m) @ &m: res] >= 1%r - eps by [].

  local lemma CPA_n_challenge &m (n : int):
    exists eps, 
      `| Pr[CPA(S,A,Left).main()  @ &m: res /\  size BS.encL <= n] -
         Pr[CPA(S,A,Right).main() @ &m: res /\  size BS.encL <= n] | <= eps by [].
end section.

(* * Single enc-challenge implies multi enc-challenge security. *)
(* ---------------------------------------------------------------------- *)

(* We must express CPA(S,A,_) as a hybrid adversary HA that can call
   a leaks oracle O.leaks and l_or_r/right oracles O.orclL/O.orclR.
   The hybrid adversary HA and O are not allowed to share state and we
   must therefore push all operations that share state with O.orclL/O.orclR
   into the oracle O.leaks. *)

(* Inputs leaks function *)

type in_leaks =  [keys_in].

op destr_keys_in(x: in_leaks) =
   with x = keys_in => true.

(* Outputs leaks function *)

type out_leaks = [ 
  | keys_out of (pkey * skey)].

op destr_keys_out(y: out_leaks) =
   with y = keys_out yo => Some yo.

require HybridArg.

op n : { int | 0 < n } as n_pos.

clone import HybridArg as H with
  type input    <-  plain * plain,
  type output   <- cipher,
  type inleaks  <- in_leaks,
  type outleaks <- out_leaks,
  type outputA  <- bool,
  op   q        <- n
  proof *.
  realize q_pos by exact/n_pos. 

module WrapAdv(A: CPA_Adv, S : EncScheme, O : CPA_Oracles) = {
  var l0         : int
  var l          : int
  var encL       : cipher list
  var pk         : pkey
    
  module WIO = {
    proc enc( p0 p1 : plain) : cipher = {
      var c;
      if (l0 < l) {
        c  <@ S.enc(pk, p0);
      } elif (l0 = l) {
        c <@ O.enc(p0, p1);
      } else {
        c  <@ S.enc(pk,  p1);
      }
      encL <- encL ++ [c];
      l <- l + 1;
      return c;
    } 
  }
    
  module A = A(WIO)

  proc main (pk_ : pkey) : bool = {
    var r;
    pk <- pk_;
    encL <- [];
    l0 <$ [0..n - 1];
    l  <- 0;
    r <@ A.main(pk);
    return r;
  }
}.

(* ---------------------------------------------------------------------- *)
section HybridProof.
  declare module S: EncScheme {Count, BS, HybOrcl, WrapAdv}.
  declare module A: CPA_Adv {Count, BS, HybOrcl, S, WrapAdv}.

  axiom S_kgen_ll:  
    islossless S.kgen.
  axiom S_enc_ll : 
    islossless S.enc.
  axiom S_dec_ll : 
    islossless S.dec.
  axiom A_main_ll (O <: CPA_Oracles{A}):
    islossless O.enc => 
    islossless A(O).main.
 
  (* ---------------------------------------------------------------------- *)
  (* Apply Hybrid lemma *)

  (* Oracles for hybrid adversary *)
  local module OrclbImpl : Orclb = {

    module IOL = CPA_Oracles(S,Left)
    module IOR = CPA_Oracles(S,Right)
 
    proc orclL = IOL.enc
    proc orclR = IOR.enc

    proc leaks(x: in_leaks): out_leaks = {
      var r = witness;
 
      if (destr_keys_in x) {
        BS.encL       <- [];
        (BS.pk,BS.sk) <@ S.kgen();
        r <- keys_out (BS.pk, BS.sk);
      }
      return r;
    }
  }.

  (* Hybrid adversary *)
  local module HA(Ob: Orclb, OLR: Orcl) = {

    module O = {
      var l, l0: int
      var chL  : cipher  list
      var sk   : skey
    
      proc enc(p0 p1: plain): cipher = {
        var r;
        r <@ OLR.orcl(p0,p1);
        return r;
      }
    }

    module A = A(O)

    proc main() : bool = {
      var r1, pk, sk, b;
      r1 <@ Ob.leaks(keys_in);
      (pk, sk) <- oget (destr_keys_out r1);
      b <@ A.main(pk);
      return b;
    } 
  }.

  local lemma leaks_ll: islossless OrclbImpl.leaks.
  proof.
    proc; sp.
    if; first by wp; call S_kgen_ll; wp.
    by wp.
  qed.

  local lemma orclL_ll : islossless OrclbImpl.orclL.
  proof. by proc; inline *; wp; call S_enc_ll; auto. qed.

  local lemma orclR_ll : islossless OrclbImpl.orclR.
  proof. by proc; inline *; wp; call S_enc_ll; auto. qed.

  local lemma hyb1 &m:
        Pr[Ln(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]
      - Pr[Rn(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1] 
    = 1%r/n%r * (
          Pr[Ln(OrclbImpl,HA).main() @ &m : res /\ Count.c <= n ]
        - Pr[Rn(OrclbImpl,HA).main() @ &m : res /\ Count.c <= n ]).
  proof.
    apply (Hybrid OrclbImpl HA  _ _ _ _ &m (fun ga ge l r, r )).
    + apply leaks_ll.
    + apply orclL_ll.
    + apply orclR_ll.
    + move=> Ob LR LR_ll leaks_ll L_ll R_ll; proc.
      call (_: true); first apply A_main_ll.
      + by proc; call LR_ll; auto. 
      by wp; call leaks_ll; auto.
  qed.

  (* ---------------------------------------------------------------------- *)
  (* Show equivalence between Hybrid adversary and IND-CCA *)

  local module HA_O_L = HA(OrclbImpl, OrclCount(L(OrclbImpl))).
  local module HA_O_R = HA(OrclbImpl, OrclCount(R(OrclbImpl))).

  (* BS: For the constraints here on C, it would be nice to define and enforce
         non-memory modules. *)
  local lemma hyb_aux1 (C <: LorR{BS,S,A,Count}) &m:
      Pr[Orcln(HA(OrclbImpl), LeftRight(C,OrclbImpl)).main() @ &m : res /\ Count.c <= n]
    = Pr[CPA(S,A,C).main()  @ &m: res /\ size BS.encL <= n].
  proof.
    byequiv (_:  ={glob A, glob S, glob BS}
             ==> _) => //. 
    proc.
    inline Count.init HA(OrclbImpl, OrclCount(LeftRight(C, OrclbImpl))).main; wp.
    call (_: Count.c{1} = size BS.encL{2} /\ ={glob BS, glob S}).
    + proc. inline *. simplify.
      swap{1} 3 -2.
      seq 1 1:
        (  ={p0, p1,l_or_r, glob S} /\
         Count.c{1} = size BS.encL{2} /\ 
           ={BS.sk, BS.encL, BS.pk, BS.eq, BS.qt}).
        by call (_: true); auto.
      wp; sp.
      by if{1}; wp; sp; call (: true); try (proc true); auto; progress; smt. 
    inline *. wp. sp.
    rcondt{1} 1; first by auto.
    by wp; call(: true); wp.  
  qed.

  local lemma hyb2 &m:
      Pr[Ln(OrclbImpl, HA).main() @ &m : res /\ Count.c <= n]
    = Pr[CPA(S,A,Left).main()  @ &m: res /\ size BS.encL <= n].
  proof. by apply (hyb_aux1 Left &m). qed.

  local lemma hyb3 &m:
      Pr[Rn(OrclbImpl, HA).main() @ &m : res /\ Count.c <= n]
    = Pr[CPA(S,A,Right).main()  @ &m: res /\ size BS.encL <= n].
  proof. by apply (hyb_aux1 Right &m). qed.

  local lemma hyb4 &m:
      `|  Pr[Ln(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]
        - Pr[Rn(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]|
    = 1%r/n%r * (
       `|  Pr[CPA(S,A,Left).main()  @ &m:  res /\  size BS.encL <= n]
         - Pr[CPA(S,A,Right).main()  @ &m: res /\  size BS.encL <= n]|).
  proof.
    rewrite -(hyb2 &m) -(hyb3 &m) (hyb1 &m) StdOrder.RealOrder.normrM.
    have H :  (0%r <= (1%r / n%r)) by smt.
    smt.
  qed.

  local lemma hyb_aux2 (C <: LorR{BS,S,A,Count,WrapAdv,HybOrcl}) &m:
      Pr[Orcln(HybGame(HA,OrclbImpl),
               LeftRight(C,OrclbImpl)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]
    = Pr[CPA(S,WrapAdv(A,S),C).main()  @ &m: res /\ WrapAdv.l <= n /\ size BS.encL <= 1].
  proof.
    byequiv (_: ={glob S, glob BS, glob A}
             ==> ={res} /\ HybOrcl.l{1} = WrapAdv.l{2} /\ 
                           Count.c{1} = size BS.encL{2} /\ Count.c{1} <= 1) => //.
    proc. inline *. wp.
    rcondt{1} 6; first by auto.
    call(_:
         ={glob S} /\ ={l,l0}(HybOrcl,WrapAdv) /\ ={encL,pk}(BS,WrapAdv) /\
         ={BS.sk, BS.pk} /\
         (forall cl, mem BS.encL{2} cl => mem BS.encL{1} cl) /\
         Count.c{1} = size BS.encL{2} /\ 
         Count.c{1} = if HybOrcl.l{1} <= HybOrcl.l0{1} then 0 else 1).
    + proc. inline *. wp. sp.
      if; first by auto.
      + wp. call (: true). 
        by auto; progress; smt.
      + if; first by auto.
        + sp.
          seq 1 1:
              (p00{2} = p0{2} /\
               p10{2} = p1{2} /\
               m0{1} = m{1} /\
               m1{1} = m0{1} /\
               ((m{1} = (p0{1}, p1{1}) /\
               (={p0, p1}) /\
               ={glob S} /\
               (HybOrcl.l{1} = WrapAdv.l{2} /\ HybOrcl.l0{1} = WrapAdv.l0{2}) /\
               (BS.encL{1} = WrapAdv.encL{2} /\ BS.pk{1} = WrapAdv.pk{2}) /\
               ={BS.sk, BS.pk} /\
               (forall (cl : cipher), mem BS.encL{2} cl => mem BS.encL{1} cl) /\
               Count.c{1} = size BS.encL{2} /\
               Count.c{1} = if HybOrcl.l{1} <= HybOrcl.l0{1} then 0 else 1) /\
               !HybOrcl.l0{1} < HybOrcl.l{1}) /\
               HybOrcl.l0{1} = HybOrcl.l{1} /\
               l_or_r1{1} = l_or_r{2}). (* this is new *)
            by call (_: true); auto.
          case (l_or_r1{1}).
          + rcondt{1} 1; first by auto.
            wp; call (_: true); try (proc true); auto; progress; smt.
          + rcondf{1} 1; first by auto.
            by wp; call (_: true); try (proc true); auto; progress; smt.
        + wp. call (_: true).
          by auto; progress; smt.
    swap{1} 2 -1. 
    swap{2} 6 -5. swap{1} 7 -5. swap{1} 7 -5. 
    wp; call(: true); wp; rnd.
    by auto; progress; smt.
  qed.

  local lemma hyb5 &m:
      Pr[Ln(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]
    = Pr[CPA(S,WrapAdv(A,S),Left).main()  @ &m: res /\ WrapAdv.l <= n /\ size BS.encL <= 1].
  proof. by apply (hyb_aux2 Left &m). qed.

  local lemma hyb6 &m:
      Pr[Rn(OrclbImpl, HybGame(HA)).main() @ &m : (res /\ HybOrcl.l <= n) /\ Count.c <= 1]
    = Pr[CPA(S,WrapAdv(A,S),Right).main()  @ &m:  res /\ WrapAdv.l <= n /\ size BS.encL <= 1].
  proof. by apply (hyb_aux2 Right &m). qed.

  lemma CPA_multi_enc &m:
      `|  Pr[CPA(S,WrapAdv(A,S),Left).main()  @ &m:   
                   res /\ WrapAdv.l <= n /\ size BS.encL <= 1]
        - Pr[CPA(S,WrapAdv(A,S),Right).main()  @ &m:  
                   res /\ WrapAdv.l <= n /\ size BS.encL <= 1] |
    = 1%r/n%r * (
       `|  Pr[CPA(S,A,Left).main()  @ &m:  res /\ size BS.encL <= n]
         - Pr[CPA(S,A,Right).main()  @ &m: res /\ size BS.encL <= n]|).
  proof.
    rewrite -(hyb5 &m) -(hyb6 &m).
    by apply (hyb4 &m).
  qed.

end section HybridProof.

